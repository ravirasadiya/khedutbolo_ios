//
//  DateGeneric.swift

import UIKit

enum DateFormat {
    
    case DayHour24
    case DayHour12
    case DayDateMonth
    case DayMonthDate
    case OnlyDay
    
    case FullWeekDay
    case ShortWeekDay
    
    case DateTimeMMDDYY
    case DateMMDDYY
    case DateYYMMDD
    case ShortDateMonth
    
    case DateTimeYY_MM_DD
    case ServerDate
    case ServerDateTZ
    case ServerDateT000Z

    case OnlyMonth
    case ShortMonth
    case FullMonth
    case OnlyYear
    case FullMonthYear
    case ShortMonthYear
    
    case DateMonth
    
    case MonthYear
    case MonthDateYear
    case MonthDateYear12Time
    
    var text:String{
        
        switch self {
        case .DateTimeMMDDYY:
            return "MM/dd/yyyy hh:mm:ss a"
        case .DateMMDDYY:
            return "MM/dd/yyyy"
        case .DateYYMMDD:
            return "yyyy-MM-dd"
        case .DayHour24:
            return "HH:mm:ss"
        case .DayHour12:
            return "h:mm a"
        case .OnlyDay:
            return "dd"
        case .FullWeekDay:
            return "EEEE"
        case .ShortWeekDay:
            return "EEE"
        case .DayDateMonth:
            return "EEE, dd MMM"
        case .DayMonthDate:
            return "EEEE, MMMM dd"
        case .DateTimeYY_MM_DD:
            return "yyyy-MM-dd HH:mm:ss"
        case .ServerDate:
            return "yyyy-MM-dd'T'HH:mm:ssZ"
        case .ServerDateTZ:
            return "yyyy-MM-ddTHH:mm:sszzz"
        case .ServerDateT000Z:
            return "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        case .OnlyMonth:
            return "MM"
        case .ShortMonth:
            return "MMM"
        case .ShortDateMonth:
            return "dd MMM"
        case .FullMonth:
            return "MMMM"
        case .OnlyYear:
            return "yyyy"
        case .FullMonthYear:
            return "MMMM yyyy"
        case .ShortMonthYear:
            return "MMM yyyy"
        case .DateMonth:
            return "dd MMMM"
        case .MonthYear:
            return "MM/yyyy"
        case .MonthDateYear:
            return "MMM dd, yyyy"
        case .MonthDateYear12Time:
            return "MMMM dd, yyyy - hh:mm a"
        }
    }
}

struct DateGeneric {
    
    static func date(fromString stringDate:String, withFormat format:DateFormat) -> Date? {
        
        let formatter = DateFormatter()
        formatter.dateFormat = format.text
        formatter.locale = Locale(identifier: "en_US_POSIX") // For time format.
        formatter.timeZone = TimeZone(abbreviation:"UTC") // for calculate time.
        
        if let date = formatter.date(from: stringDate){
            return date
        }
        return nil
    }
    
    static func dateLocal(fromString stringDate:String, withFormat format:DateFormat) -> Date? {
        
        let formatter = DateFormatter()
        formatter.dateFormat = format.text
        formatter.locale = Locale(identifier: "en_US_POSIX") // For time format.
        formatter.timeZone = TimeZone.current // for calculate time.
        
        if let date = formatter.date(from: stringDate){
            return date
        }
        return nil
    }
    
    static func string(fromDate date:Date, withFormat format:DateFormat) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format.text
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.timeZone = TimeZone(abbreviation:"UTC")
        return formatter.string(from: date)
    }
    
    static func stringWithLocalTime(fromDate date:Date, withFormat format:DateFormat) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format.text
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.timeZone = TimeZone.current
        return formatter.string(from: date)
    }
    
    static func formateDateForChat(date : Date, displayTime : String) -> String {
        
        if Calendar.current.isDateInToday(date) {
            return displayTime
        }
        else if Calendar.current.isDateInYesterday(date) {
            return "Yesterday at \(displayTime)"
        }
        else if let noOfDays = Calendar.current.dateComponents([.day], from: date, to: Date()).day, noOfDays < 7 {
            return "\(self.stringWithLocalTime(fromDate: date, withFormat: .FullWeekDay)) at \(displayTime)"
        }
        else {
            return "\(self.stringWithLocalTime(fromDate: date, withFormat: .DateMonth)) at \(displayTime)"
        }
    }
    
    static func convertTime(withOldFormat oldFormat:String, withNewFormat newFormat: String, strTime:String) -> String{
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = oldFormat
        let date = dateFormatter.date(from: strTime)
        dateFormatter.dateFormat = newFormat
        let stringTime = dateFormatter.string(from: date!)
        
        return stringTime
    }
    
    
    static func stringFromFormat(_ format: String, passDate:NSDate) -> String {
           let formatter = DateFormatter()
           formatter.locale = Locale.autoupdatingCurrent
           formatter.calendar = Calendar.autoupdatingCurrent
           formatter.dateFormat = format
        return formatter.string(from: passDate as Date)
    }
}
