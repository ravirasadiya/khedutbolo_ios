//
//  Enum.swift
//  KhedutBolo
//
//  Created by Trivedi Sagar on 10/19/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit
import Foundation

enum DeviceType : Int {
    case iOS = 0
    case Android = 1
}


enum Language : Int{
    
    case kEnglish   = 0
    case kHindi     = 1
    case kGujarati  = 2
}

enum LeftMenuType : Int {

    case MyProfile = 0
    case CurrentOrder = 1
    case CompleteOrder = 2
    case CancelOrder = 3
    case ChangeLanguage = 4
    case Wallet = 5
    case ShareApp = 6
    case Logout = 7
}

enum OderType : Int {
    case CurrentOrder = 0
    case CompleteOrder = 1
    case CancelOrder = 2
}
