//
//  ParameterKeys.swift
//  ClipDart


import Foundation

struct ParameterKeys {
    
    // MARK: - request parameters

    struct HeaderFields {
        /*~~~~~ header fields ~~~~~*/
        static let DeviceType               = "DeviceType"
        static let AppVersion               = "AppVersion"
        static let OSVersion                = "OSVersion"
        static let Authorization            = "Authorization"

    }
    
    static let Mobile                           = "mobile"
    static let CountryCode                      = "country_code"
    static let UserId                           = "userid"
    static let User_Id                          = "user_id"
    static let Id                               = "id"
    static let Customer_id                      = "customer_id"
    static let Company_id                       = "company_id"
    
    static let Product_id                       = "product_id"
    static let Quantity                         = "quantity"
    static let Product_size                     = "product_size"
    static let Total_price                      = "total_price"
    static let Attribute_id                     = "attribute_id"
    
    static let delete_price                     = "delete_price"
    
    static let Order_id                         = "order_id"
    
    static let State_id                         = "state_id"
    static let City_id                          = "city_id"
    static let Tehsil_id                        = "tehsil_id"
    
    static let Name                             = "name"
    static let Address                          = "address"
    static let Image                            = "image"
    static let Language                         = "language"
    static let Vid                              = "vid"
    static let Sid                              = "sid"
    static let Tid                              = "tid"
    static let Cid                              = "cid"
    
    static let Alias                            = "alias"
    static let Zip                              = "zip"
    static let City                             = "city"
    static let Street                           = "street"
    static let State                            = "state"
    static let FullAddress                      = "full_address"
    static let Payment                          = "payment"
    
    static let Offer_id                         = "offer_id"
    static let Wallet_point                     = "wallet_point"
    static let Total_gst                        = "total_gst"
    static let Mail                             = "mail"
    static let Token                            = "token"

    
    // MARK: - response parameters
    
    static let AuthToken                = "auth_token"
    static let ProductCat_ProductManage = "Product Categories / Manage Products"
    static let Company_List             = "Company List"
    static let Trending_Products        = "Trending Products"
    
    static let Companies                = "companies"
    static let Techicals                = "techicals"


}
