//
//  APIRouter.swift

import Foundation
import Alamofire

enum APIRouter {
    
    case SendOtp(Parameters)
    
    case Register(Parameters)
    
    case GetAppVersion(withDeviceType : Int, version : String)
    
    case GetOffers(Parameters)
    
    case GetCrop(Parameters)
    
    case ProductCategories(withCustId : String)
    
    case ProductDetail(Parameters)
    
    case ProductAttributeDetail(Parameters)
    
    case SimilarProduct(Parameters)
    
    case CompanyProducts(Parameters)
    
    case CropSolution(Parameters)
    
    case CropSubSolution(Parameters)
    
    case Solution(Parameters)
    
    case GetSubCategories(withCatID : String, custId : String)
    
    case SelectedCategory(Parameters)
    
    case AddToCart(Parameters)
    
    case GetShowCart(withCustId : String)
    
    case CurrentOrder(Parameters)
    
    case CompleteOrder(Parameters)
    
    case ShowCancelOrder(Parameters)
    
    case GetState(custId : String,Luang:Int)
    
    case GetCities(Parameters)
    
    case GetTehsil(withCityId : String)
    
    case GetVillage(withTehsilId : String)
    
    case UpdateProfile(Parameters)
    
    case GetNotification(Parameters)
    
    case AddOrder(Parameters)
    
    case GetCountProduct(withCustId : String)
    
    case GetFilter(withCropId: String, userId : String)
    
    case OfferDetails(Parameters)
    
    case GetSearch(withValue : String, custId : String)
    
    case GetWallet(withCustId: String)
    
    case DeleteOrderFromCart(Parameters)
    
    case GetOrder(withCustId : String, withFlag: String)

    case GetCancelOrder(withOrderId: String, userId : String)
    
    case GetBuyNowProduct(withCustId: String, withProdId: String, withQuantity: String, withAttributeId: String, withTotalPrice: String)
    
    case Logout(Parameters)
}

extension APIRouter {
    
    var path: String{
        
        var url:String = ""
        
            switch self {
                
                case .SendOtp:
                    url = "v1/sendOTP"
                
                case .Register:
                    url = "v1/register_customer"
                
                case .GetAppVersion:
                    url = "GetAppVersion"
                
                case .GetOffers:
                    url = "v1/offer"
                
                case .GetCrop:
                    url = "v1/crop"
                
                case .ProductCategories:
                    url = "v1/product_categories"
                
                case .ProductDetail:
                    url = "v1/productDetail"
                
                case .ProductAttributeDetail:
                    url = "v1/productAttribute"
                
                case .SimilarProduct:
                    url = "v1/similarProducts"
                
                case .CompanyProducts:
                    url = "v1/companyProducts"
                
                case .CropSolution:
                    url = "v1/cropSolution"
                
                case .CropSubSolution:
                    url = "v1/cropSubSolution"
                
                case .Solution:
                    url = "v1/solution"

                case .GetSubCategories:
                    url = "v1/get_subCategory"
                
                case .SelectedCategory:
                    url = "v1/selectedCategory"
                
                case .AddToCart:
                    url = "v1/addCart"
                
                case .GetShowCart:
                    url = "v1/showCart"
                
                case .CurrentOrder:
                    url = "v1/currentOrder"
                
                case .CompleteOrder:
                    url = "v1/completeOrder"
                
                case .ShowCancelOrder:
                    url = "v1/showCancelOrder"
                
                case .GetState:
                    url = "v1/state"
                
                case .GetCities: 
                    url = "v1/cities"
                
                case .GetTehsil:
                    url = "v1/tehsil"
                
                case .GetVillage:
                    url = "v1/villages"
                
                case .UpdateProfile:
                    url = "v1/update/profile"
                
                case .GetNotification:
                    url = "v1/notification"

                case .AddOrder:
                    url = "v1/order"
                
                case .GetCountProduct:
                    url = "v1/count-product"
                
                case .GetFilter:
                    url = "v1/ctfilter"
                
                case .OfferDetails:
                    url = "v1/offer/products"
                
                case .GetSearch:
                    url = "v1/search"
                
                case .GetWallet:
                    url = "v1/get_wallet"
                
                case .DeleteOrderFromCart:
                    url = "v1/deleteProductCart"
                
                case .GetOrder:
                    url = "v1/get-order"
                
                case .GetCancelOrder:
                    url = "v1/cancelOrder"
                
                case .GetBuyNowProduct:
                    url = "v1/buy-now"
                
                case .Logout:
                    url = "v1/log_in_out"
            }
        
        return url
    }
    
    var baseURL: String {
        
        return StaticURL.APIBase + "api/"
    }
    
    var queryString: String {
        
        switch self {
            
        case .GetAppVersion(let deviceType, let appVersion):
            return "?deviceType=\(deviceType)&version=\(appVersion)"
            
        case .ProductCategories(let userId):
            return "?user_id=\(userId)"

        case .GetSubCategories(let catId, let custId):
            return "?id=\(catId)&user_id=\(custId)"
            
        case .GetShowCart(let custId):
            return "?customer_id=\(custId)&user_id=\(custId)"
            
        case .GetTehsil(let cityId):
            return "?city_id=\(cityId)"
            
        case .GetVillage(let tehsilId):
            return "?tehsil_id=\(tehsilId)"
            
        case .GetCountProduct(let custId):
            return "?customer_id=\(custId)&user_id=\(custId)"
            
        case .GetFilter(let cropId, let userId):
            return "?crop_id=\(cropId)&user_id=\(userId)"
        
        case .GetSearch(let value, let userId):
            return "?value=\(value)&user_id=\(userId)"
            
        case .GetWallet(let custId):
            return "?customer_id=\(custId)&user_id=\(custId)"
    
        case .GetOrder(let custId, let flagId):
            return "?user_id=\(custId)&flag=\(flagId)"
            
        case .GetCancelOrder(let orderId, let userId):
            return "?order_id=\(orderId)&user_id=\(userId)"
            
        case .GetBuyNowProduct(let custId, let prodId, let qty, let attrId, let total):
            return "?customer_id=\(custId)&product_id=\(prodId)&quantity=\(qty)&attribute_id=\(attrId)&total_price=\(total)&user_id=\(custId)"
            
        default:
            return ""
        }
    }
    
    var method: HTTPMethod {
        
        switch self {
            
        case .GetAppVersion, .ProductCategories, .GetSubCategories, .GetShowCart, .GetState, .GetTehsil, .GetVillage, .GetCountProduct, .GetFilter, .GetSearch, .GetWallet, .GetOrder, .GetCancelOrder, .GetBuyNowProduct:
            return .get
        default:
            return .post
        }
    }
    
    var parameters: Parameters? {
        
        switch self {
            
        case .SendOtp(let param):
            return param
            
        case .Register(let param):
            return param
            
        case .GetOffers(let param):
            return param
            
        case .GetCrop(let param):
            return param
        
        case .ProductDetail(let param):
            return param
            
        case .ProductAttributeDetail(let param):
            return param
            
        case .SimilarProduct(let param):
            return param
            
        case .CompanyProducts(let param):
            return param
            
        case .CropSolution(let param):
            return param
            
        case .CropSubSolution(let param):
            return param
            
        case .Solution(let param):
            return param
            
        case .SelectedCategory(let param):
            return param
            
        case .AddToCart(let param):
            return param
            
        case .CurrentOrder(let param):
            return param
            
        case .CompleteOrder(let param):
            return param
            
        case .ShowCancelOrder(let param):
            return param
            
        case .GetCities(let param):
            return param
            
        case .UpdateProfile(let param):
            return param
            
        case .GetNotification(let param):
            return param
            
        case .AddOrder(let param):
            return param
            
        case .OfferDetails(let param):
            return param
            
        case .DeleteOrderFromCart(let param):
            return param
            
        case .Logout(let param):
            return param
            
        default:
            return nil
        }
    }
}
