//
//  StringFile.swift
//  KhedutBolo
//
//  Created by Trivedi Sagar on 10/18/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit

class StringFile: NSObject {
    
    static let strAppName = ["KhedutBolo","KhedutBolo","ખેડૂતબોલો"]
    static let strError = ["Error","भूल","ભૂલ"]
    static let strSorry = ["Sorry","माफ़ करना","માફ કરશો"]
    static let strThankYou = ["Thank You","धन्यवाद","આભાર"]
    static let strSuccess = ["Success","सफलता","સફળતા"]

    static let strAlert = ["Alert", "उपदेश", "ઉપદેશ"]
    static let strUnknown_error = ["Unknown error found", "अज्ञात त्रुटि पाई गई है", "અજ્ઞાત ભૂલ મળી આવે"]
    static let MsgNoInternet = ["Internet not available", "इंटरनेट उपलब्ध नहीं है", "ઇન્ટરનેટ ઉપલબ્ધ નથી"]
    
    static let msgNeedProperDetails = ["Something missing can you please try again with proper details", "कुछ गायब है आप उचित विवरण के साथ फिर से कोशिश कर सकते हैं", "કંઈક ખૂટે છે તમે કૃપા કરીને યોગ્ય વિગતો સાથે ફરીથી પ્રયાસ કરો"]
    static let msgSomthingWrong = ["Something went wrong", "कुछ गलत हो गया", "કંઈક ખોટું થયું"]
    
    static let msgProfileUpdateSuccess = ["Your profile update successfully", "आपकी प्रोफ़ाइल सफलतापूर्वक अपडेट हो गई है", "તમારું પ્રોફાઇલ સફળતાપૂર્વક અપડેટ થયું"]
    
    static let msgAddCartSuccess = ["Product is added to cart successfully", "उत्पादन को कार्ट में सफलतापूर्वक जोड़ा जाता है", "ઉત્પાદન સફળતાપૂર્વક કાર્ટમાં ઉમેરવામાં આવ્યું છે"]
    
    static let msgSelectFilterOptions = ["Please select a filter option", "कृपया एक फ़िल्टर विकल्प चुनें", "કૃપા કરીને ફિલ્ટર વિકલ્પ પસંદ કરો"]
    
   /*~~~~~ Language View ~~~~~*/
    static let strSelectLanguage = ["SELECT LANGUAGE","भाषा का चयन करें","ભાષા પસંદ કરો"]
    static let strEnglishTitle = "ENGLISH"
    static let strEnglishTitleSmall = "English"
    static let strHindiTitle = "हिंदी"
    static let strGujaratiTitle = "ગુજરાતી"
    static let strNext = ["Next","आगे","આગળ"]
    
    /*~~~~~ Login View ~~~~~*/
    static let strEnterMobileNum = ["Enter your mobile number","अपना दूरभाष क्रमांक दर्ज करें!","તમારો ફોન નંબર દાખલ કરો!"]
    static let strGet = ["Get","प्राप्त","મેળવો"]
    static let strSendOtp = ["SEND OTP","OTP भेजें","OTP મોકલો"]

    static let msgEnterPhoneNumber = ["Please enter phone number","कृपया फ़ोन नंबर दर्ज करें","કૃપા કરીને ફોન નંબર દાખલ કરો"]
    static let msgEnterPhoneNumberValid = ["Please enter valid phone number","कृपया मान्य फ़ोन नंबर दर्ज करें","કૃપા કરીને સાચો ફોન નંબર નાખો"]
    
    /*~~~~~ Verify OTP View ~~~~~*/
    static let strVerificationCode = ["Verification Code","पुष्टि संख्या","ચકાસણી સંજ્ઞા"]
    static let msgVerifyOTPInfo = ["Please type the verification code sent to","कृपया भेजा गया सत्यापन कोड टाइप करें", "કૃપા કરીને મોકલેલો ચકાસણી કોડ લખો"]
    static let strEnterCode = ["Enter your Code!","अपना कोड दर्ज करे!","તમારો કોડ નાખો!"]
    static let strVerifyOTP = ["Verify OTP","OTP को सत्यापित करें","OTP ને ચકાસી લો"]
    static let strResendOTP = ["Resend OTP","OTP पुनः भेजें","OTP ફરીથી મોકલો"]
    static let msgEnterEnterCode = ["Please enter code","कृपया कोड दर्ज करें","કૃપા કરી કોડ દાખલ કરો"]
    static let msgInvalidOTP = ["Invalid OTP","अमान्य OTP","અમાન્ય OTP"]
    static let strSecondsRemaining = ["Seconds remaining","शेष रहे","બાકી સેકન્ડ્સ"]
    
    
    /*~~~~~ Register View ~~~~~*/
    static let strUserRegistration = ["User Registration", "उपयोगकर्ता का पंजीकरण", "વપરાશકર્તા નોંધણી"]
    static let strName = ["Name", "नाम", "નામ"]
    static let strPlaceEnterName = ["Enter name", "नाम दर्ज", "નામ દાખલ કરો"]
    static let strPhoneNumber = ["Phone number", "फ़ोन नंबर", "ફોન નંબર"]
    static let strPlaceEnterPhone = ["Enter phone number here", "यहां फोन नंबर डालें", "અહીં ફોન નંબર દાખલ કરો"]
    static let strSelectState = ["Select state", "राज्य चुनें", "રાજ્ય પસંદ કરો"]
    static let strSelectCity = ["Select city", "शहर चुनें", "શહેર પસંદ કરો"]
    static let strSelectTehsil = ["Select tehsil", "तहसील चुनें", "તહસીલ પસંદ કરો"]
    static let strSelectVillage = ["Select village", "गांव चुनें", "ગામ પસંદ કરો"]
    static let strSubmitDetails = ["Submit details", "विवरण जमा करें", "વિગતો સબમિટ કરો"]
    static let strSelectLanguageSmall = ["Select Language","भाषा का चयन करें","ભાષા પસંદ કરો"]

    
    /*~~~~~ Home View ~~~~~*/
    static let strAppNameCaps = ["KHEDUT BOLO","किसनबोले","ખેડૂતબોલો"]
    static let strSelectBelowSolution = ["Select Below Solution", "नियंत्रण देखें", "નિયંત્રણ જુઓ"]
    static let strProductCategoriesManageProducts = ["Product Categories / Manage Products", "विभाग द्वारा खरीद", "વિભાગ મુજબ ખરીદી"]
    static let strTrendingProductions = ["Trending Productions", "चर्चित उत्पादन", "લોકપ્રિય ઉત્પાદનો"]
    static let strCompanyList = ["Company List", "कंपनी सूची", "કંપની યાદી"]
    static let strMORE = ["MORE", "अधिक", "વધુ"]
    static let strALL = ["ALL", "सभी", "બધા"]
    static let strPerSet = ["per set", "प्रति सेट", "દીઠ સમૂહ"]
    static let strBy = ["By", "द्वारा", "દ્વારા"]
    
    /*~~~~~ All Product View ~~~~~*/
    static let strTrendingProducts = ["Trending Products", "ट्रेंडिंग प्रोडक्ट्स", "ટ્રેંડિંગ પ્રોડક્ટ્સ"]
    static let strTechnicalName = ["Technical Name", "तकनीकी नाम", "તકનીકી નામ"]
    static let strCompanyName = ["Company Name", "कंपनी का नाम", "કંપની નું નામ"]
    static let strItemName = ["Item Name", "वस्तु का नाम", "વસ્તુનુ નામ"]

    /*~~~~~ My Profile View ~~~~~*/
    static let strMyProfile = ["My Profile", "मेरी प्रोफाइल", "મારી પ્રોફાઈલ"]
    static let strState = ["State", "राज्य", "રાજ્ય"]
    static let strCity = ["City", "शहर", "શહેર"]
    static let strTehsil = ["Tehsil", "तहसील", "તાલુકો"]
    static let strVillage = ["Village", "गाँव", "ગામ"]
    static let strEditSmall = ["Edit", "संपादित करें", "સુધારો"]
    static let strSaveSmall = ["Save", "बचाएं", "સાચવો"]
    static let strOK = ["OK", "ठीक", "બરાબર"]
    
    /*~~~~~ Order View ~~~~~*/
    static let strOrderDetail = ["Order Detail", "ऑर्डर विवरण", "ઓર્ડર વિગત"]
    static let strCurrentOrder = ["Current Order", "अभी का ऑर्डर", "હાલ ના ઑર્ડર"]
    static let strCompleteOrder = ["Complete Order", "पूर्ण ऑडर", "પૂર્ણ ઓર્ડર"]
    static let strCancelOrder = ["Cancel Order", "रद्द किया गया ऑडर", "રદ કરેલ ઓર્ડર"]//"ઑડર રદ કરો"]
    static let strCancelOrderCap = ["CANCEL ORDER", "ऑर्डर रद्द", "ઓર્ડર રદ"]
    static let msgCancelOrderInfo = ["Are you sure want to cancel the order?", "क्या आप वाकई ऑर्डर रद्द करना चाहते हैं?", "શું તમે ખરેખર ઓર્ડર રદ કરવા માંગો છો?"]
    static let strYesSmall = ["Yes", "हाँ", "હા"]
    static let strNoSmall = ["No", "नहीं", "ના"]
    static let strQty = ["Qty", "मात्रा", "નંગ"]
    static let strNoDataFound = ["No data found", "कोई डेटा नहीं मिला", "કોઈ ડેટા મળ્યો નથી"]
    static let strSellBy = ["Seller Name", "विक्रेता का नाम", "વેચનારનું નામ"]
    static let strProductSize = ["Product Size", "उत्पादन का आकार", "પ્રોડક્ટ સાઇઝ"]
    static let strQuantity = ["Quantity", "मात्रा", "નંગ"]
    static let strSellingPrice = ["Selling Price", "बिक्री मूल्य", "વેચાણ કિંમત"]
    static let strShippingPrice = ["Shipping Price", "माल भेजने का मूल्य", "પહોંચાડવાનો ખર્ચ"]
    static let strTotalPrice = ["Total Price", "कुल कीमत", "કુલ કિંમત"]
    static let strOrderid = ["Order id", "आदेश आईडी", "ઓર્ડર આઈડી"]
    static let strOrderDate = ["Order Date", "आदेश की तारीख", "ઓર્ડર તારીખ"]
    static let strVIEW = ["VIEW", "देखें", "જુઓ"]
    static let strDateOfOrder = ["Date of order", "आदेश की तिथि", "ઓર્ડરની તારીખ"]
    
    /*~~~~~ Wallet View ~~~~~*/
    static let strWallet = ["Wallet", "बटुआ", "વૉલેટ"]
    static let strYourWalletPoint = ["Your wallet point", "आपका बटुआ अंक", "તમારા વૉલેટ પોઇન્ટ્સ"]
    
    /*~~~~~ Change Language View ~~~~~*/
    static let strChangeLanguage = ["Change Language", "भाषा बदलो", "ભાષા બદલો"]
    static let strSetAsDefault = ["Set as Default", "भाषा बदलें", "ભાષા બદલો"]
    static let msgSuccessChangeLanuage = ["The language changed successfully", "भाषा सफलतापूर्वक बदल गई", "ભાષા સફળતાપૂર્વક બદલાઈ ગઈ"]
    
    /*~~~~~ Search View ~~~~~*/
    static let strSearch = ["Search", "खोजे", "પ્રોડક્ટ શોધો"]

    /*~~~~~ Company List View ~~~~~*/
    static let strCompanies = ["Companies", "कंपनियों", "કંપનીઓ"]
    
    /*~~~~~ Crop View ~~~~~*/
    static let strSelectCrop = ["Select Crop", "फसल का चयन करें", "પાક પસંદ કરો"]
    
    /*~~~~~ Solution View ~~~~~*/
    static let strSelectSolution = ["Select Solution", "नियंत्रण पसंद करें", "સોલ્યુશન પસંદ કરો"]
    
    /*~~~~~ Offers View ~~~~~*/
    static let strOffers = ["Offers", "ऑफर", "ઓફર્સ"]
    
    /*~~~~~ Notifications View ~~~~~*/
    static let strNotification = ["Notification", "सूचना", "સૂચના"]
    
    /*~~~~~ Cart View ~~~~~*/
    static let strShoppingCart = ["My Bag", "मेरा बैगर्ट", "મારી બેગ"]
    static let strSellerName = ["Seller Name", "विक्रेता का नाम", "વિક્રેતા નામ"]
    static let msgRemoveCartConfirmation = ["Are you sure want to remove", "क्या आप वाकई हटाना चाहते हैं", "શું તમે ખરેખર દૂર કરવા માંગો છો"]
    static let strRemove = ["Remove", "हटाना", "દૂર કરો"]
    static let strCancel = ["Cancel", "रद्द करना", "રદ કરો"]
    static let strContinueShopping = ["CONTINUE SHOPPING", "खरीदारी जारी रखें", "ખરીદારી ચાલુ રાખો"]
    static let strProceedCheckout = ["CONFIRM ORDER", "खरीद के लिऐ आगे बढ़े", "ઓર્ડર કરો"]
    
    /*~~~~~ Order View ~~~~~*/
    static let strOrder = ["Shipping Details", "परिवहन विवरण","શિપિંગ વિગતો"]// "आदेश", "ઓર્ડર"]
    static let strMobileNumber = ["Mobile Number", "मोबाइल नंबर", "મોબાઇલ નંબર"]
    static let strEmail = ["Email", "ईमेल", "ઇમેઇલ"]
    static let strPinCode = ["Pin Code", "पिन कोड", "પીન કોડ"]
    static let strFullAddress = ["Full Address", "पूरा पता", "પૂરું સરનામું"]
    static let strPaymentMethod = ["Payment Method", "भुगतान का तरीका", "ચુકવણી પદ્ધતિ"]
    static let strCOD = ["COD", "वस्तु मिलने पर नकद राशि देना।", "વાસ્તુ મળે ત્યારે રોકડા રૂપિયા આપવા."]
    static let strNetBanking = ["Net Banking", "नेट बैंकिंग", "નેટ બેન્કિંગ"]
    static let strTotalPayment = ["Total Payment", "कुल भुगतान", "કુલ ચુકવણી"]
    static let strPaymentCap = ["PAYMENT", "भुगतान", "ચુકવણી"]
    static let strOrderSummary = ["Order Summary", "आदेश सारांश", "ઓર્ડર સારાંશ"]
    static let strShippingAddress = ["Shipping Address", "शिपिंग पता", "પહોંચાડવાનું સરનામું"]
    static let strSelectedPaymentMethod = ["Selected payment method", "चयनित भुगतान विधि", "ચુકવણીની પસંદગીની પદ્ધતિ"]
    static let strPrice = ["Price", "कीमत", "કિંમત"]
    static let strWalletAmount = ["Wallet Amount", "बटुआ राशि", "વૉલેટ રકમ"]
    static let msgGSTIncluded = ["*GST included.\n*Shipping charge extra.", "*जीएसटी शामिल.\n*शिपिंग चार्ज अतिरिक्त.", "*જીએસટી શામેલ છે.\n*શીપીંગ ચાર્જ અતિરિક્ત."]
    static let strComplete = ["COMPLETE", "पूर्ण", "પૂર્ણ"]
    static let strDone = ["Done", "पूर्ण", "થઈ ગયું"]
    static let msgPleaseEnterName = ["Please enter name", "कृपया नाम दर्ज करें", "કૃપા કરી નામ દાખલ કરો"]
    static let msgPleaseEnterPhone = ["Please enter phone", "कृपया फ़ोन दर्ज करें", "કૃપા કરીને ફોન દાખલ કરો"]
    static let msgPleaseSelectState = ["Please select state", "कृपया राज्य चुनें", "કૃપા કરીને રાજ્ય પસંદ કરો"]
    static let msgPleaseSelectCity = ["Please select city", "कृपया शहर चुनें", "કૃપા કરીને શહેર પસંદ કરો"]
    static let msgPleaseSelectTehsil = ["Please select tehsil", "कृपया तहसील का चयन करें", "કૃપા કરી તહસીલ પસંદ કરો"]
    static let msgPleaseSelectVillage = ["Please select village", "कृपया गाँव का चयन करें", "કૃપા કરીને ગામ પસંદ કરો"]
    static let msgPleaseEnterPinCode = ["Please enter pin code", "कृपया पिन कोड दर्ज करें", "કૃપા કરીને પિન કોડ દાખલ કરો"]
    static let msgPleaseEnterFullAddress = ["Please enter full address", "कृपया पूरा पता दर्ज करें", "કૃપા કરી પૂર્ણ સરનામું દાખલ કરો"]
    static let msgOrderSuccessfully = ["You have placed order successfully", "आपने आदेश को सफलतापूर्वक रखा है", "તમે સફળતાપૂર્વક ઓર્ડર આપ્યો છે"]

    /*~~~~~ Product Details View ~~~~~*/
    static let strProductDeatils = ["Product Detail", "वस्तु की बारीकी", "વસ્તુ ની માહિતી"]
    static let strBUYNOW = ["BUY NOW", "अभी खरीदें", "હમણાં ખરીદો"]
    static let strADDTOCART = ["ADD TO CART", "बैग में डाले", "બેગ માં ઉમેરો"]
    static let strCALLTOGIVEORDER = ["CALL TO GIVE ORDER", "ऑडर देने के लिए कॉल करे", "ઓર્ડર આપવા માટે કોલ કરો"]
    static let msgOfferInformation = ["This offer has been applied to this product!", "यह ऑफ़र इस उत्पादन पर लागू किया गया है!", "આ ઓફર આ પ્રોડક્ટ પર લાગુ કરવામાં આવી છે!"]
    static let strFree = ["free", "नि: शुल्क", "મફત"]
    static let strAboutProduct = ["About Product", "उत्पादन के बारे में", "પ્રોડક્ટ વિશે"]
    static let strValuebyAdmin = ["Value by Admin", "मान व्यवस्थापक द्वारा", "એડમિન દ્વારા મૂલ્ય"]
    static let strSimilarProduct = ["Similar Product", "समान उत्पादन", "સમાન ઉત્પાદનો"]
    static let msgProductOutofStock = ["Product is out of stock now.", "उत्पादन अब आउट ऑफ स्टॉक है।", "ઉત્પાદન હવે સ્ટોકની બહાર છે."]
    static let msgPleaseSelectQuantity = ["Please select at least one quantity.", "कृपया कम से कम एक मात्रा का चयन करें।", "કૃપા કરીને ઓછામાં ઓછો એક જથ્થો પસંદ કરો."]
    
    /*~~~~~ Crop Selection View ~~~~~*/
    static let strCLEAR = ["CLEAR", "साफ़", "સાફ કરો"]
    static let strAPPLYFILTER = ["APPLY FILTER", "फिल्टर लागू करें", "અરજી ફિલ્ટર"]
    
    /*~~~~~ Left View ~~~~~*/
    static let strShareApp = ["Share App", "ऐप शेयर करें", "શેર એપ્લિકેશન"]
    static let strLogout = ["Logout", "लॉग आउट", "લૉગ આઉટ"]
    static let msgLogoutConfirm = ["Are you sure want logout?", "क्या आप वाकई लॉगआउट करना चाहते हैं?", "શું તમે ખરેખર લૉગઆઉટ કરવા માંગો છો?"]
    
    static let strTakePhoto = ["Take photo", "फोटो लो", "ફોટો લો"]
    static let strCameraRoll = ["Camera roll", "कैमरा रोल", "કેમેરા રોલ"]
    static let strPhotolibrary = ["Photo library", "फोटो लाइब्रेरी", "ફોટો લાઇબ્રેરી"]
}
