//
//  Constant.swift
//  KhedutBolo
//
//  Created by Trivedi Sagar on 10/18/19.
//  Copyright © 2019 mac. All rights reserved.
//

import Foundation
import UIKit


let appVersion = mainBundle.infoDictionary?[kBundleShortVersionKey] as? String
var osVersion = UIDevice.current.systemVersion

/*~~~~~ main bundle path ~~~~~*/
let mainBundle = Bundle.main

/*~~~~~ define all dictionary keys ~~~~~*/
let kBundleShortVersionKey = "CFBundleShortVersionString"

/*~~~~~ ApDelegate object ~~~~~*/
let kAppDelegate        = UIApplication.shared.delegate as! AppDelegate

/*~~~~~ API URL ~~~~~*/
let BASE_URL            = "http://khedutbolo.shineinfosoft.in/api/"
let kUnAthorisedStatusCode = 111

/*~~~~~ UserDefault define ~~~~~*/
let kLanguage           = "Language"

/*~~~~~ RGB Color ~~~~~*/
func RGB(red:CGFloat, green:CGFloat, blue:CGFloat, alpha:CGFloat) -> UIColor {
    return UIColor(displayP3Red: red/255.0, green: green/255.0, blue: blue/255.0, alpha: alpha)
}

let kAppThemeGreenDarkColor = RGB(red: 0, green: 101, blue: 58, alpha: 1)
let kAppThemeGreenLightColor = RGB(red: 148, green: 189, blue: 7, alpha: 1)
let kAppThemeYellowColor = RGB(red: 255, green: 231, blue: 0, alpha: 1)
let kAppStatusBarColor = RGB(red: 1, green: 72, blue: 42, alpha: 1)
let kCurrentOrderLightGray = RGB(red: 232, green: 234, blue: 239, alpha: 1)

/*~~~~~ Loading size ~~~~~*/
let kLoaderSize = CGSize(width: 50, height: 50)


/*~~~~~ segue identifiers ~~~~~*/
let kPushToLoginSegueID                      = "PushToLoginSegueID"
let kPushToVarifyOTPSegueID                  = "PushToVarifyOTPSegueID"
let kPushToRegisterSegueID                   = "PushToRegisterSegueID"
let kPushToAllProdSegueID                    = "PushToAllProdSegueID"
let kPushToOffersSegueID                     = "PushToOffersSegueID"
let kPushToCropSegueID                       = "PushToCropSegueID"
let kPushToCropSolutionSegueID               = "PushToCropSolutionSegueID"
let kPushToCropSubSegueID                    = "PushToCropSubSegueID"
let kPushToCompanyListSegueID                = "PushToCompanyListSegueID"
let kPushToSubCategoriesSegueID              = "PushToSubCategoriesSegueID"
let kPushToNotificationSegueID               = "PushToNotificationSegueID"
let kPushToProductSegueID                    = "PushToProductSegueID"
let kPushToSearchSegueID                     = "PushToSearchSegueID"

/*~~~~~ tableview cell identifier constant ~~~~~*/
let leftMenuCell            = "LeftMenuCell"
let offersListCell          = "OffersListCell"
let categoryListCell        = "CategoryListCell"
let orderListCell           = "OrderListCell"
let orderCell               = "OrderCell"
let notificationCell        = "NotificationCell"
let cartCell                = "CartCell"
let productTechCell         = "ProductTechCell"
let filterCell              = "FilterCell"
let walletListCell          = "WalletListCell"
let cropSolutionListCell    = "CropSolutionListCell"

/*~~~~~ collectionview cell identifier constant ~~~~~*/
let titleSubtitleReusableView   = "TitleSubtitleReusableView"

let offersCCell               = "OffersCCell"
let solutionCCell             = "SolutionCCell"
let homeProductCCell          = "HomeProductCCell"
let prodCatCCell              = "ProdCatCCell"
let similarProdCCell          = "SimilarProdCCell"
let cropListCCell             = "CropListCCell"
let companyListCCell          = "CompanyListCCell"

/*~~~~~ current device size ~~~~~*/
let SystemSize = UIScreen.main.bounds.size

/*~~~~~ constant variable define ~~~~~*/
let PHONE_MAX_CHAR_SIZE = 10
let Maximum_Data_Display_Home = 6

/*~~~~~ custom font name ~~~~~*/
let kPoppin_SemiBold = "Poppins-SemiBold"
let kPoppin_Regular = "Poppins-Regular"


/*~~~~~ custom font name ~~~~~*/
func kCustom_Font(fontName : String, fontSize : CGFloat) -> UIFont {
    return UIFont(name:fontName, size:fontSize)!
}

/*~~~~~ place holder image define ~~~~~*/
let kProductImagePlaceHolder = UIImage.init(named: "imgProduct")
let kCompanyImagePlaceHolder = UIImage.init(named: "imgCompany")
let kUserImagePlaceHolder = UIImage.init(named: "profile_img")


/*~~~~~ Notification center define ~~~~~*/

let kUpdateHomeCartCountNC = "UpdateHomeCartCountNC"
let kUpdateProductViewCartCountNC = "UpdateProductViewCartCountNC"
let kDismissLoaderView = "DismissLoaderView"
let kLogoutMoveLoginPage = "LogoutMoveLoginPage"

let kHomeViewUpdateNeed = "HomeViewUpdateNeed"

var kHelpPhoneNumber = "9898853228"
var kOrderPhoneNumber = "9898853228"
