//
//  StaticURL.swift
//  ClipDart


import Foundation

struct StaticURL {
    

    #if SANDBOX
   
    static let APIBase         = "http://khedutbolo.com/"//khedutbolo.com //http://khedutbolo.shineinfosoft.in/
    static let ImagePath       = "http://khedutbolo.shineinfosoft.in/storage/"
   
    #else
    static let APIBase          = "http://khedutbolo.com/"
    static let ImagePath        = "http://khedutbolo.shineinfosoft.in/storage/"
 
    #endif
  
}
