//
//  MyProfileViewCtr.swift
//  KhedutBolo
//
//  Created by Trivedi Sagar on 11/5/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import DropDown
import SDWebImage

class MyProfileViewCtr: UIViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var btnImgPic: UIButton!

    @IBOutlet weak var imgEdit: UIImageView!
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var viewSaperatorName: UIView!
    
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var viewSaperatorPhone: UIView!
  
    @IBOutlet weak var lblSelectLang: UILabel!
    @IBOutlet weak var lblSelectedLanguage: UILabel!

    @IBOutlet weak var viewSelectedLan: UIView!
    @IBOutlet weak var viewLanguage: UIStackView!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnOk: UIButton!
    
    @IBOutlet weak var btnEnglish: UIButton!
    @IBOutlet weak var btnHindi: UIButton!
    @IBOutlet weak var btnGujarati: UIButton!
    
    @IBOutlet weak var lblEnglish: UILabel!
    @IBOutlet weak var imgEnglish: UIImageView!
    @IBOutlet weak var lblHindi: UILabel!
    @IBOutlet weak var imgHindi: UIImageView!
    @IBOutlet weak var lblGujrati: UILabel!
    @IBOutlet weak var imgGujrati: UIImageView!
    
    @IBOutlet weak var tfState: SkyFloatingLabelTextField!{
        didSet{
            tfState.titleFormatter = { $0 }
            tfState.titleColor = .lightGray//kAppThemeGreenDarkColor
            tfState.titleFont = kCustom_Font(fontName: kPoppin_SemiBold, fontSize: 17.0)
            tfState.placeholderFont = kCustom_Font(fontName: kPoppin_Regular, fontSize: 17.0)
            
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
            imageView.contentMode = .scaleAspectFit
            imageView.image = UIImage.init(named: "dropDownBlack")
            
            self.tfState.rightViewMode = UITextField.ViewMode.always
            self.tfState.rightView = imageView
            
        }
    }
    @IBOutlet weak var tfCity: SkyFloatingLabelTextField!{
        didSet{
            tfCity.titleFormatter = { $0 }
            tfCity.titleColor = .lightGray//kAppThemeGreenDarkColor
            tfCity.titleFont = kCustom_Font(fontName: kPoppin_SemiBold, fontSize: 17.0)
            tfCity.placeholderFont = kCustom_Font(fontName: kPoppin_Regular, fontSize: 17.0)
            
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
            imageView.contentMode = .scaleAspectFit
            imageView.image = UIImage.init(named: "dropDownBlack")
            
            self.tfCity.rightViewMode = UITextField.ViewMode.always
            self.tfCity.rightView = imageView
            
        }
    }
    @IBOutlet weak var tfTehsil: SkyFloatingLabelTextField!{
        didSet{
            tfTehsil.titleFormatter = { $0 }
            tfTehsil.titleColor = .lightGray//kAppThemeGreenDarkColor
            tfTehsil.titleFont = kCustom_Font(fontName: kPoppin_SemiBold, fontSize: 17.0)
            tfTehsil.placeholderFont = kCustom_Font(fontName: kPoppin_Regular, fontSize: 17.0)
            
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
            imageView.contentMode = .scaleAspectFit
            imageView.image = UIImage.init(named: "dropDownBlack")
            
            self.tfTehsil.rightViewMode = UITextField.ViewMode.always
            self.tfTehsil.rightView = imageView
            
        }
    }
    @IBOutlet weak var tfVillage: SkyFloatingLabelTextField!{
        didSet{
            tfVillage.titleFormatter = { $0 }
            tfVillage.titleColor = .lightGray//kAppThemeGreenDarkColor
            tfVillage.titleFont = kCustom_Font(fontName: kPoppin_SemiBold, fontSize: 17.0)
            tfVillage.placeholderFont = kCustom_Font(fontName: kPoppin_Regular, fontSize: 17.0)
            
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
            imageView.contentMode = .scaleAspectFit
            imageView.image = UIImage.init(named: "dropDownBlack")
            
            self.tfVillage.rightViewMode = UITextField.ViewMode.always
            self.tfVillage.rightView = imageView
        }
    }
    
    @IBOutlet weak var btnStateDrop: UIButton!
    @IBOutlet weak var btnCityDrop: UIButton!
    @IBOutlet weak var btnTehsilDrop: UIButton!
    @IBOutlet weak var btnVillageDrop: UIButton!

    // MARK: - constants & variables
    var arryStateData = [StateDataModel]()
    var arryCityData = [CityDataModel]()
    var arryTehsilData = [TehsilDataModel]()
    var arryVillageData = [VillageDataModel]()
    
    var myPickerView : UIPickerView!
    var toolBar : UIToolbar!
    
    var selectedStateIndex = 0
    var currentStateIndex = 0
    
    var selectedCityIndex = 0
    var currentCityIndex = 0
    
    var selectedTehsilIndex = 0
    var currentTehsilIndex = 0
    
    var selectedVillageIndex = 0
    var currentVillageIndex = 0
    
    var strStateId = ""
    var strCityId = ""
    var strTehsilId = ""
    var strVillageId = ""
    
    var imagePicker: ImagePicker!
    var profileImage:UIImage!
    
    // MARK: - UIView Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.setupInitialView()
        self.imagePicker = ImagePicker(presentationController: self, delegate: self)

    }
    override func viewDidDisappear(_ animated: Bool) {
        GlobalMethods.updateLanguageForCurrentUser()
    }
    
    //MARK:- Class Methods and Functions
    func setupInitialView(){
        
        btnOk.layer.cornerRadius = 10
        btnOk.layer.masksToBounds = true
        
        btnEdit.layer.cornerRadius = 10
        btnEdit.layer.masksToBounds = true
        
        btnSave.layer.cornerRadius = 10
        btnSave.layer.masksToBounds = true
        
        
        self.setViewDesign(isEditable: false) //false
        
        txtName.text = kAppDelegate.userLoginData.name
        txtPhone.text = kAppDelegate.userLoginData.mobile
        tfState.text = kAppDelegate.userLoginData.state
        tfCity.text = kAppDelegate.userLoginData.city
        tfTehsil.text = kAppDelegate.userLoginData.tehsil
        tfVillage.text = kAppDelegate.userLoginData.village
        
        strStateId = kAppDelegate.userLoginData.state_id
        strCityId = kAppDelegate.userLoginData.city_id
        strTehsilId = kAppDelegate.userLoginData.tehsil_id
        strVillageId = kAppDelegate.userLoginData.village_id
   
        imgProfile.sd_setImage(with: URL(string: kAppDelegate.userLoginData.image), placeholderImage: kUserImagePlaceHolder)

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.imgProfile.layer.cornerRadius = self.imgProfile.bounds.width/2
            self.imgProfile.layer.masksToBounds = true
        }
        
        txtPhone.isUserInteractionEnabled = false
        
        btnEnglish.tag = Language.kEnglish.rawValue
        btnHindi.tag = Language.kHindi.rawValue
        btnGujarati.tag = Language.kGujarati.rawValue
        
        if kAppDelegate.userLoginData.language == "en" {
            setLanguageView(withTypeId: 0)
        }else if kAppDelegate.userLoginData.language == "hi" {
            setLanguageView(withTypeId: 1)
        }else {
            setLanguageView(withTypeId: 2)
        }
        
        self.setupStringView()
        
        self.getStateApi()

    }
    
    func setViewDesign(isEditable: Bool){
        
//        self.view.endEditing(true)
//        if isEditable == true{
//            /*viewSaperatorName.isHidden = false
//            viewSaperatorPhone.isHidden = false*/
//            print("value:\(isEditable)")
//            viewSelectedLan.isHidden = true
//            viewLanguage.isHidden = false
//
//            self.setViewHideShowWithAnimarion(view: self.btnSave, hidden: false)
//            self.setViewHideShowWithAnimarion(view: self.btnEdit, hidden: true)
//            self.setViewHideShowWithAnimarion(view: self.btnOk, hidden: true)
//            self.setViewHideShowWithAnimarion(view: self.imgEdit, hidden: false)
//
//            tfState.placeholder = StringFile.strSelectState[kAppDelegate.userCurrentLanguage]
//            tfCity.placeholder = StringFile.strSelectCity[kAppDelegate.userCurrentLanguage]
//            tfTehsil.placeholder = StringFile.strSelectTehsil[kAppDelegate.userCurrentLanguage]
//            tfVillage.placeholder = StringFile.strSelectVillage[kAppDelegate.userCurrentLanguage]
//            self.setViewHideShowWithAnimarion(view: tfState.rightView!, hidden: false)
//            self.setViewHideShowWithAnimarion(view: tfCity.rightView!, hidden: false)
//            self.setViewHideShowWithAnimarion(view: tfTehsil.rightView!, hidden: false)
//            self.setViewHideShowWithAnimarion(view: tfVillage.rightView!, hidden: false)
//
//
//        }else{
            /*viewSaperatorName.isHidden = true
            viewSaperatorPhone.isHidden = true*/
//        isEditable = true
            print("value:\(isEditable)")
            viewSelectedLan.isHidden = false
            viewLanguage.isHidden = true
            
            self.setViewHideShowWithAnimarion(view: self.btnSave, hidden: true)
            self.setViewHideShowWithAnimarion(view: self.btnEdit, hidden: false)
            self.setViewHideShowWithAnimarion(view: self.btnOk, hidden: false)
            self.setViewHideShowWithAnimarion(view: self.imgEdit, hidden: true)
            
            tfState.placeholder = StringFile.strState[kAppDelegate.userCurrentLanguage]
            tfCity.placeholder = StringFile.strCity[kAppDelegate.userCurrentLanguage]
            tfTehsil.placeholder = StringFile.strTehsil[kAppDelegate.userCurrentLanguage]
            tfVillage.placeholder = StringFile.strVillage[kAppDelegate.userCurrentLanguage]
            self.setViewHideShowWithAnimarion(view: tfState.rightView!, hidden: true)
            self.setViewHideShowWithAnimarion(view: tfCity.rightView!, hidden: true)
            self.setViewHideShowWithAnimarion(view: tfTehsil.rightView!, hidden: true)
            self.setViewHideShowWithAnimarion(view: tfVillage.rightView!, hidden: true)
        
     //   }
        
        txtName.isUserInteractionEnabled = isEditable
        tfState.isUserInteractionEnabled = isEditable
        tfCity.isUserInteractionEnabled = isEditable
        tfTehsil.isUserInteractionEnabled = isEditable
        tfVillage.isUserInteractionEnabled = isEditable
        
        btnImgPic.isUserInteractionEnabled = isEditable
        
        btnStateDrop.isUserInteractionEnabled = isEditable
        btnCityDrop.isUserInteractionEnabled = isEditable
        btnVillageDrop.isUserInteractionEnabled = isEditable
        btnTehsilDrop.isUserInteractionEnabled = isEditable
        
        if kAppDelegate.userCurrentLanguage == 0{
            lblSelectedLanguage.text = StringFile.strEnglishTitleSmall
        }else if kAppDelegate.userCurrentLanguage == 1{
            lblSelectedLanguage.text = StringFile.strHindiTitle
        }else{
            lblSelectedLanguage.text = StringFile.strGujaratiTitle
        }
    }
    
    func setupStringView(){
        
        lblTitle.text = StringFile.strMyProfile[kAppDelegate.userCurrentLanguage]
        lblName.text = StringFile.strName[kAppDelegate.userCurrentLanguage]
        txtName.placeholder = StringFile.strPlaceEnterName[kAppDelegate.userCurrentLanguage]
        lblPhone.text = StringFile.strPhoneNumber[kAppDelegate.userCurrentLanguage]
        txtPhone.placeholder = StringFile.strPlaceEnterPhone[kAppDelegate.userCurrentLanguage]
        lblSelectLang.text = StringFile.strSelectLanguageSmall[kAppDelegate.userCurrentLanguage]
        lblEnglish.text = StringFile.strEnglishTitle
        lblHindi.text = StringFile.strHindiTitle
        lblGujrati.text = StringFile.strGujaratiTitle
        btnEdit.setTitle(StringFile.strEditSmall[kAppDelegate.userCurrentLanguage], for: .normal)
        btnSave.setTitle(StringFile.strSaveSmall[kAppDelegate.userCurrentLanguage], for: .normal)
        btnOk.setTitle(StringFile.strOK[kAppDelegate.userCurrentLanguage], for: .normal)
    }
    
    func showDropDown(withType type: Int){
        //type:
        // 1 = State, 2 = City, 3 = Tehsil, 4 - Village
        
        for view in self.view.subviews{
            if view == self.myPickerView{
                toolBar.removeFromSuperview()
                myPickerView.removeFromSuperview()
            }
        }
        
        self.myPickerView = UIPickerView(frame:CGRect(x: 0, y: UIScreen.main.bounds.size.height - 216, width: self.view.frame.size.width, height: 216))
        self.myPickerView.delegate = self
        self.myPickerView.dataSource = self
        self.myPickerView.backgroundColor = UIColor.white
        self.myPickerView.tag = type
        self.view.addSubview(self.myPickerView)
        toolBar = UIToolbar(frame:CGRect(x: 0, y: UIScreen.main.bounds.size.height - 216, width: self.view.frame.size.width, height: 50))
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = kAppThemeGreenDarkColor
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: StringFile.strDone[kAppDelegate.userCurrentLanguage], style: .plain, target: self, action: #selector(self.doneClick(sender:)))
        doneButton.tag = type
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: StringFile.strCancel[kAppDelegate.userCurrentLanguage], style: .plain, target: self, action: #selector(self.cancelClick(sender:)))
        cancelButton.tag = type
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        self.view.addSubview(toolBar)
        
        if type == 1{
            self.myPickerView.selectRow(selectedStateIndex, inComponent: 0, animated: true)
        }else if type == 2{
            self.myPickerView.selectRow(selectedCityIndex, inComponent: 0, animated: true)
        }else if type == 3{
            self.myPickerView.selectRow(selectedTehsilIndex, inComponent: 0, animated: true)
        }else if type == 4{
            self.myPickerView.selectRow(selectedVillageIndex, inComponent: 0, animated: true)
        }
    
    }
    
    @objc func doneClick(sender: UIButton!) {
        
        self.view.endEditing(true)
        if sender.tag == 1{
            selectedStateIndex = currentStateIndex
            tfState.text = self.arryStateData[selectedStateIndex].DTName
            self.strStateId = self.arryStateData[selectedStateIndex].STCode
            self.tfCity.text = ""
            self.tfTehsil.text = ""
            self.tfVillage.text = ""
            self.arryCityData.removeAll()
            self.arryTehsilData.removeAll()
            self.arryVillageData.removeAll()
            self.getCityApi(withState: self.arryStateData[selectedStateIndex].STCode)
        }else if sender.tag == 2{
            selectedCityIndex = currentCityIndex
            tfCity.text = self.arryCityData[selectedCityIndex].DTName
            self.strCityId = self.arryCityData[selectedCityIndex].DTCode
            self.tfTehsil.text = ""
            self.tfVillage.text = ""
            self.arryTehsilData.removeAll()
            self.arryVillageData.removeAll()
            self.getTehsilApi(withCity: self.arryCityData[selectedCityIndex].DTCode)

        }else if sender.tag == 3{
            selectedTehsilIndex = currentTehsilIndex
            tfTehsil.text = self.arryTehsilData[selectedTehsilIndex].SDTName
            self.strTehsilId = self.arryTehsilData[selectedTehsilIndex].SDTCode
            self.tfVillage.text = ""
            self.arryVillageData.removeAll()
            self.getVillageApi(withTehsil: self.arryTehsilData[selectedTehsilIndex].SDTCode, isFirstTime: false)
        }else if sender.tag == 4{
            selectedVillageIndex = currentVillageIndex
            tfVillage.text = self.arryVillageData[selectedVillageIndex].Name
            self.strVillageId = self.arryVillageData[selectedVillageIndex].TVCode
        }
        
        toolBar.removeFromSuperview()
        myPickerView.removeFromSuperview()
    }
    
    @objc func cancelClick(sender: UIButton!) {
        
        if sender.tag == 1{
            if selectedStateIndex != currentStateIndex{
                currentStateIndex = selectedStateIndex
                tfState.text = self.arryStateData[currentStateIndex].DTName
                strStateId = self.arryStateData[currentStateIndex].STCode
            }
        }else if sender.tag == 2{
            if selectedCityIndex != currentCityIndex{
                currentCityIndex = selectedCityIndex
                tfCity.text = self.arryCityData[currentCityIndex].DTName
                strCityId = self.arryCityData[currentCityIndex].DTCode
            }
        }else if sender.tag == 3{
            if selectedTehsilIndex != currentTehsilIndex{
                currentTehsilIndex = selectedTehsilIndex
                tfTehsil.text = self.arryTehsilData[currentTehsilIndex].SDTName
                strTehsilId = self.arryTehsilData[currentTehsilIndex].SDTCode
            }
        }else if sender.tag == 4{
            if selectedVillageIndex != currentVillageIndex{
                currentVillageIndex = selectedVillageIndex
                tfVillage.text = self.arryVillageData[currentVillageIndex].Name
                strVillageId = self.arryVillageData[currentVillageIndex].TVCode
            }
        }
        
        toolBar.removeFromSuperview()
        myPickerView.removeFromSuperview()
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    //MARK:- IBActions
    @IBAction func btnBackClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnChangPicClick(_ sender: Any) {
        self.imagePicker.present(from: sender as! UIView)
    }
    
    @IBAction func btnEditClick(_ sender: Any) {
      //  isEditing = true
      //  isEditable = true
        lblSelectLang.isHidden = true
        viewSelectedLan.isHidden = true
        viewLanguage.isHidden = true
                
        self.setViewHideShowWithAnimarion(view: self.btnSave, hidden: false)
        self.setViewHideShowWithAnimarion(view: self.btnEdit, hidden: true)
        self.setViewHideShowWithAnimarion(view: self.btnOk, hidden: true)
        self.setViewHideShowWithAnimarion(view: self.imgEdit, hidden: false)
        
        tfState.placeholder = StringFile.strSelectState[kAppDelegate.userCurrentLanguage]
        tfCity.placeholder = StringFile.strSelectCity[kAppDelegate.userCurrentLanguage]
        tfTehsil.placeholder = StringFile.strSelectTehsil[kAppDelegate.userCurrentLanguage]
        tfVillage.placeholder = StringFile.strSelectVillage[kAppDelegate.userCurrentLanguage]
        self.setViewHideShowWithAnimarion(view: tfState.rightView!, hidden: false)
        self.setViewHideShowWithAnimarion(view: tfCity.rightView!, hidden: false)
        self.setViewHideShowWithAnimarion(view: tfTehsil.rightView!, hidden: false)
        self.setViewHideShowWithAnimarion(view: tfVillage.rightView!, hidden: false)
        
        
      //  self.setViewDesign(isEditable: true)
    }
    
    @IBAction func btnOkClick(_ sender: Any) {
        if btnOk.isHidden{
            self.setViewDesign(isEditable: false)
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    @IBAction func btnStateChangeClick(_ sender: Any) {
        if arryStateData.count > 0{
            self.showDropDown(withType: 1)
        }
    }

    @IBAction func btnCityChangeClick(_ sender: Any) {
        if arryCityData.count > 0{
            self.showDropDown(withType: 2)
        }
    }
    
    @IBAction func btnTehsilChangeClick(_ sender: Any) {
        if arryTehsilData.count > 0{
            self.showDropDown(withType: 3)
        }
    }
    
    @IBAction func btnVillageChangeClick(_ sender: Any) {
        if arryVillageData.count > 0{
            self.showDropDown(withType: 4)
        }
    }
    
    @IBAction func btnLanguageClick(_ sender: UIButton) {
        self.view.endEditing(true)
        if kAppDelegate.userCurrentLanguage != sender.tag{
            self.setLanguageView(withTypeId: sender.tag)
        }
    }
    
    func setLanguageView(withTypeId sender:Int){
         
        imgEnglish.image = UIImage.init(named: "uncheck")
        imgHindi.image = UIImage.init(named: "uncheck")
        imgGujrati.image = UIImage.init(named: "uncheck")
         
        if sender == Language.kEnglish.rawValue{
            imgEnglish.image = UIImage.init(named: "check")
        }else if sender == Language.kHindi.rawValue{
            imgHindi.image = UIImage.init(named: "check")
        }else{
            imgGujrati.image = UIImage.init(named: "check")
        }
        kAppDelegate.userCurrentLanguage = sender
        self.setupStringView()
        self.setViewDesign(isEditable: true)

    }
    
    @IBAction func btnSaveClick(_ sender: Any) {
        self.setViewDesign(isEditable: false)
        self.view.endEditing(true)
         if (txtName.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            self.ShowAlertDisplay(titleObj: StringFile.strAppName[kAppDelegate.userCurrentLanguage], messageObj: StringFile.msgPleaseEnterName[kAppDelegate.userCurrentLanguage], viewcontrolelr: self)
         }else if (txtPhone.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            self.ShowAlertDisplay(titleObj: StringFile.strAppName[kAppDelegate.userCurrentLanguage], messageObj: StringFile.msgEnterPhoneNumber[kAppDelegate.userCurrentLanguage], viewcontrolelr: self)
         }else if txtPhone.text!.count < PHONE_MAX_CHAR_SIZE{
             self.ShowAlertDisplay(titleObj: StringFile.strAppName[kAppDelegate.userCurrentLanguage], messageObj: StringFile.msgEnterPhoneNumberValid[kAppDelegate.userCurrentLanguage], viewcontrolelr: self)
         }else if (tfState.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            self.ShowAlertDisplay(titleObj: StringFile.strAppName[kAppDelegate.userCurrentLanguage], messageObj: StringFile.msgPleaseSelectState[kAppDelegate.userCurrentLanguage], viewcontrolelr: self)
         }else if (tfCity.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            self.ShowAlertDisplay(titleObj: StringFile.strAppName[kAppDelegate.userCurrentLanguage], messageObj: StringFile.msgPleaseSelectCity[kAppDelegate.userCurrentLanguage], viewcontrolelr: self)
         }else if (tfTehsil.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            self.ShowAlertDisplay(titleObj: StringFile.strAppName[kAppDelegate.userCurrentLanguage], messageObj: StringFile.msgPleaseSelectTehsil[kAppDelegate.userCurrentLanguage], viewcontrolelr: self)
         }else if (tfVillage.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            self.ShowAlertDisplay(titleObj: StringFile.strAppName[kAppDelegate.userCurrentLanguage], messageObj: StringFile.msgPleaseSelectVillage[kAppDelegate.userCurrentLanguage], viewcontrolelr: self)
         }
         else{
            self.updateProfileWithImage()
         }
    }
    
    //MARK:- Web Services Call
    
    func updateProfileWithImage(){
        GlobalMethods.presentLoaderViewCtr(withViewController: self)
       
        let selectedlang = GlobalMethods.getCurrentLanguageCode()
        let reqParam = [ParameterKeys.Id: kAppDelegate.userLoginData.id, ParameterKeys.Mobile: self.txtPhone.text!, ParameterKeys.Name: txtName.text!, ParameterKeys.Address: "", ParameterKeys.Language: selectedlang, ParameterKeys.Sid : strStateId, ParameterKeys.Cid : strCityId, ParameterKeys.Tid : strTehsilId, ParameterKeys.Vid : strVillageId, ParameterKeys.User_Id : kAppDelegate.userLoginData.id] as [String : Any]
        
              var imgArryModel = [ImageUploadModel]()
        
        if self.profileImage != nil {
            let imgArray = [["Image" : self.profileImage!, "ImageKey" : "image"]]
            imgArryModel = [ImageUploadModel](dictionaryArray: imgArray as [NSDictionary])
        }
        
        
         WebServiceCall.uploadImageWithURL(route: APIRouter.UpdateProfile(reqParam), imageArray : imgArryModel) { (_ responseCode: Int?, _ responseValue : [String : AnyObject]?, _ error : Error?) in
            
            GlobalMethods.dismissLoaderView()
            
            if (responseValue != nil) {
                
                let responseModel = ResponseModelNew(dictionary: responseValue! as NSDictionary)
                
                if responseModel.status == 200 && responseModel.dataDict != nil {
                    kAppDelegate.userLoginData.delete()
                    kAppDelegate.userLoginData = LoginUserModel(dictionary: responseModel.dataDict! as NSDictionary)
                    kAppDelegate.userLoginData.save()
                    NotificationCenter.default.post(name: Notification.Name(kHomeViewUpdateNeed), object: nil)
                    GlobalMethods.showAlert(alertTitle: StringFile.strThankYou[kAppDelegate.userCurrentLanguage], alertMessage: StringFile.msgProfileUpdateSuccess[kAppDelegate.userCurrentLanguage], viewCtr: self, completion: nil)
                    self.setViewDesign(isEditable: false)
                }
                else {
                    GlobalMethods.showAlert(alertTitle: StringFile.strError[kAppDelegate.userCurrentLanguage], alertMessage: StringFile.msgNeedProperDetails[kAppDelegate.userCurrentLanguage], viewCtr: self, completion: nil)
                }
            }
            else {
                GlobalMethods.showErrorFromAPI(responseCode, responseValue, error, viewCtr: self)
            }
        }
    }
    
    
    func getStateApi(){
        GlobalMethods.presentLoaderViewCtr(withViewController: self)
        
        WebServiceCall.cancelAllAPIRequest()

        WebServiceCall.callMethodWithURL(route: APIRouter.GetState(custId:kAppDelegate.userLoginData.id,Luang:kAppDelegate.userCurrentLanguage)) { (_ responseCode: Int?, _ responseValue : [String : AnyObject]?, _ error : Error?) in
            
            GlobalMethods.dismissLoaderView()
            
             if (responseValue != nil) {
                
                let responseModel = StateModel(dictionary: responseValue! as NSDictionary)

                if responseModel.status == 200{
                    self.arryStateData = responseModel.data
                    
                    if self.strStateId.count > 0 && self.arryCityData.count == 0{
                        self.setUpCurrentStateIndex()
                        self.getCityApi(withState: self.strStateId,isNeedToLoader: false)
                    }
                }
                else {
                   GlobalMethods.showAlert(alertTitle: StringFile.strError[kAppDelegate.userCurrentLanguage], alertMessage: StringFile.msgNeedProperDetails[kAppDelegate.userCurrentLanguage], viewCtr: self, completion: nil)
               }
           }
           else {
               GlobalMethods.showErrorFromAPI(responseCode, responseValue, error, viewCtr: self)
           }
        }
    }
    
    func setUpCurrentStateIndex(){
        for (index, objState) in arryStateData.enumerated() {
            if objState.STCode == self.strStateId{
                selectedStateIndex = index
                currentStateIndex = index
            }
        }
    }
    
 

    func getCityApi(withState stateId: String, isNeedToLoader: Bool = true){
        
        if isNeedToLoader{
            GlobalMethods.presentLoaderViewCtr(withViewController: self)
        }
        WebServiceCall.cancelAPIRequest(arryLastPathComponents: [APIRouter.GetCities([:]).path])

        let reqParam = [ParameterKeys.State_id : stateId,ParameterKeys.Customer_id: kAppDelegate.userLoginData.id,ParameterKeys.Language:kAppDelegate.userCurrentLanguage] as [String : Any]

        WebServiceCall.callMethodWithURL(route: APIRouter.GetCities(reqParam)) { (_ responseCode: Int?, _ responseValue : [String : AnyObject]?, _ error : Error?) in
            
            if isNeedToLoader{
                GlobalMethods.dismissLoaderView()
            }
             if (responseValue != nil) {
                
                let responseModel = CityModel(dictionary: responseValue! as NSDictionary)

                if responseModel.status == 200{
                    self.arryCityData = responseModel.data
                    
                    if self.strCityId.count > 0 && self.arryTehsilData.count == 0{
                        self.getTehsilApi(withCity: self.strCityId, isNeedToLoader: false)
                        self.setUpCurrentCityIndex()
                    }

                }
                else {
                   GlobalMethods.showAlert(alertTitle: StringFile.strError[kAppDelegate.userCurrentLanguage], alertMessage: StringFile.msgNeedProperDetails[kAppDelegate.userCurrentLanguage], viewCtr: self, completion: nil)
               }
           }
           else {
               GlobalMethods.showErrorFromAPI(responseCode, responseValue, error, viewCtr: self)
           }
        }
    }
    
    func setUpCurrentCityIndex(){
        for (index, objCity) in arryCityData.enumerated() {
            if objCity.DTCode == self.strCityId{
                selectedCityIndex = index
                currentCityIndex = index
            }
        }
    }
    

    func getTehsilApi(withCity cityId: String, isNeedToLoader: Bool = true){
        if isNeedToLoader{
            GlobalMethods.presentLoaderViewCtr(withViewController: self)
        }

        WebServiceCall.callMethodWithURL(route: APIRouter.GetTehsil(withCityId: cityId)) { (_ responseCode: Int?, _ responseValue : [String : AnyObject]?, _ error : Error?) in
            
            if isNeedToLoader{
                GlobalMethods.dismissLoaderView()
            }
             if (responseValue != nil) {
                
                let responseModel = TehsilModel(dictionary: responseValue! as NSDictionary)

                if responseModel.status == 200{
                    self.arryTehsilData = responseModel.data
                    
                    if self.strTehsilId.count > 0 && self.arryVillageData.count == 0{
                        self.getVillageApi(withTehsil: self.strTehsilId, isFirstTime: true, isNeedToLoader: false)
                        self.setUpCurrentTehsilIndex()
                    }
                }
                else {
                   GlobalMethods.showAlert(alertTitle: StringFile.strError[kAppDelegate.userCurrentLanguage], alertMessage: StringFile.msgNeedProperDetails[kAppDelegate.userCurrentLanguage], viewCtr: self, completion: nil)
               }
           }
           else {
               GlobalMethods.showErrorFromAPI(responseCode, responseValue, error, viewCtr: self)
           }
        }
    }
    
    func setUpCurrentTehsilIndex(){
        
        for (index, objTehsil) in arryTehsilData.enumerated() {
            if objTehsil.SDTName == kAppDelegate.userLoginData.tehsil{
                selectedTehsilIndex = index
                currentTehsilIndex = index
            }
        }
    }
    
    
 
    func getVillageApi(withTehsil tehsilId: String, isFirstTime: Bool, isNeedToLoader: Bool = true){
        
        if isNeedToLoader{
            GlobalMethods.presentLoaderViewCtr(withViewController: self)
        }

        WebServiceCall.callMethodWithURL(route: APIRouter.GetVillage(withTehsilId: tehsilId)) { (_ responseCode: Int?, _ responseValue : [String : AnyObject]?, _ error : Error?) in
            
            GlobalMethods.dismissLoaderView()
            
             if (responseValue != nil) {
                
                let responseModel = VillageModel(dictionary: responseValue! as NSDictionary)

                if responseModel.status == 200{
                    self.arryVillageData = responseModel.data
                    if isFirstTime{
                        self.setUpCurrentVillageIndex()
                    }
                }
                else {
                   GlobalMethods.showAlert(alertTitle: StringFile.strError[kAppDelegate.userCurrentLanguage], alertMessage: StringFile.msgNeedProperDetails[kAppDelegate.userCurrentLanguage], viewCtr: self, completion: nil)
               }
           }
           else {
               GlobalMethods.showErrorFromAPI(responseCode, responseValue, error, viewCtr: self)
           }
        }
    }
    
    func setUpCurrentVillageIndex(){
        for (index, objVillage) in arryVillageData.enumerated() {
            if objVillage.TVCode == self.strVillageId{
                selectedVillageIndex = index
                currentVillageIndex = index
            }
        }
    }
    
}


extension MyProfileViewCtr:UITextFieldDelegate{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
    
        if textField == self.tfState {
            return false
        }else if textField == self.tfCity{
            return false
        } else if textField == self.tfTehsil{
            return false
        } else if textField == self.tfVillage{
            return false
        }
        return true
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool
    {
        
        let  char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        
        if (isBackSpace == -92) {
            return true
        }
        
        if textField == txtPhone{
            return string.isNumber
        }
        
        return true
    }
}

extension MyProfileViewCtr: ImagePickerDelegate {

    func didSelect(image: UIImage?) {
        if image != nil{
            profileImage = image
            self.imgProfile.image = image
        }
    }
}

extension MyProfileViewCtr : UIPickerViewDelegate, UIPickerViewDataSource{
    //MARK:- PickerView Delegate & DataSource
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        var returnCount = 0
        if pickerView.tag == 1{
            returnCount = self.arryStateData.count
        } else if pickerView.tag == 2{
            returnCount = self.arryCityData.count
        } else if pickerView.tag == 3{
            returnCount = self.arryTehsilData.count
        } else if pickerView.tag == 4{
            returnCount = self.arryVillageData.count
        }
        return returnCount
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        var passString = ""
        if pickerView.tag == 1{
            passString = self.arryStateData[row].DTName
        } else if pickerView.tag == 2{
            passString = self.arryCityData[row].DTName
        } else if pickerView.tag == 3{
            passString = self.arryTehsilData[row].SDTName
        } else if pickerView.tag == 4{
            passString = self.arryVillageData[row].Name
        }
        return passString
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView.tag == 1{
            let passString = self.arryStateData[row].DTName
            tfState.text = passString
            currentStateIndex = row
            strStateId = self.arryStateData[currentStateIndex].STCode

        } else if pickerView.tag == 2{
            let passString = self.arryCityData[row].DTName
            tfCity.text = passString
            currentCityIndex = row
            strCityId = self.arryCityData[currentCityIndex].DTCode

        } else if pickerView.tag == 3{
            let passString = self.arryTehsilData[row].SDTName
            tfTehsil.text = passString
            currentTehsilIndex = row
            strTehsilId = self.arryTehsilData[currentTehsilIndex].SDTCode

        } else if pickerView.tag == 4{
            let passString = self.arryVillageData[row].Name
            tfVillage.text = passString
            currentVillageIndex = row
            strVillageId = self.arryVillageData[currentVillageIndex].TVCode
        }
    }

}
