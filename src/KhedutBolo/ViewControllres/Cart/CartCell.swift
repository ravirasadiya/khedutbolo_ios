//
//  CartCell.swift
//  KhedutBolo
//
//  Created by Trivedi Sagar on 11/2/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit

class CartCell: UITableViewCell {
    
    // MARK: - define IBOutlet start
    @IBOutlet weak var viewBack: UIView!
    @IBOutlet weak var viewImgBack: UIView!

    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var lblPrise: UILabel!
    @IBOutlet weak var lblOldPrise: UILabel!
    @IBOutlet weak var lblOldPriseCheckmark: UILabel!
    @IBOutlet weak var lblPerset: UILabel!
    @IBOutlet weak var lblCompanyName: UILabel!
    @IBOutlet weak var lblSellarName: UILabel!
    @IBOutlet weak var lblQtyTitle: UILabel!
    @IBOutlet weak var lblQty: UILabel!
  
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        
        imgProduct.layer.cornerRadius = 10
        imgProduct.layer.masksToBounds = true
        
        viewImgBack.dropShadow(color: UIColor.darkGray, opacity: 0.4, shadowRadius: 5.0, cornerRadius: 10.0)
        lblQtyTitle.text = "\(StringFile.strQty[kAppDelegate.userCurrentLanguage]) :"
    }
    
}
