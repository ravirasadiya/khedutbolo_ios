//
//  CartViewCtr.swift
//  KhedutBolo
//
//  Created by Trivedi Sagar on 11/2/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit
import EmptyDataSet_Swift

class CartViewCtr: UIViewController {
    
    // MARK: - define IBOutlet
    @IBOutlet weak var lblTopTitle: UILabel!
    @IBOutlet weak var tblCart: UITableView!
    @IBOutlet weak var viewBottom: UIView!
    @IBOutlet weak var btnContinue: UIButton!
    @IBOutlet weak var btnCheckout: UIButton!
    
    var objCart = CartObjectModel()
    var arryCart = [CartDataModel]()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.setupInitialView()
        
    }
    
    //MARK:- Class Methods and Functions
    func setupInitialView(){
        
        tblCart.registerCell(cellIDs: [cartCell], isDynamicHeight: true, estimatedHeight: 60)
        lblTopTitle.text = StringFile.strShoppingCart[lan]//[kAppDelegate.userCurrentLanguage]
        viewBottom.layer.cornerRadius = 10
        viewBottom.layer.masksToBounds = true
        
        viewBottom.isHidden = true
        
        btnContinue.backgroundColor = UIColor.black
        btnCheckout.backgroundColor = kAppThemeGreenDarkColor
        btnContinue.setTitle(StringFile.strContinueShopping[lan], for: .normal)//[kAppDelegate.userCurrentLanguage], for: .normal)
        btnCheckout.setTitle(StringFile.strProceedCheckout[lan], for: .normal)//[kAppDelegate.userCurrentLanguage], for: .normal)
        
        self.getCartListAPI(withCustId: kAppDelegate.userLoginData.id)

    }
    
    func moveToCheckOutPage(){
        let obj_CheckoutView = self.storyboard?.instantiateViewController(withIdentifier: "CheckoutViewCtr") as! CheckoutViewCtr
        obj_CheckoutView.passedTotalAmount = self.objCart.total_order_price
        obj_CheckoutView.passedGSTAmount = self.objCart.gst_price
        obj_CheckoutView.passedWalletAmount = "\(self.objCart.total_wallet)"
        obj_CheckoutView.passedPrice = "\(self.objCart.total)"
        obj_CheckoutView.passedWalletPoint = "\(self.objCart.total_point)"
        self.navigationController?.pushViewController(obj_CheckoutView, animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    //MARK:- IBActions
    @IBAction func clickOnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func clickOnCheckout(_ sender: Any) {
        self.moveToCheckOutPage()
    }
    
    @IBAction func clickOnContinue(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    //MARK:-  Web Services
    func getCartListAPI(withCustId cId: String){
      
        GlobalMethods.presentLoaderViewCtr(withViewController: self)
        

        WebServiceCall.callMethodWithURL(route: APIRouter.GetShowCart(withCustId: cId)) { (_ responseCode: Int?, _ responseValue : [String : AnyObject]?, _ error : Error?) in
            GlobalMethods.dismissLoaderView()
            
            if (responseValue != nil) {
                
                let responseModel = CartModel(dictionary: responseValue! as NSDictionary)
                
                if responseModel.status == 200{
                    self.objCart = responseModel.data
                    self.arryCart = responseModel.data.carts
                    self.afterGetCartResponse()
                }
                else {
                    self.afterGetCartResponse()
                }
            }
            else {
                GlobalMethods.showErrorFromAPI(responseCode, responseValue, error, viewCtr: self)
            }
        }
    }
    
    func afterGetCartResponse(){
        if arryCart.count != 0{
            self.tblCart.reloadData()
            self.setViewHideShowWithAnimarion(view: viewBottom, hidden: false)
        }else{
            tblCart.emptyDataSetSource = self
            tblCart.emptyDataSetDelegate = self
            self.setViewHideShowWithAnimarion(view: viewBottom, hidden: true)
            self.tblCart.reloadData()
        }
    }
    //deleteProductCart
    func deleteOrderFromCart(withIndex arryIndex: Int){
              
        let objCart = self.arryCart[arryIndex]
        GlobalMethods.presentLoaderViewCtr(withViewController: self)
        

        let reqParam = [ParameterKeys.Product_id : objCart.id, ParameterKeys.Customer_id : kAppDelegate.userLoginData.id, ParameterKeys.Attribute_id : objCart.attribute_id, ParameterKeys.delete_price : objCart.price, ParameterKeys.Total_price : objCart.total_price, ParameterKeys.User_Id : kAppDelegate.userLoginData.id] as [String : Any]

        WebServiceCall.callMethodWithURL(route: APIRouter.DeleteOrderFromCart(reqParam)) { (_ responseCode: Int?, _ responseValue : [String : AnyObject]?, _ error : Error?) in
            GlobalMethods.dismissLoaderView()
            
            if (responseValue != nil) {
                
                let responseModel = CartModel(dictionary: responseValue! as NSDictionary)
                
                if responseModel.status == 200{
                    self.arryCart.remove(at: arryIndex)
                    self.objCart = responseModel.data
                    kAppDelegate.cartCount = self.objCart.count
                    self.afterGetCartResponse()
                }
                else {
                    GlobalMethods.showAlert(alertTitle: StringFile.strError[kAppDelegate.userCurrentLanguage], alertMessage: StringFile.msgNeedProperDetails[kAppDelegate.userCurrentLanguage], viewCtr: self, completion: nil)
                }
            }
            else {
                GlobalMethods.showErrorFromAPI(responseCode, responseValue, error, viewCtr: self)
            }
        }
    }
    

    
}

extension CartViewCtr : UITableViewDelegate, UITableViewDataSource {

    //MARK:- UITableView Methods
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arryCart.count
    }

    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        let cell = tableView.dequeueReusableCell(withIdentifier: cartCell, for: indexPath) as! CartCell
        let objCart = arryCart[indexPath.row]
        cell.lblProductName.text = objCart.name
        cell.lblPrise.text = "₹\(objCart.sale_price)"
        cell.lblOldPrise.text = objCart.price
        cell.lblQty.text = objCart.quantity
        cell.lblPerset.text = StringFile.strPerSet[kAppDelegate.userCurrentLanguage]
        cell.lblCompanyName.text = "\(StringFile.strBy[lan]) : \(objCart.dealer_name)"
        cell.imgProduct.sd_setImage(with: objCart.coverImgUrl, placeholderImage: kProductImagePlaceHolder)
        cell.lblSellarName.text = "\(StringFile.strSellerName[kAppDelegate.userCurrentLanguage]) : \(StringFile.strAppName[kAppDelegate.userCurrentLanguage])"
        return cell
    }

    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){

        GlobalMethods.pushToProductCtr(withViewController: self, animated: true, productId: "\(arryCart[indexPath.row].id)", attributeId: "\(arryCart[indexPath.row].attribute_id)")
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            // handle delete (by removing the data from your array and updating the tableview)
            
            GlobalMethods.showAlert(alertTitle: "\(StringFile.strAppName[kAppDelegate.userCurrentLanguage])", alertMessage: "\(StringFile.msgRemoveCartConfirmation[kAppDelegate.userCurrentLanguage]) \(self.arryCart[indexPath.row].name)?", okActionTitle:StringFile.strRemove[kAppDelegate.userCurrentLanguage], cancelActionTitle: StringFile.strCancel[kAppDelegate.userCurrentLanguage], viewCtr: self, successCompletion: {
                if self.arryCart.count != 0 {
                    kAppDelegate.cartCount = self.arryCart.count - 1
                }
                self.deleteOrderFromCart(withIndex: indexPath.row)
            }) {
                //
            }
        }
    }
    
}

// MARK: - Empty Data Set Methods
extension CartViewCtr : EmptyDataSetSource, EmptyDataSetDelegate {
    
    func emptyDataSetShouldDisplay(_ scrollView: UIScrollView) -> Bool {
        return true
    }
    
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        let str = StringFile.strNoDataFound[kAppDelegate.userCurrentLanguage]
        let font = kCustom_Font(fontName: kPoppin_SemiBold, fontSize: 18.0)
        let attributes = [NSAttributedString.Key.font: font]
        let attributedStr = NSAttributedString(string: str, attributes: attributes)
        return attributedStr
    }
    
    func image(forEmptyDataSet scrollView: UIScrollView) -> UIImage? {
        return UIImage.init(named: "nodata")
    }
    
    func spaceHeight(forEmptyDataSet scrollView: UIScrollView) -> CGFloat {
        return 20.0
    }
}
