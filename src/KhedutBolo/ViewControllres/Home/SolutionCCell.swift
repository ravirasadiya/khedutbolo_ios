//
//  SolutionCCell.swift
//  KhedutBolo
//
//  Created by Trivedi Sagar on 10/25/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit

class SolutionCCell: UICollectionViewCell {

    // MARK: - define IBOutlet start
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgTitle: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
