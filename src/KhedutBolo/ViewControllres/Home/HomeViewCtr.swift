//
//  HomeViewCtr.swift
//  KhedutBolo
//
//  Created by Trivedi Sagar on 10/25/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit

class HomeViewCtr: UIViewController {

    // MARK: - define IBOutlet
    @IBOutlet weak var lblTopTitle: UILabel!
    @IBOutlet weak var lblCartCount: UILabel!
    @IBOutlet weak var cvOffers: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    @IBOutlet weak var lblSelSolution: UILabel!
    @IBOutlet weak var cvSolution: UICollectionView!
//    @IBOutlet weak var topLableSelSolutionConstraint: NSLayoutConstraint!

    @IBOutlet weak var lblSepratorAfterSol: UILabel!
    
    @IBOutlet weak var lblProductCat: UILabel!
    @IBOutlet weak var cvProductsCat: UICollectionView!
    @IBOutlet weak var hightProdCatCVConstraint: NSLayoutConstraint!
    @IBOutlet weak var topLableProductCatConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var lblTradingProd: UILabel!
    @IBOutlet weak var cvProductsTrading: UICollectionView!
    @IBOutlet weak var hightProdTradingCVConstraint: NSLayoutConstraint!
    @IBOutlet weak var btnMore: UIButton!
    @IBOutlet weak var topLableProductTreadingConstraint: NSLayoutConstraint!

    
    @IBOutlet weak var lblCompanyList: UILabel!
    @IBOutlet weak var cvCompanyList: UICollectionView!
    @IBOutlet weak var hightCompanyListCVConstraint: NSLayoutConstraint!
    @IBOutlet weak var topLableCompanyListConstraint: NSLayoutConstraint!

    @IBOutlet weak var btnAll: UIButton!
    @IBOutlet weak var btnCall: UIButton!
    
    @IBOutlet weak var hightMainViewConstraint: NSLayoutConstraint!
    
    //MARK:- Variables
    var arrProductOffer = [ProductOfferModel]()
    var arryCropSol = [CropDataModel]()
    
    var arrMainList = [ProductModel]()
    var arrManageProduct = [ProductDetailModel]()
    var arrCompany = [CompanyModel]()
    var arrTredingProduct = [ProductDetailModel]()
    
    var p_cat = 0
    var p_tranding = 0
    var p_company = 0
    
    var offerTimer: Timer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNeedsStatusBarAppearanceUpdate()
        // Do any additional setup after loading the view.
        
        cvOffers.register(UINib(nibName: offersCCell, bundle: nil), forCellWithReuseIdentifier: offersCCell)
        cvSolution.register(UINib(nibName: solutionCCell, bundle: nil), forCellWithReuseIdentifier: solutionCCell)
        cvProductsCat.register(UINib(nibName: homeProductCCell, bundle: nil), forCellWithReuseIdentifier: homeProductCCell)
        
        cvProductsTrading.register(UINib(nibName: prodCatCCell, bundle: nil), forCellWithReuseIdentifier: prodCatCCell)
        
        cvCompanyList.register(UINib(nibName: offersCCell, bundle: nil), forCellWithReuseIdentifier: offersCCell)
        
       
        lblCartCount.isHidden = true
        
        /*~~~~~ notification  ~~~~~*/
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateHomeCartCount), name: NSNotification.Name(rawValue: kUpdateHomeCartCountNC), object: nil)
        
        /*~~~~~ notification  ~~~~~*/
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateHomeView), name: NSNotification.Name(rawValue: kHomeViewUpdateNeed), object: nil)

        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.setupInitialView()
        print(" lan 1 didload:\(lan)")
        setUpStringView()
        updateHomeCartCount()
    }
//    override func viewWillAppear(_ animated: Bool) {
//        setupInitialView()
//        if kAppDelegate.cartCount > 0{
//            lblCartCount.isHidden = false
//            lblCartCount.text = "\(kAppDelegate.cartCount)"
//        }else{
//            lblCartCount.isHidden = true
//        }
//
//        if self.arrProductOffer.count > 1{
//            self.setTimer()
//        }
//        print("lan userLogin2:\(kAppDelegate.userLoginData.language)")
//       // let main = GlobalMethods.GetupdateLanguageForCurrentUser()
//      //  print("lan main:\(main)")
//      //  lan = main
//        print(" lan 1 didload:\(lan)")
//        self.setUpStringView()
//    }
//    override func viewDidAppear(_ animated: Bool) {
//        if kAppDelegate.cartCount > 0{
//            lblCartCount.isHidden = false
//            lblCartCount.text = "\(kAppDelegate.cartCount)"
//        }else{
//            lblCartCount.isHidden = true
//        }
//
//        if self.arrProductOffer.count > 1{
//            self.setTimer()
//        }
//        GlobalMethods.updateLanguageForCurrentUser()
//        self.setUpStringView()
//
//    }
    
    override func viewDidDisappear(_ animated: Bool) {
        offerTimer?.invalidate()
    }
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    //MARK:- Class Methods and Functions
       func setupInitialView(){
        GlobalMethods.updateLanguageForCurrentUser()
        lblProductCat.textColor = kAppThemeGreenDarkColor
        lblTradingProd.textColor = kAppThemeGreenDarkColor
        lblCompanyList.textColor = kAppThemeGreenDarkColor
        
        btnAll.setTitleColor(kAppThemeGreenDarkColor, for: .normal)
        btnMore.setTitleColor(kAppThemeGreenDarkColor, for: .normal)
        btnCall.setTitle("", for: .normal)
        btnCall.backgroundColor = kAppThemeGreenDarkColor
        
        btnAll.layer.cornerRadius = 10.0
        btnAll.layer.borderWidth = 3
        btnAll.layer.borderColor = kAppThemeGreenDarkColor.cgColor
        btnAll.layer.masksToBounds = true
        
        btnMore.layer.cornerRadius = 10.0
        btnMore.layer.borderWidth = 3
        btnMore.layer.borderColor = kAppThemeGreenDarkColor.cgColor
        btnMore.layer.masksToBounds = true
        
        btnCall.layer.cornerRadius = btnCall.frame.size.width/2
        btnCall.layer.masksToBounds = true
        
        lblCartCount.layer.cornerRadius = lblCartCount.bounds.width/2
        lblCartCount.layer.masksToBounds = true
        
        self.pageControl.numberOfPages = 0
//        self.topLableSelSolutionConstraint.constant = 8
        self.getOfferApi()
        updateHomeCartCount()
    }
    
    func setUpStringView(){
        GlobalMethods.updateLanguageForCurrentUser()
        print("lan 1 setupString:\(lan)")
        lblTopTitle.text = StringFile.strAppNameCaps[lan]
        lblSelSolution.text = StringFile.strSelectBelowSolution[lan]//[kAppDelegate.userCurrentLanguage]
        lblProductCat.text = StringFile.strProductCategoriesManageProducts[lan]//[kAppDelegate.userCurrentLanguage]
        lblTradingProd.text = StringFile.strTrendingProductions[lan]//[kAppDelegate.userCurrentLanguage]
        lblCompanyList.text = StringFile.strCompanyList[lan]//[kAppDelegate.userCurrentLanguage]
        
        btnMore.setTitle("  \(StringFile.strMORE[lan])  ", for: .normal)//[kAppDelegate.userCurrentLanguage], for: .normal)
        btnAll.setTitle("\(StringFile.strALL[lan])", for: .normal)//[kAppDelegate.userCurrentLanguage], for: .normal)
        self.lblSelSolution.isHidden = self.arryCropSol.count > 0 ? false : true
        self.lblCompanyList.isHidden = self.arrCompany.count > 0 ? false : true
        self.btnAll.isHidden = self.arrCompany.count > 0 ? false : true
        self.lblProductCat.isHidden = self.arrManageProduct.count > 0 ? false : true
        self.lblTradingProd.isHidden = self.arrTredingProduct.count > 0 ? false : true
        self.btnMore.isHidden = self.arrTredingProduct.count > 0 ? false : true
       
    }
    
    @objc func updateHomeView(){
        self.getOfferApi()
    }
    
   @objc func updateHomeCartCount(){
        if kAppDelegate.cartCount > 0{
            self.lblCartCount.isHidden = false
            self.lblCartCount.text = "\(kAppDelegate.cartCount)"
        }
    }
    
    func setTimer(){
        offerTimer?.invalidate()
        offerTimer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(autoOfferCollectionScroll), userInfo: nil, repeats: true)
    }
    
    @objc func autoOfferCollectionScroll(){
            
        if pageControl.currentPage == self.arrProductOffer.count-1{
            cvOffers.scrollToItem(at:IndexPath(item: 0, section: 0), at: .right, animated: true)
        }else{
            cvOffers.scrollToItem(at:IndexPath(item: pageControl.currentPage + 1, section: 0), at: .right, animated: true)
        }
    }
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == kPushToOffersSegueID {
            let selectedIndex = sender as! Int
            let obj_OffersViewCtr = segue.destination as! OffersViewCtr
            obj_OffersViewCtr.passIdOfferId = self.arrProductOffer[selectedIndex].id
        }else if segue.identifier == kPushToCropSegueID {
            var selectedIdenx : Int = 0
            selectedIdenx = sender as! Int
            let solModel = arryCropSol[selectedIdenx]
            let obj_CropViewCtr = segue.destination as! CropViewCtr
            obj_CropViewCtr.objCropModel = solModel
        }
        else if segue.identifier == kPushToCompanyListSegueID{
            let obj_CompanyListViewCtr = segue.destination as! CompanyListViewCtr
            obj_CompanyListViewCtr.arrCompanyList = self.arrCompany

        }else if segue.identifier == kPushToSubCategoriesSegueID{
            var indexId = 0
            indexId = sender as! Int
            let obj_SubCategoriesViewCtr = segue.destination as! SubCategoriesViewCtr
            obj_SubCategoriesViewCtr.objProductPrev = self.arrManageProduct[indexId]
        } else if segue.identifier == kPushToAllProdSegueID{
            let obj_AllProductsViewCtr = segue.destination as! AllProductsViewCtr
            obj_AllProductsViewCtr.arrAllProduct = self.arrTredingProduct
        }
    }
    
    
    // MARK: - IBActions

    @IBAction func btnSearchSel(_ sender: Any) {
        self.performSegue(withIdentifier: kPushToNotificationSegueID, sender: self)
       // self.performSegue(withIdentifier: kPushToSearchSegueID, sender: self)
    }
    
    @IBAction func btnNotificationSel(_ sender: Any) {
        self.performSegue(withIdentifier: kPushToSearchSegueID, sender: self)
       // self.performSegue(withIdentifier: kPushToNotificationSegueID, sender: self)
    }
    
    @IBAction func btnCartSel(_ sender: Any) {
        let obj_CartView = self.storyboard?.instantiateViewController(withIdentifier: "CartViewCtr") as! CartViewCtr
        self.navigationController?.pushViewController(obj_CartView, animated: true)
    }
    
    @IBAction func btnMoreSel(_ sender: Any) {
        self.performSegue(withIdentifier: kPushToAllProdSegueID, sender: self)
    }
    
    @IBAction func btnAllSel(_ sender: Any) {
        self.performSegue(withIdentifier: kPushToCompanyListSegueID, sender: self)
    }
    
    @IBAction func btnCallSel(_ sender: Any) {
        if let url = URL(string: "tel://\(kHelpPhoneNumber)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            }
            else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    // MARK: - Web Services

    func getOfferApi(){
        
        GlobalMethods.presentLoaderViewCtr(withViewController: self)
        
        WebServiceCall.cancelAPIRequest(arryLastPathComponents: [APIRouter.GetOffers([:]).path])
        let reqParam = [ParameterKeys.User_Id : kAppDelegate.userLoginData.id] as [String : Any]

        WebServiceCall.callMethodWithURL(route: APIRouter.GetOffers(reqParam)) { (_ responseCode: Int?, _ responseValue : [String : AnyObject]?, _ error : Error?) in
            
             if (responseValue != nil) {
                           
               let responseModel = OfferService(dictionary: responseValue! as NSDictionary)
               
               if responseModel.status == 200 {
                
                self.arrProductOffer = responseModel.data
                self.cvOffers.reloadData()
                
                kHelpPhoneNumber = responseModel.mobile
                
                if self.arrProductOffer.count > 0{
//                    self.topLableSelSolutionConstraint.constant = self.cvOffers.bounds.height + 40
                }else{
//                    self.topLableSelSolutionConstraint.constant = 8
                }
                
                if self.arrProductOffer.count > 1{
                    self.setTimer()
                }
                
                self.pageControl.numberOfPages = self.arrProductOffer.count
                self.getCropSolutionApi()
                            
               }
               else {
                self.getCropSolutionApi()
                   GlobalMethods.showAlert(alertTitle: StringFile.strError[kAppDelegate.userCurrentLanguage], alertMessage: StringFile.msgNeedProperDetails[kAppDelegate.userCurrentLanguage], viewCtr: self, completion: nil)
               }
           }
           else {
                GlobalMethods.dismissLoaderView()
                GlobalMethods.showErrorFromAPI(responseCode, responseValue, error, viewCtr: self)
           }
        }
    }
    
    func getCropSolutionApi(){
            
        WebServiceCall.cancelAPIRequest(arryLastPathComponents: [APIRouter.GetCrop([:]).path])
        let reqParam = [ParameterKeys.User_Id : kAppDelegate.userLoginData.id] as [String : Any]

        WebServiceCall.callMethodWithURL(route: APIRouter.GetCrop(reqParam)) { (_ responseCode: Int?, _ responseValue : [String : AnyObject]?, _ error : Error?) in
            
             if (responseValue != nil) {
                           
               let responseModel = CropModel(dictionary: responseValue! as NSDictionary)
               
               if responseModel.status == 200 {
                
                self.arryCropSol = responseModel.data
                self.cvSolution.reloadData()
                
                self.getProductCategoriesApi()
                
               }
               else {
                self.getProductCategoriesApi()
                   GlobalMethods.showAlert(alertTitle: StringFile.strError[kAppDelegate.userCurrentLanguage], alertMessage: StringFile.msgNeedProperDetails[kAppDelegate.userCurrentLanguage], viewCtr: self, completion: nil)
               }
           }
           else {
                GlobalMethods.dismissLoaderView()

               GlobalMethods.showErrorFromAPI(responseCode, responseValue, error, viewCtr: self)
           }
        }
    }
    
    func getProductCategoriesApi(){
            
        WebServiceCall.callMethodWithURL(route: APIRouter.ProductCategories(withCustId: kAppDelegate.userLoginData.id)) { (_ responseCode: Int?, _ responseValue : [String : AnyObject]?, _ error : Error?) in

            GlobalMethods.dismissLoaderView()
            
             if (responseValue != nil) {
               let responseModel = ProductResponseModel(dictionary: responseValue! as NSDictionary)
               
               if responseModel.status == 200 {
                
                self.arrMainList = responseModel.data
                
                for objMain in self.arrMainList{
                    if objMain.type == ParameterKeys.ProductCat_ProductManage{
                        self.arrManageProduct = objMain.myProduct
                        self.p_cat = objMain.priority
                    }
                    
                    if objMain.type == ParameterKeys.Trending_Products{
                        print("Treading:\(objMain.myTreadingProduct.count)")
                        self.arrTredingProduct = objMain.myTreadingProduct
                        print("arr:\(self.arrTredingProduct.count)")
                        self.p_tranding = objMain.priority
                    }
                    
                    if objMain.type == ParameterKeys.Company_List{
                        self.arrCompany = objMain.myCompanyList
                        self.p_company = objMain.priority
                    }
                }
                self.setProductData()
               }
               else {

                   GlobalMethods.showAlert(alertTitle: StringFile.strError[kAppDelegate.userCurrentLanguage], alertMessage: StringFile.msgNeedProperDetails[kAppDelegate.userCurrentLanguage], viewCtr: self, completion: nil)
               }
           }
           else {
               GlobalMethods.showErrorFromAPI(responseCode, responseValue, error, viewCtr: self)
           }
        }
    }
    
    func setProductData(){
        self.cvCompanyList.reloadData()
        self.cvProductsCat.reloadData()
        self.cvProductsTrading.reloadData()
        self.setHightView()
        GlobalMethods.getCartCount()
    }
    
    func setHightView(){
     
        let cellHight = (SystemSize.width-50)/2 //50
        
        var objCount = arrManageProduct.count
        if objCount % 2 != 0 {
            objCount = objCount + 1
        }
        objCount = objCount/2
        hightProdCatCVConstraint.constant = CGFloat((Int(cellHight) * Int(objCount)) + 30)
        
        var objCountTreading = arrTredingProduct.count
        if objCountTreading % 2 != 0 {
            objCountTreading = objCountTreading + 1
        }
        
        if objCountTreading > Maximum_Data_Display_Home{
            objCountTreading = Maximum_Data_Display_Home
        }
        
        
        objCountTreading = objCountTreading/2
        hightProdTradingCVConstraint.constant = CGFloat((Int(cellHight) * Int(objCountTreading)) + 260)//CGFloat(250 * objCountTreading)
    
        let cellCompanyHight = (SystemSize.width-50)/2
        
        var cellCompanyHightNew = cellCompanyHight/2
        
        cellCompanyHightNew = cellCompanyHightNew + 20
        
        var objCountCompany = arrCompany.count
        if objCountCompany % 2 != 0 {
            objCountCompany = objCountCompany + 1
        }
        
        if objCountCompany > Maximum_Data_Display_Home{
            objCountCompany = Maximum_Data_Display_Home
        }
        
        objCountCompany = objCountCompany/2

        hightCompanyListCVConstraint.constant = CGFloat((Int(cellCompanyHightNew) * Int(objCountCompany)) + 20)
       
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
           
            self.hightMainViewConstraint.constant = self.lblSepratorAfterSol.frame.origin.y + self.hightProdCatCVConstraint.constant + self.hightProdTradingCVConstraint.constant + self.hightCompanyListCVConstraint.constant + 200
           
           
            if self.p_cat == 1{
                if self.p_tranding == 2{
                    self.topLableProductCatConstraint.constant = 20
                    self.topLableProductTreadingConstraint.constant = self.hightProdCatCVConstraint.constant + 64
                    self.topLableCompanyListConstraint.constant = self.hightProdCatCVConstraint.constant + self.hightProdTradingCVConstraint.constant + 100
                }else{
                    self.topLableProductCatConstraint.constant = 20
                    self.topLableCompanyListConstraint.constant = self.hightProdCatCVConstraint.constant + 64
                    self.topLableProductTreadingConstraint.constant = self.hightProdCatCVConstraint.constant + self.hightCompanyListCVConstraint.constant + 100
                }
            } else if self.p_tranding == 1{
                if self.p_cat == 2{
                    self.topLableProductTreadingConstraint.constant = 20
                    self.topLableProductCatConstraint.constant = self.hightProdTradingCVConstraint.constant + 64
                    self.topLableCompanyListConstraint.constant = self.hightProdTradingCVConstraint.constant + self.hightProdCatCVConstraint.constant + 120
                }else{
                    self.topLableProductCatConstraint.constant = 20
                    self.topLableCompanyListConstraint.constant = self.hightProdCatCVConstraint.constant + 64
                    self.topLableProductTreadingConstraint.constant = self.hightProdCatCVConstraint.constant + self.hightCompanyListCVConstraint.constant + 120
                }
                
            } else if self.p_company == 1{
                if self.p_cat == 2{
                    self.topLableCompanyListConstraint.constant = 20
                    self.topLableProductCatConstraint.constant = self.hightCompanyListCVConstraint.constant + 64
                    self.topLableProductTreadingConstraint.constant = self.hightCompanyListCVConstraint.constant + self.hightProdCatCVConstraint.constant + 100
                }else{
                    self.topLableCompanyListConstraint.constant = 20
                    self.topLableProductTreadingConstraint.constant = self.hightCompanyListCVConstraint.constant + 64
                    self.topLableProductCatConstraint.constant = self.hightCompanyListCVConstraint.constant + self.hightProdTradingCVConstraint.constant + 100
                }
            }else{
                // not called but
                print("order not Set")
                self.topLableProductCatConstraint.constant = 20
                self.topLableProductTreadingConstraint.constant = self.hightProdCatCVConstraint.constant + 60
                self.topLableCompanyListConstraint.constant = self.hightProdCatCVConstraint.constant + self.hightProdTradingCVConstraint.constant + 100
            }
            
            self.setUpStringView()

        }
        
    }

}

// MARK: - UICollectionView Delegate, DataSource Methods
extension HomeViewCtr : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        var totalCellCount = 0
        if collectionView == cvOffers{
            totalCellCount = self.arrProductOffer.count
        }else if collectionView == cvSolution{
            totalCellCount = arryCropSol.count
        }else if collectionView == cvProductsCat{
            totalCellCount = self.arrManageProduct.count
        }else if collectionView == cvProductsTrading{
            totalCellCount = self.arrTredingProduct.count
            if totalCellCount > Maximum_Data_Display_Home{
                totalCellCount = Maximum_Data_Display_Home
            }
        }else if collectionView == cvCompanyList{
            totalCellCount = self.arrCompany.count
            if totalCellCount > Maximum_Data_Display_Home{
                totalCellCount = Maximum_Data_Display_Home
            }
        }
        return totalCellCount
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == cvOffers{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: offersCCell, for: indexPath) as! OffersCCell
            let objOfferModel = arrProductOffer[indexPath.row]
            cell.imgOffers.sd_setImage(with: objOfferModel.coverImgUrl, placeholderImage: kProductImagePlaceHolder)
            return cell
            
        }else if collectionView == cvSolution{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: solutionCCell, for: indexPath) as! SolutionCCell
            let solModel = arryCropSol[indexPath.row]
            cell.lblTitle.text = solModel.name
            cell.imgTitle.sd_setImage(with: solModel.imgUrl, placeholderImage: kProductImagePlaceHolder)
            return cell
            
        }else if collectionView == cvProductsCat{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: homeProductCCell, for: indexPath) as! HomeProductCCell
            let objModel = self.arrManageProduct[indexPath.row]
            cell.lblName.text = objModel.name
            cell.imgProduct.sd_setImage(with: objModel.coverImgUrl, placeholderImage: kProductImagePlaceHolder)
            return cell
            
        }else if collectionView == cvProductsTrading{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: prodCatCCell, for: indexPath) as! ProdCatCCell
            let objModel = self.arrTredingProduct[indexPath.row]
            cell.lblName.text = objModel.name
            cell.lblSalePrice.text = "₹\(objModel.sale_price)"
            cell.lblPrice.text = "₹\(objModel.price)"
            cell.lblPerSet.text = StringFile.strPerSet[kAppDelegate.userCurrentLanguage]
            cell.lblPrice.textColor = UIColor.black
            cell.lblPerSet.textColor = UIColor.black
            cell.lblMarker.textColor = UIColor.black
            cell.lblDealerName.text = "\(StringFile.strBy[lan]) : \(objModel.company_name)"
            cell.imgProduct.sd_setImage(with: objModel.coverImgUrl, placeholderImage: kProductImagePlaceHolder)
            return cell
            
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: offersCCell, for: indexPath) as! OffersCCell
            let objModel = self.arrCompany[indexPath.row]
            cell.imgOffers.sd_setImage(with: objModel.coverImgUrl, placeholderImage: kCompanyImagePlaceHolder)
            cell.imgOffers.contentMode = .scaleAspectFit
            return cell
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == cvOffers{
            let cellSize  = CGSize(width: collectionView.bounds.width, height: collectionView.bounds.height)
            return cellSize
        }else if collectionView == cvSolution{
            let cellSize  = CGSize(width: (SystemSize.width-40)/3, height: collectionView.bounds.height)
            return cellSize
        }else if collectionView == cvProductsCat {
            let cellWidth = (SystemSize.width-50)/2
            let cellSize  = CGSize(width: cellWidth, height: cellWidth)
            return cellSize
        }else if collectionView == cvProductsTrading{
            let cellWidth = (SystemSize.width-50)/2
            let cellSize  = CGSize(width: cellWidth, height: cellWidth + 80)
            return cellSize
        }
        else{
            let cellWidth = (SystemSize.width-50)/2
            let cellSize  = CGSize(width: cellWidth, height: (cellWidth/2) + 20)
            return cellSize
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == cvOffers{
            self.performSegue(withIdentifier: kPushToOffersSegueID, sender: indexPath.row)
        }else if collectionView == cvSolution{
            self.performSegue(withIdentifier: kPushToCropSegueID, sender: indexPath.row)
        }else if collectionView == cvProductsCat{
            self.performSegue(withIdentifier: kPushToSubCategoriesSegueID, sender: indexPath.row)
        }else if collectionView == cvProductsTrading{
            let objModel = self.arrTredingProduct[indexPath.row]
            
            GlobalMethods.pushToProductCtr(withViewController: self, animated: true, productId: "\(objModel.id)")
            
        } else if collectionView == cvCompanyList {
            let objModel = self.arrCompany[indexPath.row]
            let obj_CatView = self.storyboard?.instantiateViewController(withIdentifier: "CompanyCategoryListViewCtr") as! CompanyCategoryListViewCtr
            obj_CatView.companyId = objModel.id
            obj_CatView.companyName = objModel.name
            self.navigationController?.pushViewController(obj_CatView, animated: true)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        if collectionView == cvOffers{
            let currentIdx = Int(collectionView.contentOffset.x / collectionView.frame.size.width)
            pageControl.currentPage = currentIdx
        }
    }
}
