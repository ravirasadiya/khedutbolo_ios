//
//  HomeProductCCell.swift
//  KhedutBolo
//
//  Created by Trivedi Sagar on 10/25/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit

class HomeProductCCell: UICollectionViewCell {

    // MARK: - define IBOutlet start
    @IBOutlet weak var viewBack: UIView!
    @IBOutlet weak var viewTitleLableBack: UIView!

    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgProduct: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
   
        self.viewBack.dropShadow(color: UIColor.lightGray, opacity: 0.4, shadowRadius: 3.0, cornerRadius: 5.0)
        
        imgProduct.layer.cornerRadius = 5
        imgProduct.layer.masksToBounds = true
        
//        self.viewTitleLableBack.dropShadow(color: UIColor.lightGray, opacity: 0.4, shadowRadius: 3.0, cornerRadius: 5.0)
               
    }
    
    
}
