//
//  SubCategoriesViewCtr.swift
//  KhedutBolo
//
//  Created by Trivedi Sagar on 1/23/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit
import EmptyDataSet_Swift

class SubCategoriesViewCtr: UIViewController {
    
    // MARK: - define IBOutlet
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var cvSubCatList: UICollectionView!
    
    var objProductPrev = ProductDetailModel()
    var arrProduct = [ProductDetailModel]()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        cvSubCatList.register(UINib(nibName: homeProductCCell, bundle: nil), forCellWithReuseIdentifier: homeProductCCell)
        
        self.setUpView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print(objProductPrev)
        print(arrProduct)

    }
    
    func setUpView(){
        lblTitle.text = objProductPrev.name
        self.getCategoriesAPI(withCatId: "\(objProductPrev.id)")
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    //MARK:- IBActions
       @IBAction func clickOnBack(_ sender: Any) {
           self.navigationController?.popViewController(animated: true)
       }
    
    //MARK:- Web Services
    func getCategoriesAPI(withCatId catId: String){

        GlobalMethods.presentLoaderViewCtr(withViewController: self)
        
        WebServiceCall.callMethodWithURL(route: APIRouter.GetSubCategories(withCatID: catId, custId: kAppDelegate.userLoginData.id)) { (_ responseCode: Int?, _ responseValue : [String : AnyObject]?, _ error : Error?) in

            GlobalMethods.dismissLoaderView()
            
            if (responseValue != nil) {
                
                let responseModel = SubCategoriesResponseModel(dictionary: responseValue! as NSDictionary)
                
                if responseModel.status == 200{
                    
                    self.arrProduct = responseModel.data
                    if responseModel.subcategory_status == 1 {
                        
                        let objDetailView = self.storyboard?.instantiateViewController(withIdentifier: "SubCatProductListViewCtr") as! SubCatProductListViewCtr
                        objDetailView.objProduct = self.objProductPrev
                        objDetailView.IsSubCatCalled = true
                        self.navigationController?.pushViewController(objDetailView, animated: false)

                        
                    } else {
                        self.apiResponseSet()
                    }
//                    self.apiResponseSet()
                }
                else {
                    self.apiResponseSet()
                   
                }
            }
            else {
                GlobalMethods.showErrorFromAPI(responseCode, responseValue, error, viewCtr: self)
            }
        }
    }
    
    func apiResponseSet(){
        if arrProduct.count != 0{
            self.cvSubCatList.reloadData()
        }else{
            cvSubCatList.emptyDataSetSource = self
            cvSubCatList.emptyDataSetDelegate = self
            self.cvSubCatList.reloadData()
        }
    }

}

// MARK: - UICollectionView Delegate, DataSource Methods
extension SubCategoriesViewCtr : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return  self.arrProduct.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: homeProductCCell, for: indexPath) as! HomeProductCCell
        let objModel = self.arrProduct[indexPath.row]
        cell.lblName.text = objModel.name
        cell.imgProduct.sd_setImage(with: objModel.coverImgUrl, placeholderImage: kProductImagePlaceHolder)
        return cell
       
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
               
        let cellWidth = (SystemSize.width-50)/2
        let cellSize  = CGSize(width: cellWidth, height: cellWidth)
        return cellSize

    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let objDetailView = self.storyboard?.instantiateViewController(withIdentifier: "SubCatProductListViewCtr") as! SubCatProductListViewCtr
        objDetailView.objProduct = arrProduct[indexPath.row]
        objDetailView.IsSubCatCalled = false
        self.navigationController?.pushViewController(objDetailView, animated: true)
    }
    
    
}

// MARK: - Empty Data Set Methods
extension SubCategoriesViewCtr : EmptyDataSetSource, EmptyDataSetDelegate{
    
    func emptyDataSetShouldDisplay(_ scrollView: UIScrollView) -> Bool {
        return true
    }
    
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        let str = StringFile.strNoDataFound[kAppDelegate.userCurrentLanguage]
        let font = kCustom_Font(fontName: kPoppin_SemiBold, fontSize: 18.0)
        let attributes = [NSAttributedString.Key.font: font]
        let attributedStr = NSAttributedString(string: str, attributes: attributes)
        return attributedStr
    }
    
    func image(forEmptyDataSet scrollView: UIScrollView) -> UIImage? {
        return UIImage.init(named: "nodata")
    }
    
    func spaceHeight(forEmptyDataSet scrollView: UIScrollView) -> CGFloat {
        return 20.0
    }
}
