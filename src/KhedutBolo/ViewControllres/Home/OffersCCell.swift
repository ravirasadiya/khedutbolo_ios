//
//  OffersCCell.swift
//  KhedutBolo
//
//  Created by Trivedi Sagar on 10/25/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit

class OffersCCell: UICollectionViewCell {

    // MARK: - define IBOutlet start
    @IBOutlet weak var imgOffers: UIImageView!
    @IBOutlet weak var viewBack: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.viewBack.dropShadow(color: UIColor.lightGray, opacity: 0.4, shadowRadius: 3.0, cornerRadius: 5.0)

        
        self.imgOffers.layer.cornerRadius = 5
        self.imgOffers.layer.masksToBounds = true
    }

}
