//
//  SubCatProductListViewCtr.swift
//  KhedutBolo
//
//  Created by Trivedi Sagar on 1/23/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit
import EmptyDataSet_Swift

class SubCatProductListViewCtr: UIViewController {
    
    // MARK: - define IBOutlet
    @IBOutlet weak var tblList: UITableView!
    @IBOutlet weak var lblTitle: UILabel!

    
    var allProductArray = [SolutionModel]()
    var arrCatProduct = [SolutionCategoriesModel]()
    var storedOffsets = [Int: CGFloat]()
    
    var objProduct = ProductDetailModel()
    var IsSubCatCalled : Bool = false


    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tblList.registerCell(cellIDs: [categoryListCell], isDynamicHeight: true, estimatedHeight: 100)

        self.setupInitialView()

    }
    
    //MARK:- Class Methods and Functions
    func setupInitialView(){
        lblTitle.text = objProduct.name
        self.getSubCategoriesProductAPI(withCatId: "\(objProduct.id)")

    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    //MARK:- IBActions
       @IBAction func clickOnBack(_ sender: Any) {
        
        if let viewControllers = self.navigationController?.viewControllers {
            if IsSubCatCalled {
                self.navigationController?.popToViewController(viewControllers[0], animated: true)
            } else {
                        // fail
                self.navigationController?.popViewController(animated: true)
            }
        }

//           self.navigationController?.popViewController(animated: true)
       }
    
    // MARK: - Web Service Function Methods
    func getSubCategoriesProductAPI(withCatId cId: String){

        GlobalMethods.presentLoaderViewCtr(withViewController: self)
        
        let reqParam = [ParameterKeys.Id : cId, ParameterKeys.User_Id : kAppDelegate.userLoginData.id] as [String : Any]

        WebServiceCall.callMethodWithURL(route: APIRouter.SelectedCategory(reqParam)) { (_ responseCode: Int?, _ responseValue : [String : AnyObject]?, _ error : Error?) in

            GlobalMethods.dismissLoaderView()
            
            if (responseValue != nil) {
                
                let responseModel = SolutionModelResponse(dictionary: responseValue! as NSDictionary)
                
                if responseModel.status == 200{
                    self.allProductArray = responseModel.data
                    self.setResponseProduct()
                }
                else {
                    self.setResponseProduct()
                }
            }
            else {
                GlobalMethods.showErrorFromAPI(responseCode, responseValue, error, viewCtr: self)
            }
        }
    }
    
    func setResponseProduct(){
        
        if IsSubCatCalled {
            
            let categorieNames = Array(Set(allProductArray.map({$0.technical_name})))
            
            for cat in categorieNames{
                
                let check = allProductArray.filter { $0.technical_name == cat}
                
                let catObj = SolutionCategoriesModel()
                catObj.cat_data = check
                catObj.categariesType = cat
                
                self.arrCatProduct.append(catObj)
            }
            
        }else{
            
            let categorieNames = Array(Set(allProductArray.map({$0.category_name})))
            
            for cat in categorieNames{
                
                let check = allProductArray.filter { $0.category_name == cat}
                
                let catObj = SolutionCategoriesModel()
                catObj.cat_data = check
                catObj.categariesType = cat
                
                self.arrCatProduct.append(catObj)
            }

        }
    
        self.reloadData()
        
    }

    func reloadData(){
        if arrCatProduct.count != 0{
            self.tblList.reloadData()
        }else{
            tblList.emptyDataSetSource = self
            tblList.emptyDataSetDelegate = self
            self.tblList.reloadData()
        }
    }



}


extension SubCatProductListViewCtr : UITableViewDelegate, UITableViewDataSource {

    //MARK:- UITableView Methods
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.arrCatProduct.count
    }

    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
    
        let cell = tableView.dequeueReusableCell(withIdentifier: categoryListCell, for: indexPath) as! CategoryListCell
        cell.isFromSolution = true
        cell.lblName.text = self.arrCatProduct[indexPath.row].categariesType
        cell.arraySolution = self.arrCatProduct[indexPath.row].cat_data
      //  cell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.row)
        cell.collectionViewOffset = storedOffsets[indexPath.row] ?? 0
        
        let cellWidth = (SystemSize.width-50)/2
        
        var objCountTreading = cell.arraySolution.count
        if objCountTreading % 2 != 0 {
            objCountTreading = objCountTreading + 1
        }
        objCountTreading = objCountTreading/2
     
        cell.hightCollectionView.constant = (cellWidth * CGFloat(objCountTreading)) + CGFloat((82 * objCountTreading))
        cell.cvProducts.dataSource = cell.self
        cell.cvProducts.delegate = cell.self
        cell.cvProducts.reloadData()
        cell.viewCtr = self

        return cell
    }

    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {}
}


// MARK: - Empty Data Set Methods
extension SubCatProductListViewCtr : EmptyDataSetSource, EmptyDataSetDelegate{
    
    func emptyDataSetShouldDisplay(_ scrollView: UIScrollView) -> Bool {
        return true
    }
    
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        let str = StringFile.strNoDataFound[kAppDelegate.userCurrentLanguage]
        let font = kCustom_Font(fontName: kPoppin_SemiBold, fontSize: 18.0)
        let attributes = [NSAttributedString.Key.font: font]
        let attributedStr = NSAttributedString(string: str, attributes: attributes)
        return attributedStr
    }
    
    func image(forEmptyDataSet scrollView: UIScrollView) -> UIImage? {
        return UIImage.init(named: "nodata")
    }
    
    func spaceHeight(forEmptyDataSet scrollView: UIScrollView) -> CGFloat {
        return 20.0
    }
}
