//
//  CropListCCell.swift
//  KhedutBolo
//
//  Created by Trivedi Sagar on 10/31/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit

class CropListCCell: UICollectionViewCell {
    
    // MARK: - define IBOutlet start
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgTitle: UIImageView!
    @IBOutlet weak var viewImageBack: UIView!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.viewImageBack.dropShadow(color: UIColor.lightGray, opacity: 0.4, shadowRadius: 3.0, cornerRadius: 5.0)
        
        self.imgTitle.layer.cornerRadius = 5
        self.imgTitle.layer.masksToBounds = true
        
        
    }

}
