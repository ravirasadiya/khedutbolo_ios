//
//  CropSolutionViewCtr.swift
//  KhedutBolo
//
//  Created by Trivedi Sagar on 10/31/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit
import EmptyDataSet_Swift
import Alamofire

class CropSolutionViewCtr: UIViewController {
    
    // MARK: - define IBOutlet
    @IBOutlet weak var tblList: UITableView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnRighMenu: UIButton!

    @IBOutlet weak var viewFilter: UIView!
    @IBOutlet weak var viewListFilterBG: UIView!
    @IBOutlet weak var tblFilter: UITableView!
    
    @IBOutlet weak var viewBottom: UIView!
    @IBOutlet weak var btnClear: UIButton!
    @IBOutlet weak var btnApplyFilter: UIButton!

    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var objSearchBar: UISearchBar!
    
    @IBOutlet weak var btnCart: UIButton!
    @IBOutlet weak var lblCartCount: UILabel!

    @IBOutlet weak var btnSearch: UIButton!
    
    
    var allProductArray = [SolutionModel]()
    var arrCatProduct = [SolutionCategoriesModel]()
    var arrAllCatProduct = [SolutionCategoriesModel]()

    var storedOffsets = [Int: CGFloat]()
    var passedTitle = ""

    var objCropSolution = CropSolutionDataModel()
    
    var arrMainList = [FilterDataModel]()
    var arryCompanyFilter = [FilterCompanyModel]()
    var arryTechnicalFilter = [FilterTechicalsModel]()
    
    var isSearch : Bool = false
    var isRemovingTextWithBackspace = true

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        tblList.registerCell(cellIDs: [categoryListCell], isDynamicHeight: true, estimatedHeight: 100)
        
        tblFilter.registerCell(cellIDs: [filterCell], isDynamicHeight: true, estimatedHeight: 40)
        
        tblFilter.tableFooterView = viewBottom
        self.setupInitialView()
        
        lblCartCount.isHidden = true
        btnClose.isHidden = true
        /*~~~~~ notification  ~~~~~*/
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateCartCount), name: NSNotification.Name(rawValue: kUpdateProductViewCartCountNC), object: nil)


    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.updateCartCount()
    }

    @objc func updateCartCount(){
        if kAppDelegate.cartCount > 0{
            lblCartCount.isHidden = false
            lblCartCount.text = "\(kAppDelegate.cartCount)"
        }else{
            lblCartCount.isHidden = true
        }
    }

    
    //MARK:- Class Methods and Functions
    func setupInitialView(){
        lblTitle.text = StringFile.strSelectSolution[lan]
        
        btnClear.layer.cornerRadius = 10
        btnClear.layer.masksToBounds = true
        
        btnApplyFilter.layer.cornerRadius = 10
        btnApplyFilter.layer.masksToBounds = true
        
        self.viewFilter.isHidden = true
        
        btnClear.setTitle(StringFile.strCLEAR[kAppDelegate.userCurrentLanguage], for: .normal)
        btnApplyFilter.setTitle(StringFile.strAPPLYFILTER[kAppDelegate.userCurrentLanguage], for: .normal)
        self.getSolutionProductAPI(withSolution: objCropSolution.id)
                
        lblCartCount.layer.cornerRadius = lblCartCount.bounds.width/2
        lblCartCount.layer.masksToBounds = true
        
        let textFieldInsideUISearchBar = objSearchBar.value(forKey: "searchField") as? UITextField
        textFieldInsideUISearchBar?.textColor = UIColor.white
        textFieldInsideUISearchBar!.leftViewMode = UITextField.ViewMode.never
        
        textFieldInsideUISearchBar?.clearButtonMode = .always

       
        textFieldInsideUISearchBar!.backgroundColor = kAppThemeGreenDarkColor
        textFieldInsideUISearchBar!.attributedPlaceholder = NSAttributedString(string: textFieldInsideUISearchBar?.placeholder ?? StringFile.strSearch[kAppDelegate.userCurrentLanguage], attributes: [NSAttributedString.Key.foregroundColor : UIColor.white])
        
        textFieldInsideUISearchBar?.font = kCustom_Font(fontName: kPoppin_Regular, fontSize: 16.0)
        textFieldInsideUISearchBar?.clearButtonMode = .never
       
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).tintColor = .white

        
        objSearchBar.backgroundImage = UIImage()
        objSearchBar.isHidden = true
        objSearchBar.resignFirstResponder()
        

    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - Web Service Function Methods
    
    func applyFilterApi(withComArray arryComp: NSMutableArray, withTech arryTech: NSMutableArray){

        GlobalMethods.presentLoaderViewCtr(withViewController: self)
        
        let solutionIdArray = NSMutableArray()
        solutionIdArray.add(objCropSolution.id)
        
        let parameters = ["companies": arryComp,
                          "technicals": arryTech, "solution_id": solutionIdArray, ParameterKeys.User_Id : kAppDelegate.userLoginData.id] as [String : Any]
                
        Alamofire.request("\(StaticURL.APIBase)/api/v1/filter", method: .post, parameters: parameters, encoding: JSONEncoding.default)
        .responseJSON { response in
            
            GlobalMethods.dismissLoaderView()
            
            self.allProductArray.removeAll()
            self.clickOnRightMenu(self)
            
            switch(response.result) {
                
                case .success(let JSON):
                    if let dict = JSON as? [String : AnyObject] {
                         let responseModel = SolutionModelResponse(dictionary: dict as NSDictionary)
                        
                        if responseModel.status == 200{
                            self.allProductArray = responseModel.data
                            self.setResponseProduct()
                        }
                        else {
                            self.setResponseProduct()
                        }
                    }
                break
                case .failure(let error):
                    self.setResponseProduct()
                    //GlobalMethods.showAlert(alertTitle: StringFile.strError[kAppDelegate.userCurrentLanguage], alertMessage: error.localizedDescription, viewCtr: self, completion: nil)
                break
            }
            
        }
    }
    
    func getSolutionProductAPI(withSolution sId: String){

        GlobalMethods.presentLoaderViewCtr(withViewController: self)
        
        self.allProductArray.removeAll()
        self.tblList.reloadData()
        let reqParam = [ParameterKeys.Id : sId, ParameterKeys.User_Id : kAppDelegate.userLoginData.id] as [String : Any]

        WebServiceCall.callMethodWithURL(route: APIRouter.Solution(reqParam)) { (_ responseCode: Int?, _ responseValue : [String : AnyObject]?, _ error : Error?) in
            
            self.getFilter(withCropId: sId)
            
            if (responseValue != nil) {
                
                let responseModel = SolutionModelResponse(dictionary: responseValue! as NSDictionary)
                
                if responseModel.status == 200{
                    self.allProductArray = responseModel.data
                    self.setResponseProduct()
                }
                else {
                    self.setResponseProduct()
                }
            }
            else {
                GlobalMethods.showErrorFromAPI(responseCode, responseValue, error, viewCtr: self)
            }
        }
    }
    
    func setResponseProduct(){
        
        let categorieNames = Array(Set(allProductArray.map({$0.technical_name})))
        
        self.arrCatProduct.removeAll()
        self.arrAllCatProduct.removeAll()
        
        for cat in categorieNames{
            
            let check = allProductArray.filter { $0.technical_name == cat}
            
            let catObj = SolutionCategoriesModel()
            catObj.cat_data = check
            catObj.categariesType = cat
            
            self.arrCatProduct.append(catObj)
            self.arrAllCatProduct.append(catObj)
        }
    
        self.reloadData()
        
    }

    func reloadData(){
        if arrCatProduct.count != 0{
            self.tblList.reloadData()
        }else{
            tblList.emptyDataSetSource = self
            tblList.emptyDataSetDelegate = self
            self.tblList.reloadData()
        }
    }

    func getFilter(withCropId cropId: String){
    
        WebServiceCall.callMethodWithURL(route: APIRouter.GetFilter(withCropId: cropId, userId: kAppDelegate.userLoginData.id)) { (_ responseCode: Int?, _ responseValue : [String : AnyObject]?, _ error : Error?) in
             
            GlobalMethods.dismissLoaderView()
            
            if (responseValue != nil) {
                let responseModel = FilterModel(dictionary: responseValue! as NSDictionary)
               
                if responseModel.status == 200 {
                
                    self.arrMainList = responseModel.data
                
                    self.arrMainList.reverse()
                    
                    for objMain in self.arrMainList{
                        
                        if objMain.type == ParameterKeys.Techicals{
                            self.arryTechnicalFilter = objMain.technicalFilter
                        }
                        
                        if objMain.type == ParameterKeys.Companies{
                            self.arryCompanyFilter = objMain.companyFilter
                        }
                    }
                
                    self.tblFilter.reloadData()
                }
                else {
                    GlobalMethods.showAlert(alertTitle: StringFile.strError[kAppDelegate.userCurrentLanguage], alertMessage: StringFile.msgNeedProperDetails[kAppDelegate.userCurrentLanguage], viewCtr: self, completion: nil)
                }
            }
            else {
                GlobalMethods.showErrorFromAPI(responseCode, responseValue, error, viewCtr: self)
            }
        }
    }
    
    func getSearchProduct(withSearchText strSearch:String){
    
        
    }

    
    
    //MARK:- IBActions
    @IBAction func clickOnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btncloseOnClick(_ sender: UIButton) {
       
    }
    @IBAction func clickOnRightMenu(_ sender: Any) {
        if viewFilter.isHidden{
            btnRighMenu.setImage(UIImage.init(named: "filter_after"), for: .normal)
            self.setViewHideShowWithAnimarion(view: viewFilter, hidden: false)
        }else{
            self.setViewHideShowWithAnimarion(view: viewFilter, hidden: true)
            btnRighMenu.setImage(UIImage.init(named: "filter_before"), for: .normal)
        }
    }
    
    @IBAction func clickOnBtnApplyFilter(_ sender: Any) {
        let selectedCompanyArray = NSMutableArray()
        let selectedTechArray = NSMutableArray()
        
        for objMain in self.arrMainList{
            if objMain.type == ParameterKeys.Companies{
                for objCompanyModel in objMain.companyFilter
                {
                    if objCompanyModel.isSelected{
                        selectedCompanyArray.add("\(objCompanyModel.company_id)")
                    }
                }
            }
            if objMain.type == ParameterKeys.Techicals{
                for objTechModel in objMain.technicalFilter
                {
                    if objTechModel.isSelected{
                        selectedTechArray.add("\(objTechModel.id)")
                    }
                }
            }
        }
        
        if selectedTechArray.count > 0 || selectedCompanyArray.count > 0{
            self.applyFilterApi(withComArray: selectedCompanyArray, withTech: selectedTechArray)
        }else{
            GlobalMethods.showAlert(alertTitle: StringFile.strError[kAppDelegate.userCurrentLanguage], alertMessage: StringFile.msgSelectFilterOptions[kAppDelegate.userCurrentLanguage], viewCtr: self, completion: nil)

        }
        
    }
    
    @IBAction func clickOnBtnClearFilter(_ sender: Any) {
       /* for objMain in self.arrMainList{
            if objMain.type == ParameterKeys.Companies{
                for objCompanyModel in objMain.companyFilter
                {
                    objCompanyModel.isSelected = false
                }
            }
            if objMain.type == ParameterKeys.Techicals{
                for objTechModel in objMain.technicalFilter
                {
                    objTechModel.isSelected = false
                }
            }
        }
        
        tblFilter.reloadData()*/
        self.clickOnRightMenu(self)
        self.getSolutionProductAPI(withSolution: objCropSolution.id)

    }
    
    
    @IBAction func clickOnBtnSearch(_ sender: Any) {
        
        if isSearch == false {
                        
            isSearch = true
            objSearchBar.isHidden = false
            objSearchBar.becomeFirstResponder()


        } else {
            
            isSearch = false
            objSearchBar.isHidden = true
            objSearchBar.resignFirstResponder()

        }
        
    }
    
    @IBAction func clickOnBtnCart(_ sender: Any) {
        
        let obj_CartView = self.storyboard?.instantiateViewController(withIdentifier: "CartViewCtr") as! CartViewCtr
        self.navigationController?.pushViewController(obj_CartView, animated: true)
        
    }
    
    
}

extension CropSolutionViewCtr : UITableViewDelegate, UITableViewDataSource {

    //MARK:- UITableView Methods
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == tblFilter{
            return self.arrMainList.count
        }
        return 1
    }
    
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView.init()
        headerView.backgroundColor = UIColor.clear
        
        if tableView == tblFilter{

            let lblSecTitle = UILabel.init(frame: CGRect(x: 8, y: 8, width: 200, height: 25))
            lblSecTitle.textColor = UIColor.white
            
            let objModel = self.arrMainList[section]
            
            lblSecTitle.font = kCustom_Font(fontName: kPoppin_SemiBold, fontSize: 18.0)
            lblSecTitle.text = objModel.type.capitalized
            
//            lblSecTitle.attributedText = NSAttributedString(string: objModel.type.capitalized, attributes:
//            [.underlineStyle: NSUnderlineStyle.single.rawValue])
//
            let lblBorder = UILabel.init(frame: CGRect(x: 0, y: 34, width: tblFilter.bounds.width, height: 1))
            lblBorder.backgroundColor = UIColor.white

            headerView.addSubview(lblSecTitle)
            headerView.addSubview(lblBorder)

        }
        
        return headerView
    }
    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if tableView == tblFilter{
            return 40
        }
        return 0
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if tableView == tblFilter{
            
            let objModel = self.arrMainList[section]
            
            if objModel.type == ParameterKeys.Companies{
                return objModel.companyFilter.count
            }else{
                return objModel.technicalFilter.count
            }
        }
        else{
            return self.arrCatProduct.count
        }
    }

    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
    
        if tableView == tblFilter{
            let cell = tableView.dequeueReusableCell(withIdentifier: filterCell, for: indexPath) as! FilterCell
            var strTitle = ""

            let objModel = self.arrMainList[indexPath.section]

            if objModel.type == ParameterKeys.Companies{
                let objCompanyModel = objModel.companyFilter[indexPath.row]
                strTitle = objCompanyModel.name
                
                if objCompanyModel.isSelected{
                    cell.imgCheck.image = UIImage.init(named: "checkBox")
                }else{
                    cell.imgCheck.image = UIImage.init(named: "uncheckBox")
                }
            }else{
                
                let objTechnicalModel = objModel.technicalFilter[indexPath.row]
                strTitle = objTechnicalModel.technical_name
                
                if objTechnicalModel.isSelected{
                    cell.imgCheck.image = UIImage.init(named: "checkBox")
                }else{
                    cell.imgCheck.image = UIImage.init(named: "uncheckBox")
                }
            }
            cell.lblTitle.text = strTitle
            
            return cell
            
        }else{
            
           let cell = tableView.dequeueReusableCell(withIdentifier: categoryListCell, for: indexPath) as! CategoryListCell
           cell.isFromSolution = true
           cell.lblName.text = self.arrCatProduct[indexPath.row].categariesType
           cell.arraySolution = self.arrCatProduct[indexPath.row].cat_data
           cell.collectionViewOffset = storedOffsets[indexPath.row] ?? 0
           
           let cellWidth = (SystemSize.width-50)/2
           
           var objCountTreading = cell.arraySolution.count
           if objCountTreading % 2 != 0 {
               objCountTreading = objCountTreading + 1
           }
           objCountTreading = objCountTreading/2
        
           cell.hightCollectionView.constant = (cellWidth * CGFloat(objCountTreading)) + CGFloat((82 * objCountTreading))
           cell.cvProducts.dataSource = cell.self
           cell.cvProducts.delegate = cell.self
           cell.cvProducts.reloadData()
           cell.viewCtr = self

           return cell
        }
        
    }

    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        if tableView == tblFilter{
            let objModel = self.arrMainList[indexPath.section]

            if objModel.type == ParameterKeys.Companies{
                let objCompanyModel = objModel.companyFilter[indexPath.row]
                objCompanyModel.isSelected = !objCompanyModel.isSelected
            }else{
                let objTechnicalModel = objModel.technicalFilter[indexPath.row]
                objTechnicalModel.isSelected = !objTechnicalModel.isSelected
            }

            tblFilter.reloadData()
        }
    }
}


// MARK: - Empty Data Set Methods
extension CropSolutionViewCtr : EmptyDataSetSource, EmptyDataSetDelegate{
    
    func emptyDataSetShouldDisplay(_ scrollView: UIScrollView) -> Bool {
        return true
    }
    
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        let str = StringFile.strNoDataFound[kAppDelegate.userCurrentLanguage]
        let font = kCustom_Font(fontName: kPoppin_SemiBold, fontSize: 18.0)
        let attributes = [NSAttributedString.Key.font: font]
        let attributedStr = NSAttributedString(string: str, attributes: attributes)
        return attributedStr
    }
    
    func image(forEmptyDataSet scrollView: UIScrollView) -> UIImage? {
        return UIImage.init(named: "nodata")
    }
    
    func spaceHeight(forEmptyDataSet scrollView: UIScrollView) -> CGFloat {
        return 20.0
    }
}


extension CropSolutionViewCtr : UISearchBarDelegate{
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        isSearch = true

    }
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
    {
        isRemovingTextWithBackspace = (NSString(string: searchBar.text!).replacingCharacters(in: range, with: text).count == 0)
        return true
    }



    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        isSearch = false
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
//        self.navigationController?.popViewController(animated: false)
        isSearch = false
        searchBar.isHidden = true
    }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        isSearch = false
    }

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.count > 0 {
            
            arrCatProduct.removeAll()
            
//            arrCatProduct = arrAllCatProduct.filter {
//                $0.cat_data.filter {
//                    $0.name.lowercased().contains(searchText.lowercased())
//                }.count != 0
//
//            }

             arrCatProduct = arrAllCatProduct.filter({ singleObj -> Bool in
                let result = singleObj.cat_data.filter({ $0.name.lowercased().contains(searchText.lowercased()) })
                print("\(result)")
                return result.count > 0 ? true : false
            })

            
//            arrCatProduct = arrAllCatProduct.filter { $0.categariesType .contains(searchText) }

//            print("\(arrCatProduct)")

            
            tblList.reloadData()
            
        }
        else{
            arrCatProduct.removeAll()

            self.arrCatProduct = self.arrAllCatProduct
            tblList.reloadData()
        }
        
        if searchText.count == 0 && !isRemovingTextWithBackspace {

            isSearch = false
            searchBar.isHidden = true
            searchBar.endEditing(true)
            searchBar.resignFirstResponder()
            self.view.endEditing(true)
            self.view.resignFirstResponder()

        }
        else{
            
        }
        
    }
    

    
}
