//
//  CropSolutionListCell.swift
//  KhedutBolo
//
//  Created by Shine on 11/09/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit

class CropSolutionListCell: UITableViewCell {

    // MARK: - define IBOutlet start
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgTitle: UIImageView!
    @IBOutlet weak var viewImageBack: UIView!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.viewImageBack.dropShadow(color: UIColor.lightGray, opacity: 0.4, shadowRadius: 3.0, cornerRadius: 5.0)
        
        self.imgTitle.layer.cornerRadius = 5
        self.imgTitle.layer.masksToBounds = true
        
        
    }

}
