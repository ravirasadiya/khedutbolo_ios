//
//  CropViewCtr.swift
//  KhedutBolo
//
//  Created by Trivedi Sagar on 10/31/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit
import EmptyDataSet_Swift

class CropViewCtr: UIViewController {
    
    // MARK: - define IBOutlet
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var cvList: UICollectionView!
    
    var objCropModel = CropDataModel()
    var arrySolution = [CropSolutionDataModel]()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        cvList.register(UINib(nibName: cropListCCell, bundle: nil), forCellWithReuseIdentifier: cropListCCell)
        self.setupInitialView()
    }
    
    //MARK:- Class Methods and Functions
    func setupInitialView(){
        self.lblTitle.text = StringFile.strSelectCrop[lan]//[kAppDelegate.userCurrentLanguage]
        self.getSolutionAPI(withProductId: objCropModel.id)

    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == kPushToCropSubSegueID{
            var senderId : Int = 0
            senderId = sender as! Int
            let objModel = arrySolution[senderId]
            let obj_CropSolutionViewCtr = segue.destination as! CropSubViewCtr
            obj_CropSolutionViewCtr.passedTitle = "\(objModel.name)"
            obj_CropSolutionViewCtr.objCropModel = objModel
        }
    }
    
    
    func getSolutionAPI(withProductId SolId: Int){

        GlobalMethods.presentLoaderViewCtr(withViewController: self)
        
        let reqParam = [ParameterKeys.Id : SolId, ParameterKeys.User_Id : kAppDelegate.userLoginData.id] as [String : Any]

        WebServiceCall.callMethodWithURL(route: APIRouter.CropSolution(reqParam)) { (_ responseCode: Int?, _ responseValue : [String : AnyObject]?, _ error : Error?) in

            GlobalMethods.dismissLoaderView()
            
            if (responseValue != nil) {
                
                let responseModel = CropSolutionResponseModel(dictionary: responseValue! as NSDictionary)
                
                if responseModel.status == 200{
                    self.arrySolution = responseModel.data
                    self.afterGetSolutionResponse()
                }
                else {
                    self.afterGetSolutionResponse()
                }
            }
            else {
                GlobalMethods.showErrorFromAPI(responseCode, responseValue, error, viewCtr: self)
            }
        }
    }
    
    
    func afterGetSolutionResponse(){
        if arrySolution.count != 0{
            self.cvList.reloadData()
        }else{
            cvList.emptyDataSetSource = self
            cvList.emptyDataSetDelegate = self
            self.cvList.reloadData()
        }
    }
    
   
    //MARK:- IBActions
    @IBAction func clickOnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
// MARK: - UICollectionView Delegate, DataSource Methods
extension CropViewCtr : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrySolution.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cropListCCell, for: indexPath) as! CropListCCell
        let objSolution = arrySolution[indexPath.row]
        cell.lblTitle.text = objSolution.name
        cell.imgTitle.sd_setImage(with: objSolution.imgUrl, placeholderImage: kProductImagePlaceHolder)

        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let cellWidth = (SystemSize.width - 50)/2
        let cellSize  = CGSize(width: cellWidth, height: cellWidth + 50)
        return cellSize
       
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: kPushToCropSubSegueID, sender: indexPath.row)
    }
}

// MARK: - Empty Data Set Methods
extension CropViewCtr : EmptyDataSetSource, EmptyDataSetDelegate{
    
    func emptyDataSetShouldDisplay(_ scrollView: UIScrollView) -> Bool {
        return true
    }
    
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        let str = StringFile.strSelectCrop[kAppDelegate.userCurrentLanguage]
        let font = kCustom_Font(fontName: kPoppin_SemiBold, fontSize: 18.0)
        let attributes = [NSAttributedString.Key.font: font]
        let attributedStr = NSAttributedString(string: str, attributes: attributes)
        return attributedStr
    }
    
    func image(forEmptyDataSet scrollView: UIScrollView) -> UIImage? {
        return UIImage.init(named: "nodata")
    }
    
    func spaceHeight(forEmptyDataSet scrollView: UIScrollView) -> CGFloat {
        return 20.0
    }
}
