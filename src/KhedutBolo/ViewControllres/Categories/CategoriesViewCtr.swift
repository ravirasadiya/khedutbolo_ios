//
//  CategoriesViewCtr.swift
//  KhedutBolo
//
//  Created by Trivedi Sagar on 10/31/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit
import EmptyDataSet_Swift


class CategoriesViewCtr: UIViewController {

    // MARK: - define IBOutlet
    @IBOutlet weak var tblCategory: UITableView!
    @IBOutlet weak var lblTopTitle: UILabel!

    var companyId : UInt = 0
    var companyName = ""
    var allProductArray = [CompanyModel]()
    var arrCatProduct = [CategoriesModel]()
    var storedOffsets = [Int: CGFloat]()
    var IsCompanyCatCalled : Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.setupInitialView()
    }
    
    //MARK:- Class Methods and Functions
    func setupInitialView(){
        lblTopTitle.text = companyName
        tblCategory.registerCell(cellIDs: [categoryListCell], isDynamicHeight: true, estimatedHeight: 100)
        self.getCompanyProductAPI(withCompanyId: companyId)

    }
    
    //MARK:- IBActions
    @IBAction func clickOnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - Web Service Function Methods
    func getCompanyProductAPI(withCompanyId cId: UInt){

        GlobalMethods.presentLoaderViewCtr(withViewController: self)
        
        let reqParam = [ParameterKeys.Company_id : cId, ParameterKeys.User_Id : kAppDelegate.userLoginData.id] as [String : Any]

        WebServiceCall.callMethodWithURL(route: APIRouter.CompanyProducts(reqParam)) { (_ responseCode: Int?, _ responseValue : [String : AnyObject]?, _ error : Error?) in

            GlobalMethods.dismissLoaderView()
            
            if (responseValue != nil) {
                
                let responseModel = CompanyModelResponse(dictionary: responseValue! as NSDictionary)
                
                if responseModel.status == 200{
                    self.allProductArray = responseModel.data
                    self.setResponseCompanyProduct()
                }
                else {
                    self.setTableViewData()
                  //  GlobalMethods.showAlert(alertTitle: StringFile.strError[kAppDelegate.userCurrentLanguage], alertMessage: responseModel.message, viewCtr: self, completion: nil)
                }
            }
            else {
                GlobalMethods.showErrorFromAPI(responseCode, responseValue, error, viewCtr: self)
            }
        }
    }
    
    
    
    func setResponseCompanyProduct(){
        
        let categorieNames = Array(Set(allProductArray.map({$0.name})))
        
        for cat in categorieNames{
            
            let check = allProductArray.filter { $0.name == cat}
            
            let catObj = CategoriesModel()
            catObj.cat_data = check
            catObj.categariesType = cat
            
            self.arrCatProduct.append(catObj)
        }
        
        self.setTableViewData()

    }
    
    func setTableViewData(){
        if arrCatProduct.count != 0{
            self.tblCategory.reloadData()
        }else{
            tblCategory.emptyDataSetSource = self
            tblCategory.emptyDataSetDelegate = self
            self.tblCategory.reloadData()
        }
    }

}

extension CategoriesViewCtr : UITableViewDelegate, UITableViewDataSource {

    //MARK:- UITableView Methods
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.arrCatProduct.count
    }

    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
    
        let cell = tableView.dequeueReusableCell(withIdentifier: categoryListCell, for: indexPath) as! CategoryListCell
        cell.lblName.text = self.arrCatProduct[indexPath.row].categariesType
        cell.arrayProduct = self.arrCatProduct[indexPath.row].cat_data
      //  cell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.row)
        cell.collectionViewOffset = storedOffsets[indexPath.row] ?? 0
        
        let cellWidth = (SystemSize.width-50)/2
        
        var objCountTreading = cell.arrayProduct.count
        if objCountTreading % 2 != 0 {
            objCountTreading = objCountTreading + 1
        }
        objCountTreading = objCountTreading/2
      
        cell.hightCollectionView.constant = (cellWidth * CGFloat(objCountTreading)) + CGFloat((82 * objCountTreading))
        cell.cvProducts.dataSource = cell.self
        cell.cvProducts.delegate = cell.self
        cell.cvProducts.reloadData()
        cell.viewCtr = self
        return cell
    }

    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {}
}


// MARK: - Empty Data Set Methods
extension CategoriesViewCtr : EmptyDataSetSource, EmptyDataSetDelegate{
    
    func emptyDataSetShouldDisplay(_ scrollView: UIScrollView) -> Bool {
        return true
    }
    
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        let str = StringFile.strNoDataFound[kAppDelegate.userCurrentLanguage]
        let font = kCustom_Font(fontName: kPoppin_SemiBold, fontSize: 18.0)
        let attributes = [NSAttributedString.Key.font: font]
        let attributedStr = NSAttributedString(string: str, attributes: attributes)
        return attributedStr
    }
    
    func image(forEmptyDataSet scrollView: UIScrollView) -> UIImage? {
        return UIImage.init(named: "nodata")
    }
    
    func spaceHeight(forEmptyDataSet scrollView: UIScrollView) -> CGFloat {
        return 20.0
    }
}
