//
//  CategoryListCell.swift
//  KhedutBolo
//
//  Created by Trivedi Sagar on 10/31/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit

class CategoryListCell: UITableViewCell {

    // MARK: - define IBOutlet start
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var cvProducts: UICollectionView!
    @IBOutlet weak var hightCollectionView: NSLayoutConstraint!
    
    //MARK:- Variables
    var arrayProduct = [CompanyModel]()
    var arraySolution = [SolutionModel]()
    
    var viewCtr = UIViewController()

    var isFromSolution = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        cvProducts.register(UINib(nibName: prodCatCCell, bundle: nil), forCellWithReuseIdentifier: prodCatCCell)
        
        self.contentView.backgroundColor = .clear
        self.backgroundColor = .clear
        self.selectionStyle = .none

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setCollectionViewDataSourceDelegate<D: UICollectionViewDataSource & UICollectionViewDelegate>(_ dataSourceDelegate: D, forRow row: Int) {

           cvProducts.delegate = dataSourceDelegate
           cvProducts.dataSource = dataSourceDelegate
           cvProducts.tag = row
          // cvProducts.setContentOffset(cvProducts.contentOffset, animated:false) // Stops collection view if it was scrolling.
           cvProducts.reloadData()
       }

       var collectionViewOffset: CGFloat {
           set { cvProducts.contentOffset.x = newValue }
           get { return cvProducts.contentOffset.x }
       }
    
}

// MARK: - UICollectionView Delegate, DataSource Methods
extension CategoryListCell : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        var totalCellCount = 0
        if isFromSolution{
            totalCellCount = arraySolution.count
        }else{
            totalCellCount = arrayProduct.count
        }
        return totalCellCount
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
       
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: prodCatCCell, for: indexPath) as! ProdCatCCell
        
        if isFromSolution{
            let objSolution = arraySolution[indexPath.row]
            
            cell.lblName.text = objSolution.name
            cell.lblPrice.text = "₹\(objSolution.price)"
            cell.lblSalePrice.text = "₹\(objSolution.sale_price)"
            cell.lblPerSet.text = StringFile.strPerSet[kAppDelegate.userCurrentLanguage]
            cell.lblDealerName.text = "\(StringFile.strBy[lan]) : \(objSolution.dealer_name)"
            cell.imgProduct.sd_setImage(with: objSolution.coverImgUrl, placeholderImage: kProductImagePlaceHolder)

        }else{
            let objProduct = arrayProduct[indexPath.row]

            cell.lblName.text = objProduct.name
            cell.lblPrice.text = objProduct.price
            cell.lblSalePrice.text = objProduct.sale_price
            cell.lblPerSet.text = StringFile.strPerSet[kAppDelegate.userCurrentLanguage]
            cell.lblDealerName.text = "\(StringFile.strBy[lan]) : \(objProduct.company_name)"
            cell.imgProduct.sd_setImage(with: objProduct.coverImgUrl, placeholderImage: kProductImagePlaceHolder)

        }
        
        return cell
       
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let cellWidth = (SystemSize.width-65)/2 //50
        let cellSize  = CGSize(width: cellWidth, height: cellWidth + 83) // 72
        return cellSize
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        var prodId = ""
        
        if isFromSolution{
            prodId = "\(arraySolution[indexPath.row].id)"
        }else{
            prodId = "\(arrayProduct[indexPath.row].id)"
        }
        
        GlobalMethods.pushToProductCtr(withViewController: viewCtr, animated: true, productId: prodId)

    }
}
