//
//  VerifyOTPViewCtr.swift
//  KhedutBolo
//
//  Created by Trivedi Sagar on 1/24/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit

class VerifyOTPViewCtr:  UIViewController, UITextFieldDelegate{

    // MARK:- Outlets
    @IBOutlet weak var lblTopTitle: UILabel!
    @IBOutlet weak var viewTopTitle: UIView!

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblInfo: UILabel!
    @IBOutlet weak var lblEnterCode: UILabel!
    @IBOutlet weak var txtFirst: UITextField!
    @IBOutlet weak var txtSecond: UITextField!
    @IBOutlet weak var txtThird: UITextField!
    @IBOutlet weak var txtFour: UITextField!
    @IBOutlet weak var txtFive: UITextField!
    @IBOutlet weak var txtSix: UITextField!
    @IBOutlet weak var btnVerify: UIButton!
    @IBOutlet weak var lblTimer: UILabel!
    @IBOutlet weak var btnResend: UIButton!

    // MARK: - constants & variables
    
    var remainnigSecond = 0
    var objTimer: Timer?
  
    // MARK:- UIView Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupInitialView()
    }
    
    //MARK:- Class Methods and Functions
    func setupInitialView(){
        
        btnVerify.backgroundColor = kAppThemeGreenLightColor
        btnVerify.layer.cornerRadius = 5.0
        btnVerify.layer.masksToBounds = true
        
        viewTopTitle.layer.masksToBounds = false
        viewTopTitle.layer.shadowOffset = CGSize(width: 0, height: 0)
        viewTopTitle.layer.shadowRadius = 1
        viewTopTitle.layer.shadowOpacity = 0.5
       // viewTopTitle.backgroundColor = kAppThemeGreenDarkColor
        
        btnResend.backgroundColor = kAppThemeGreenLightColor
        btnResend.layer.cornerRadius = 5.0
        btnResend.layer.masksToBounds = true
                
        txtFirst.addTarget(self, action: #selector(textFieldEditingDidChange), for: UIControl.Event.editingChanged)
        txtSecond.addTarget(self, action: #selector(textFieldEditingDidChange), for: UIControl.Event.editingChanged)
        txtThird.addTarget(self, action: #selector(textFieldEditingDidChange), for: UIControl.Event.editingChanged)
        txtFour.addTarget(self, action: #selector(textFieldEditingDidChange), for: UIControl.Event.editingChanged)
        txtFive.addTarget(self, action: #selector(textFieldEditingDidChange), for: UIControl.Event.editingChanged)
        txtSix.addTarget(self, action: #selector(textFieldEditingDidChange), for: UIControl.Event.editingChanged)
        
        self.setUpViewString()
        self.setUpTimer()
        btnResend.isHidden = true
        
    }

    func setUpTimer(){
        remainnigSecond = 60
        lblTimer.text = "\(StringFile.strSecondsRemaining[kAppDelegate.userCurrentLanguage]): 00:\(remainnigSecond)"
        objTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(runTimedCode), userInfo: nil, repeats: true)
        self.setViewHideShowWithAnimarion(view: btnResend, hidden: true)
        self.setViewHideShowWithAnimarion(view: btnVerify, hidden: false)
    }
    
    @objc func runTimedCode(){
        if remainnigSecond == 0{
            self.objTimer?.invalidate()
            self.setViewHideShowWithAnimarion(view: btnResend, hidden: false)
            self.setViewHideShowWithAnimarion(view: btnVerify, hidden: true)
        }else{
            remainnigSecond = remainnigSecond - 1
            let strRemain = String(format: "%02d", remainnigSecond)

            lblTimer.text = "\(StringFile.strSecondsRemaining[kAppDelegate.userCurrentLanguage]): 00:\(strRemain)"
        }
    }

    func setUpViewString(){
        
        lblTitle.text = StringFile.strVerificationCode[kAppDelegate.userCurrentLanguage]
        lblTopTitle.text = StringFile.strVerificationCode[kAppDelegate.userCurrentLanguage]
        lblEnterCode.text = StringFile.strEnterCode[kAppDelegate.userCurrentLanguage]
        btnVerify.setTitle(StringFile.strVerifyOTP[kAppDelegate.userCurrentLanguage], for: .normal)
        btnResend.setTitle(StringFile.strResendOTP[kAppDelegate.userCurrentLanguage], for: .normal)
        lblInfo.text = "\(StringFile.msgVerifyOTPInfo[kAppDelegate.userCurrentLanguage]) +91 \(kAppDelegate.userLoginData.mobile)"
    }
    
    func setTokenMoveToDashboard(){

        GlobalMethods.presentLoaderViewCtr(withViewController: self)
        
        GlobalMethods.userLogoutApiCall(withToken: kAppDelegate.iOSDeviceToken) { (isSuccess) in
                GlobalMethods.dismissLoaderView()
            if isSuccess{
                kAppDelegate.userLoginData.save()
                GlobalMethods.updateLanguageForCurrentUser()
                GlobalMethods.pushToMenuViewCtr(withViewController: self, animated: true, fromSignUp: false)
            }
        }
    }
    
    //MARK:- IBActions
    
    @IBAction func clickOnBack(_ sender: Any) {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickOnVerify(_ sender: Any) {
        self.view.endEditing(true)
        
        let strOtp = "\(txtFirst.text!)\(txtSecond.text!)\(txtThird.text!)\(txtFour.text!)\(txtFive.text!)\(txtSix.text!)"
    
        if txtFirst.text!.isEmpty || txtSecond.text!.isEmpty || txtThird.text!.isEmpty || txtFour.text!.isEmpty || txtFive.text!.isEmpty || txtSix.text!.isEmpty{
            self.ShowAlertDisplay(titleObj: StringFile.strAppName[kAppDelegate.userCurrentLanguage], messageObj: StringFile.msgEnterEnterCode[kAppDelegate.userCurrentLanguage], viewcontrolelr: self)
        }
        else if strOtp == kAppDelegate.userLoginData.otp{
            if kAppDelegate.userLoginData.is_register == "false"{
                self.performSegue(withIdentifier: kPushToRegisterSegueID, sender: self)
            }else{
                self.setTokenMoveToDashboard()
            }
            
        } else {
            self.ShowAlertDisplay(titleObj: StringFile.strAppName[kAppDelegate.userCurrentLanguage], messageObj: StringFile.msgInvalidOTP[kAppDelegate.userCurrentLanguage], viewcontrolelr: self)
        }
    }
    
    
    
    @IBAction func clickOnResend(_ sender: Any) {
        self.view.endEditing(true)
        self.sendOtpApi()
    }
    
    func sendOtpApi(){
            
        GlobalMethods.presentLoaderViewCtr(withViewController: self)
        
        let reqParam = [ParameterKeys.Mobile : kAppDelegate.userLoginData.mobile] as [String : Any]

        WebServiceCall.callMethodWithURL(route: APIRouter.SendOtp(reqParam)) { (_ responseCode: Int?, _ responseValue : [String : AnyObject]?, _ error : Error?) in

            GlobalMethods.dismissLoaderView()
            
            if (responseValue != nil) {
                
                let responseModel = ResponseModelNew(dictionary: responseValue! as NSDictionary)
                
                if responseModel.status == 200 && responseModel.dataDict != nil {
                    
                    kAppDelegate.userLoginData = LoginUserModel(dictionary: responseModel.dataDict! as NSDictionary)
                    self.setUpTimer()
                    
                }
                else {
                    GlobalMethods.showAlert(alertTitle: StringFile.strError[kAppDelegate.userCurrentLanguage], alertMessage: StringFile.msgNeedProperDetails[kAppDelegate.userCurrentLanguage], viewCtr: self, completion: nil)
                }
            }
            else {
                GlobalMethods.showErrorFromAPI(responseCode, responseValue, error, viewCtr: self)
            }
        }
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    
    //MARK:- UITextField Delegate Method
    
    @objc func textFieldEditingDidChange(textField: UITextField) {
        
        let text = textField.text
        
        if (text?.utf16.count)! >= 1{
            switch textField{
            case txtFirst:
                txtSecond.becomeFirstResponder()
            case txtSecond:
                txtThird.becomeFirstResponder()
            case txtThird:
                txtFour.becomeFirstResponder()
            case txtFour:
                txtFive.becomeFirstResponder()
            case txtFive:
                txtSix.becomeFirstResponder()
            case txtSix:
                txtSix.resignFirstResponder()
            default:
                break
            }
        }
        
        
        if  text?.count == 0 {
            switch textField{
            case txtFirst:
                txtFirst.becomeFirstResponder()
            case txtSecond:
                txtFirst.becomeFirstResponder()
            case txtThird:
                txtSecond.becomeFirstResponder()
            case txtFour:
                txtThird.becomeFirstResponder()
            case txtFive:
                txtFour.becomeFirstResponder()
            case txtSix:
                txtFive.becomeFirstResponder()
            default:
                break
            }
        }
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let  char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        
        if (isBackSpace == -92) {
            return true
        }
        
        if textField.text!.count == 1{
            return false
        }
        
        return string.isNumber
       
    }
}
