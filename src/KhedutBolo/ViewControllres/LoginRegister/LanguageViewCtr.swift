//
//  LanguageViewCtr.swift
//  KhedutBolo
//
//  Created by Trivedi Sagar on 10/19/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit

class LanguageViewCtr: UIViewController {

    //MARK:- Outlets
    
    @IBOutlet weak var viewEnglish: UIView!
    @IBOutlet weak var viewHindi: UIView!
    @IBOutlet weak var viewGujarati: UIView!
    
    @IBOutlet weak var lblSelLangTitle: UILabel!
    @IBOutlet weak var lblEnglishTitle: UILabel!
    @IBOutlet weak var lblHindiTitle: UILabel!
    @IBOutlet weak var lblGujTitle: UILabel!
    
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var btnEnglish: UIButton!
    @IBOutlet weak var btnHindi: UIButton!
    @IBOutlet weak var btnGujarati: UIButton!
    
    
    // MARK: - constants & variables
    
    //MARK:- UIView Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInitialView()
    }
    
    //MARK:- Class Methods and Functions
    func setupInitialView(){
        
        btnEnglish.tag = Language.kEnglish.rawValue
        btnHindi.tag = Language.kHindi.rawValue
        btnGujarati.tag = Language.kGujarati.rawValue
        btnNext.backgroundColor = kAppThemeGreenLightColor
        btnNext.layer.cornerRadius = 5.0
        btnNext.layer.masksToBounds = true
        
        viewEnglish.layer.cornerRadius = 5.0
        viewEnglish.layer.masksToBounds = true
        
        viewHindi.layer.cornerRadius = 5.0
        viewHindi.layer.masksToBounds = true
        
        viewGujarati.layer.cornerRadius = 5.0
        viewGujarati.layer.masksToBounds = true
        
        self.setLanguageView(withTypeId: kAppDelegate.userCurrentLanguage)
        
        self.setUpViewString()
        
        if kAppDelegate.userLoginData != nil {
            GlobalMethods.pushToMenuViewCtr(withViewController: self, animated: false, fromSignUp: false)
        }
    }
    
    func setUpViewString(){
        lblSelLangTitle.text = StringFile.strSelectLanguage[kAppDelegate.userCurrentLanguage]
        lblEnglishTitle.text = StringFile.strEnglishTitle
        lblHindiTitle.text = StringFile.strHindiTitle
        lblGujTitle.text = StringFile.strGujaratiTitle
        btnNext.setTitle(StringFile.strNext[kAppDelegate.userCurrentLanguage], for: .normal)
    }
    
    func setLanguageView(withTypeId sender:Int){
       
        btnEnglish.setImage(UIImage.init(named: "uncheck"), for: .normal)
        btnHindi.setImage(UIImage.init(named: "uncheck"), for: .normal)
        btnGujarati.setImage(UIImage.init(named: "uncheck"), for: .normal)
        
        if sender == Language.kEnglish.rawValue{
            btnEnglish.setImage(UIImage.init(named: "check"), for: .normal)
        }else if sender == Language.kHindi.rawValue{
            btnHindi.setImage(UIImage.init(named: "check"), for: .normal)
        }else{
            btnGujarati.setImage(UIImage.init(named: "check"), for: .normal)
        }
        
        GlobalMethods.SetUserDefualtLanguage(withLanguageType: sender)

        self.setUpViewString()
    }
    
    //MARK:- IBActions
    @IBAction func clickOnNext(_ sender: Any) {
        self.performSegue(withIdentifier: kPushToLoginSegueID, sender: self)
    }
    
    @IBAction func clickOnLanguageSel(_ sender: UIButton) {
        if kAppDelegate.userCurrentLanguage != sender.tag{
            self.setLanguageView(withTypeId: sender.tag)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
