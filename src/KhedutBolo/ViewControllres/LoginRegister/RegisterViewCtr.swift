//
//  RegisterViewCtr.swift
//  KhedutBolo
//
//  Created by Trivedi Sagar on 10/24/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import DropDown

class RegisterViewCtr: UIViewController {

    // MARK:- Outlets
    @IBOutlet weak var lblTopTitle: UILabel!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var imgCamera: UIImageView!
    @IBOutlet weak var btnImgSelect: UIButton!
    @IBOutlet weak var lblNameTitle: UILabel!
    @IBOutlet weak var tfName: UITextField!
    @IBOutlet weak var viewName: UIView!
    @IBOutlet weak var viewPhone: UIView!
    @IBOutlet weak var lblPhoneTitle: UILabel!
    @IBOutlet weak var tfPhone: UITextField!
    @IBOutlet weak var viewState: UIView!
    @IBOutlet weak var tfState: SkyFloatingLabelTextField!{
        didSet{
            tfState.titleFormatter = { $0 }
            tfState.titleColor = kAppThemeGreenDarkColor
            tfState.titleFont = kCustom_Font(fontName: kPoppin_SemiBold, fontSize: 17.0)
            tfState.placeholderFont = kCustom_Font(fontName: kPoppin_Regular, fontSize: 17.0)

            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
            imageView.contentMode = .scaleAspectFit
            imageView.image = UIImage.init(named: "dropDownBlack")
            
            self.tfState.rightViewMode = UITextField.ViewMode.always
            self.tfState.rightView = imageView
            
        }
    }

    @IBOutlet weak var viewCity: UIView!
    @IBOutlet weak var tfCity: SkyFloatingLabelTextField!{
        didSet{
            tfCity.titleFormatter = { $0 }
            tfCity.titleColor = kAppThemeGreenDarkColor
            tfCity.titleFont = kCustom_Font(fontName: kPoppin_SemiBold, fontSize: 17.0)
            tfCity.placeholderFont = kCustom_Font(fontName: kPoppin_Regular, fontSize: 17.0)

            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
            imageView.contentMode = .scaleAspectFit
            imageView.image = UIImage.init(named: "dropDownBlack")
            
            self.tfCity.rightViewMode = UITextField.ViewMode.always
            self.tfCity.rightView = imageView
            
        }
    }
    @IBOutlet weak var viewTehsil: UIView!
    @IBOutlet weak var tfTehsil: SkyFloatingLabelTextField!{
        didSet{
            tfTehsil.titleFormatter = { $0 }
            tfTehsil.titleColor = kAppThemeGreenDarkColor
            tfTehsil.titleFont = kCustom_Font(fontName: kPoppin_SemiBold, fontSize: 17.0)
            tfTehsil.placeholderFont = kCustom_Font(fontName: kPoppin_Regular, fontSize: 17.0)

            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
            imageView.contentMode = .scaleAspectFit
            imageView.image = UIImage.init(named: "dropDownBlack")
            
            self.tfTehsil.rightViewMode = UITextField.ViewMode.always
            self.tfTehsil.rightView = imageView
            
        }
    }
    @IBOutlet weak var viewVillage: UIView!
    @IBOutlet weak var tfVillage: SkyFloatingLabelTextField!{
        didSet{
            tfVillage.titleFormatter = { $0 }
            tfVillage.titleColor = kAppThemeGreenDarkColor
            tfVillage.titleFont = kCustom_Font(fontName: kPoppin_SemiBold, fontSize: 17.0)
            tfVillage.placeholderFont = kCustom_Font(fontName: kPoppin_Regular, fontSize: 17.0)

            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
            imageView.contentMode = .scaleAspectFit
            imageView.image = UIImage.init(named: "dropDownBlack")
            
            self.tfVillage.rightViewMode = UITextField.ViewMode.always
            self.tfVillage.rightView = imageView
        }
    }
    
    @IBOutlet weak var viewLang: UIView!
    @IBOutlet weak var lblSelectLangTitle: UILabel!
    @IBOutlet weak var lblEnglish: UILabel!
    @IBOutlet weak var imgEnglish: UIImageView!
    @IBOutlet weak var lblHindi: UILabel!
    @IBOutlet weak var imgHindi: UIImageView!
    @IBOutlet weak var lblGujarati: UILabel!
    @IBOutlet weak var imgGujrati: UIImageView!
    @IBOutlet weak var btnEnglish: UIButton!
    @IBOutlet weak var btnHindi: UIButton!
    @IBOutlet weak var btnGujarati: UIButton!
    @IBOutlet weak var btnSubmit: UIButton!
    
    // MARK: - constants & variables
    var arryStateData = [StateDataModel]()
    var arryCityData = [CityDataModel]()
    var arryTehsilData = [TehsilDataModel]()
    var arryVillageData = [VillageDataModel]()
    
    var myPickerView : UIPickerView!
    var toolBar : UIToolbar!
    
    var selectedStateIndex = 0
    var currentStateIndex = 0
    
    var selectedCityIndex = 0
    var currentCityIndex = 0
    
    var selectedTehsilIndex = 0
    var currentTehsilIndex = 0
    
    var selectedVillageIndex = 0
    var currentVillageIndex = 0
    
    var strStateId = ""
    var strCityId = ""
    var strTehsilId = ""
    var strVillageId = ""
        
    var imagePicker: ImagePicker!
    var profileImage:UIImage!


    // MARK:- UIView Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setupInitialView()
        
        self.imagePicker = ImagePicker(presentationController: self, delegate: self)

    }
    override func viewDidAppear(_ animated: Bool) {
        self.getStateApi()
    }
    
    //MARK:- Class Methods and Functions
    func setupInitialView(){

        viewName.layer.cornerRadius = 5.0
        viewName.layer.masksToBounds = true
        
        viewPhone.layer.cornerRadius = 5.0
        viewPhone.layer.masksToBounds = true
        
        viewState.layer.cornerRadius = 5.0
        viewState.layer.masksToBounds = true
        
        viewCity.layer.cornerRadius = 5.0
        viewCity.layer.masksToBounds = true
        
        viewTehsil.layer.cornerRadius = 5.0
        viewTehsil.layer.masksToBounds = true
        
        viewVillage.layer.cornerRadius = 5.0
        viewVillage.layer.masksToBounds = true
        
        viewLang.layer.cornerRadius = 5.0
        viewLang.layer.masksToBounds = true
        
        btnSubmit.backgroundColor = kAppThemeGreenLightColor
        btnSubmit.layer.cornerRadius = 5.0
        btnSubmit.layer.masksToBounds = true
        
        btnEnglish.tag = Language.kEnglish.rawValue
        btnHindi.tag = Language.kHindi.rawValue
        btnGujarati.tag = Language.kGujarati.rawValue
        
        self.setUpViewString()
        
        tfPhone.text = kAppDelegate.userLoginData.mobile
        tfPhone.isUserInteractionEnabled = false
    }
    
    func setUpViewString(){
        lblTopTitle.text = StringFile.strUserRegistration[kAppDelegate.userCurrentLanguage]
        lblNameTitle.text = StringFile.strName[kAppDelegate.userCurrentLanguage]
        tfName.placeholder = StringFile.strPlaceEnterName[kAppDelegate.userCurrentLanguage]
        
        lblPhoneTitle.text = StringFile.strPhoneNumber[kAppDelegate.userCurrentLanguage]
        tfPhone.placeholder = StringFile.strPlaceEnterPhone[kAppDelegate.userCurrentLanguage]
        
        tfState.placeholder = StringFile.strSelectState[kAppDelegate.userCurrentLanguage]
        tfVillage.placeholder = StringFile.strSelectVillage[kAppDelegate.userCurrentLanguage]
        tfTehsil.placeholder = StringFile.strSelectTehsil[kAppDelegate.userCurrentLanguage]
        tfCity.placeholder = StringFile.strSelectCity[kAppDelegate.userCurrentLanguage]
        
        lblSelectLangTitle.text = StringFile.strSelectLanguageSmall[kAppDelegate.userCurrentLanguage]
        
        lblEnglish.text = StringFile.strEnglishTitle
        lblHindi.text = StringFile.strHindiTitle
        lblGujarati.text = StringFile.strGujaratiTitle
        
        btnSubmit.setTitle(StringFile.strSubmitDetails[kAppDelegate.userCurrentLanguage], for: .normal)
    }
    
    func setLanguageView(withTypeId sender:Int){
        
        imgEnglish.image = UIImage.init(named: "uncheck")
        imgHindi.image = UIImage.init(named: "uncheck")
        imgGujrati.image = UIImage.init(named: "uncheck")
        
        if sender == Language.kEnglish.rawValue{
            imgEnglish.image = UIImage.init(named: "check")
        }else if sender == Language.kHindi.rawValue{
            imgHindi.image = UIImage.init(named: "check")
        }else{
            imgGujrati.image = UIImage.init(named: "check")
        }
        
        GlobalMethods.SetUserDefualtLanguage(withLanguageType: sender)
        self.setUpViewString()
    }
    
    func showDropDown(withType type: Int){
        //type:
        // 1 = State, 2 = City, 3 = Tehsil, 4 - Village
        
        for view in self.view.subviews{
            if view == self.myPickerView{
                toolBar.removeFromSuperview()
                myPickerView.removeFromSuperview()
            }
        }
    
        
        self.myPickerView = UIPickerView(frame:CGRect(x: 0, y: UIScreen.main.bounds.size.height - 216, width: self.view.frame.size.width, height: 216))
        self.myPickerView.delegate = self
        self.myPickerView.dataSource = self
        self.myPickerView.backgroundColor = UIColor.white
        self.myPickerView.tag = type
        self.view.addSubview(self.myPickerView)
        toolBar = UIToolbar(frame:CGRect(x: 0, y: UIScreen.main.bounds.size.height - 216, width: self.view.frame.size.width, height: 50))
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = kAppThemeGreenDarkColor
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: StringFile.strDone[kAppDelegate.userCurrentLanguage], style: .plain, target: self, action: #selector(self.doneClick(sender:)))
        doneButton.tag = type
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: StringFile.strCancel[kAppDelegate.userCurrentLanguage], style: .plain, target: self, action: #selector(self.cancelClick(sender:)))
        cancelButton.tag = type
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        self.view.addSubview(toolBar)
        
        if type == 1{
            self.myPickerView.selectRow(selectedStateIndex, inComponent: 0, animated: true)
        }else if type == 2{
            self.myPickerView.selectRow(selectedCityIndex, inComponent: 0, animated: true)
        }else if type == 3{
            self.myPickerView.selectRow(selectedTehsilIndex, inComponent: 0, animated: true)
        }else if type == 4{
            self.myPickerView.selectRow(selectedVillageIndex, inComponent: 0, animated: true)
        }
        

    }
    
    @objc func doneClick(sender: UIButton!) {
        
        self.view.endEditing(true)
        if sender.tag == 1{
            selectedStateIndex = currentStateIndex
            tfState.text = self.arryStateData[selectedStateIndex].DTName
            self.strStateId = self.arryStateData[selectedStateIndex].STCode
            self.tfCity.text = ""
            self.tfTehsil.text = ""
            self.tfVillage.text = ""
            self.arryCityData.removeAll()
            self.arryTehsilData.removeAll()
            self.arryVillageData.removeAll()
            self.getCityApi(withState: self.arryStateData[selectedStateIndex].STCode)
        }else if sender.tag == 2{
            selectedCityIndex = currentCityIndex
            tfCity.text = self.arryCityData[selectedCityIndex].DTName
            self.strCityId = self.arryCityData[selectedCityIndex].DTCode
            self.tfTehsil.text = ""
            self.tfVillage.text = ""
            self.arryTehsilData.removeAll()
            self.arryVillageData.removeAll()
            self.getTehsilApi(withCity: self.arryCityData[selectedCityIndex].DTCode)

        }else if sender.tag == 3{
            selectedTehsilIndex = currentTehsilIndex
            tfTehsil.text = self.arryTehsilData[selectedTehsilIndex].SDTName
            self.strTehsilId = self.arryTehsilData[selectedTehsilIndex].SDTCode
            self.tfVillage.text = ""
            self.arryVillageData.removeAll()
            self.getVillageApi(withTehsil: self.arryTehsilData[selectedTehsilIndex].SDTCode)
        }else if sender.tag == 4{
            selectedVillageIndex = currentVillageIndex
            tfVillage.text = self.arryVillageData[selectedVillageIndex].Name
            self.strVillageId = self.arryVillageData[selectedVillageIndex].TVCode
        }
        
        toolBar.removeFromSuperview()
        myPickerView.removeFromSuperview()
    }
    
    @objc func cancelClick(sender: UIButton!) {
        self.view.endEditing(true)

        if sender.tag == 1{
            if selectedStateIndex != currentStateIndex{
                currentStateIndex = selectedStateIndex
                tfState.text = self.arryStateData[currentStateIndex].DTName
                strStateId = self.arryStateData[currentStateIndex].STCode
            }
        }else if sender.tag == 2{
            if selectedCityIndex != currentCityIndex{
                currentCityIndex = selectedCityIndex
                tfCity.text = self.arryCityData[currentCityIndex].DTName
                strCityId = self.arryCityData[currentCityIndex].DTCode
            }
        }else if sender.tag == 3{
            if selectedTehsilIndex != currentTehsilIndex{
                currentTehsilIndex = selectedTehsilIndex
                tfTehsil.text = self.arryTehsilData[currentTehsilIndex].SDTName
                strTehsilId = self.arryTehsilData[currentTehsilIndex].SDTCode

            }
        }else if sender.tag == 4{
            if selectedVillageIndex != currentVillageIndex{
                currentVillageIndex = selectedVillageIndex
                tfVillage.text = self.arryVillageData[currentVillageIndex].Name
                strVillageId = self.arryVillageData[currentVillageIndex].TVCode
            }
        }
        
        toolBar.removeFromSuperview()
        myPickerView.removeFromSuperview()
    }

    
    //MARK:- IBActions
    
    @IBAction func clickOnBack(_ sender: Any) {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnImageClick(_ sender: Any) {
        self.view.endEditing(true)
        self.imagePicker.present(from: sender as! UIView)
    }
    
    @IBAction func btnStateDropClick(_ sender: Any) {
        self.view.endEditing(true)
        if arryStateData.count > 0{
            self.showDropDown(withType: 1)
        }
    }
    
    @IBAction func btnCityDropClick(_ sender: Any) {
        self.view.endEditing(true)

        if arryCityData.count > 0{
            self.showDropDown(withType: 2)
        }
    }
    
    @IBAction func btnTehsilDropClick(_ sender: Any) {
        self.view.endEditing(true)

        if arryTehsilData.count > 0{
            self.showDropDown(withType: 3)
        }
    }
    
    @IBAction func btnVillageDropClick(_ sender: Any) {
        self.view.endEditing(true)

        if arryVillageData.count > 0{
            self.showDropDown(withType: 4)
        }
    }
    
    @IBAction func btnLanguageClick(_ sender: UIButton) {
        self.view.endEditing(true)
        if kAppDelegate.userCurrentLanguage != sender.tag{
            self.setLanguageView(withTypeId: sender.tag)
        }
    }
    
    @IBAction func clickOnSubmit(_ sender: Any) {
        //
        self.view.endEditing(true)
         if (tfName.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            self.ShowAlertDisplay(titleObj: StringFile.strAppName[kAppDelegate.userCurrentLanguage], messageObj: StringFile.msgPleaseEnterName[kAppDelegate.userCurrentLanguage], viewcontrolelr: self)
         }else if (tfPhone.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
             self.ShowAlertDisplay(titleObj: StringFile.strAppName[kAppDelegate.userCurrentLanguage], messageObj: StringFile.msgPleaseEnterPhone[kAppDelegate.userCurrentLanguage], viewcontrolelr: self)
         }
         else if tfPhone.text!.count < PHONE_MAX_CHAR_SIZE{
             self.ShowAlertDisplay(titleObj: StringFile.strAppName[kAppDelegate.userCurrentLanguage], messageObj: StringFile.msgEnterPhoneNumberValid[kAppDelegate.userCurrentLanguage], viewcontrolelr: self)
         }else if (tfState.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
             self.ShowAlertDisplay(titleObj: StringFile.strAppName[kAppDelegate.userCurrentLanguage], messageObj: StringFile.msgPleaseSelectState[kAppDelegate.userCurrentLanguage], viewcontrolelr: self)
         }else if (tfCity.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
             self.ShowAlertDisplay(titleObj: StringFile.strAppName[kAppDelegate.userCurrentLanguage], messageObj: StringFile.msgPleaseSelectCity[kAppDelegate.userCurrentLanguage], viewcontrolelr: self)
         }else if (tfTehsil.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
             self.ShowAlertDisplay(titleObj: StringFile.strAppName[kAppDelegate.userCurrentLanguage], messageObj: StringFile.msgPleaseSelectTehsil[kAppDelegate.userCurrentLanguage], viewcontrolelr: self)
         }else if (tfVillage.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
             self.ShowAlertDisplay(titleObj: StringFile.strAppName[kAppDelegate.userCurrentLanguage], messageObj: StringFile.msgPleaseSelectVillage[kAppDelegate.userCurrentLanguage], viewcontrolelr: self)
         }
         else{
             //self.registerApi()
            self.registerApiCall()
         }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    //MARK:- Web Services Call
    
    func registerApiCall(){
         
        GlobalMethods.presentLoaderViewCtr(withViewController: self)
        
        let selectedLang = GlobalMethods.getCurrentLanguageCode()

        let reqParam = [ParameterKeys.Mobile: self.tfPhone.text!, ParameterKeys.Name: tfName.text!, ParameterKeys.Address: "", ParameterKeys.Language: selectedLang, ParameterKeys.Sid : strStateId, ParameterKeys.Cid : strCityId, ParameterKeys.Tid : strTehsilId, ParameterKeys.Vid : strVillageId, ParameterKeys.Token : kAppDelegate.iOSDeviceToken] as [String : Any]
        
        var imgArryModel = [ImageUploadModel]()
        
        if self.profileImage != nil {
            let imgArray = [["Image" : self.profileImage!, "ImageKey" : "image"]]
            imgArryModel = [ImageUploadModel](dictionaryArray: imgArray as [NSDictionary])
        }
        
        
         WebServiceCall.uploadImageWithURL(route: APIRouter.Register(reqParam), imageArray : imgArryModel) { (_ responseCode: Int?, _ responseValue : [String : AnyObject]?, _ error : Error?) in
 
            GlobalMethods.dismissLoaderView()
            
            let responseModel = ResponseModelNew(dictionary: responseValue! as NSDictionary)

            if (responseValue != nil) {
                
                if responseModel.status == 200 && responseModel.dataDict != nil {
                    kAppDelegate.userLoginData = LoginUserModel(dictionary: responseModel.dataDict! as NSDictionary)
                    kAppDelegate.userLoginData.save()
                    GlobalMethods.pushToMenuViewCtr(withViewController: self, animated: true, fromSignUp: true)
                }
                else
                {
                    self.ShowAlertDisplay(titleObj: StringFile.strAppName[kAppDelegate.userCurrentLanguage], messageObj: responseModel.message, viewcontrolelr: self)
                }

            }
            else {
                GlobalMethods.showErrorFromAPI(responseCode, responseValue, error, viewCtr: self)
            }
        }
    }
    
    func getStateApi(){

        GlobalMethods.presentLoaderViewCtr(withViewController: self)
        
        WebServiceCall.cancelAPIRequest(arryLastPathComponents: [APIRouter.GetState(custId:kAppDelegate.userLoginData.id,Luang:kAppDelegate.userCurrentLanguage).path])

        WebServiceCall.callMethodWithURL(route: APIRouter.GetState(custId:kAppDelegate.userLoginData.id,Luang:kAppDelegate.userCurrentLanguage)) { (_ responseCode: Int?, _ responseValue : [String : AnyObject]?, _ error : Error?) in
            
                
            GlobalMethods.dismissLoaderView()
            
             if (responseValue != nil) {
                
                let responseModel = StateModel(dictionary: responseValue! as NSDictionary)

                if responseModel.status == 200{
                    self.arryStateData = responseModel.data
                }
                else {
                   GlobalMethods.showAlert(alertTitle: StringFile.strError[kAppDelegate.userCurrentLanguage], alertMessage: StringFile.msgNeedProperDetails[kAppDelegate.userCurrentLanguage], viewCtr: self, completion: nil)
               }
           }
           else {
               GlobalMethods.showErrorFromAPI(responseCode, responseValue, error, viewCtr: self)
           }
        }
    }
    
    func getCityApi(withState stateId: String){

        GlobalMethods.presentLoaderViewCtr(withViewController: self)
        
        WebServiceCall.cancelAPIRequest(arryLastPathComponents: [APIRouter.GetCities([:]).path])

        let reqParam = [ParameterKeys.State_id : stateId,ParameterKeys.Customer_id: kAppDelegate.userLoginData.id,ParameterKeys.Language:kAppDelegate.userCurrentLanguage] as [String : Any]

        WebServiceCall.callMethodWithURL(route: APIRouter.GetCities(reqParam)) { (_ responseCode: Int?, _ responseValue : [String : AnyObject]?, _ error : Error?) in

            GlobalMethods.dismissLoaderView()
            
             if (responseValue != nil) {
                
                let responseModel = CityModel(dictionary: responseValue! as NSDictionary)

                if responseModel.status == 200{
                    self.arryCityData = responseModel.data
                }
                else {
                   GlobalMethods.showAlert(alertTitle: StringFile.strError[kAppDelegate.userCurrentLanguage], alertMessage: StringFile.msgNeedProperDetails[kAppDelegate.userCurrentLanguage], viewCtr: self, completion: nil)
               }
           }
           else {
               GlobalMethods.showErrorFromAPI(responseCode, responseValue, error, viewCtr: self)
           }
        }
    }
    
    func getTehsilApi(withCity cityId: String){

        GlobalMethods.presentLoaderViewCtr(withViewController: self)
        
        WebServiceCall.callMethodWithURL(route: APIRouter.GetTehsil(withCityId: cityId)) { (_ responseCode: Int?, _ responseValue : [String : AnyObject]?, _ error : Error?) in

            GlobalMethods.dismissLoaderView()
            
             if (responseValue != nil) {
                
                let responseModel = TehsilModel(dictionary: responseValue! as NSDictionary)

                if responseModel.status == 200{
                    self.arryTehsilData = responseModel.data
                }
                else {
                   GlobalMethods.showAlert(alertTitle: StringFile.strError[kAppDelegate.userCurrentLanguage], alertMessage: StringFile.msgNeedProperDetails[kAppDelegate.userCurrentLanguage], viewCtr: self, completion: nil)
               }
           }
           else {
               GlobalMethods.showErrorFromAPI(responseCode, responseValue, error, viewCtr: self)
           }
        }
    }
    
    func getVillageApi(withTehsil tehsilId: String){

        GlobalMethods.presentLoaderViewCtr(withViewController: self)
        
        WebServiceCall.callMethodWithURL(route: APIRouter.GetVillage(withTehsilId: tehsilId)) { (_ responseCode: Int?, _ responseValue : [String : AnyObject]?, _ error : Error?) in

            GlobalMethods.dismissLoaderView()
            
             if (responseValue != nil) {
                
                let responseModel = VillageModel(dictionary: responseValue! as NSDictionary)

                if responseModel.status == 200{
                    self.arryVillageData = responseModel.data
                }
                else {
                   GlobalMethods.showAlert(alertTitle: StringFile.strError[kAppDelegate.userCurrentLanguage], alertMessage: StringFile.msgNeedProperDetails[kAppDelegate.userCurrentLanguage], viewCtr: self, completion: nil)
               }
           }
           else {
               GlobalMethods.showErrorFromAPI(responseCode, responseValue, error, viewCtr: self)
           }
        }
    }
}

extension RegisterViewCtr:UITextFieldDelegate{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
    
        if textField == self.tfState {
            return false
        }else if textField == self.tfCity{
            return false
        } else if textField == self.tfTehsil{
            return false
        } else if textField == self.tfVillage{
            return false
        }
        return true
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool
    {
        
        let  char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        
        if (isBackSpace == -92) {
            return true
        }
        
        if textField == tfPhone{
            return string.isNumber
        }
        return true
    }
}

extension RegisterViewCtr: ImagePickerDelegate {
    func didSelect(image: UIImage?) {
        if image != nil{
            profileImage = image
            self.imgUser.image = image
            self.imgUser.layer.cornerRadius = self.imgUser.bounds.width/2
            self.imgUser.layer.masksToBounds = true
        }
    }
}

extension RegisterViewCtr : UIPickerViewDelegate, UIPickerViewDataSource{
    //MARK:- PickerView Delegate & DataSource
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        var returnCount = 0
        if pickerView.tag == 1{
            returnCount = self.arryStateData.count
        } else if pickerView.tag == 2{
            returnCount = self.arryCityData.count
        } else if pickerView.tag == 3{
            returnCount = self.arryTehsilData.count
        } else if pickerView.tag == 4{
            returnCount = self.arryVillageData.count
        }
        return returnCount
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        var passString = ""
        if pickerView.tag == 1{
            passString = self.arryStateData[row].DTName
        } else if pickerView.tag == 2{
            passString = self.arryCityData[row].DTName
        } else if pickerView.tag == 3{
            passString = self.arryTehsilData[row].SDTName
        } else if pickerView.tag == 4{
            passString = self.arryVillageData[row].Name
        }
        return passString
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView.tag == 1{
            let passString = self.arryStateData[row].DTName
            tfState.text = passString
            currentStateIndex = row
            strStateId = self.arryStateData[currentStateIndex].STCode
        } else if pickerView.tag == 2{
            let passString = self.arryCityData[row].DTName
            tfCity.text = passString
            currentCityIndex = row
            strCityId = self.arryCityData[currentCityIndex].DTCode

        } else if pickerView.tag == 3{
            let passString = self.arryTehsilData[row].SDTName
            tfTehsil.text = passString
            currentTehsilIndex = row
            strTehsilId = self.arryTehsilData[currentTehsilIndex].SDTCode

        } else if pickerView.tag == 4{
            let passString = self.arryVillageData[row].Name
            tfVillage.text = passString
            currentVillageIndex = row
            strVillageId = self.arryVillageData[currentVillageIndex].TVCode

        }
    }

}
