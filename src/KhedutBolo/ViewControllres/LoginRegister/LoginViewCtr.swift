//
//  LoginViewCtr.swift
//  KhedutBolo
//
//  Created by Trivedi Sagar on 10/18/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit

class LoginViewCtr: UIViewController, UITextFieldDelegate{

    // MARK:- Outlets
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var txtEnterPhone: UITextField!
    @IBOutlet weak var btnGet: UIButton!
   
    // MARK: - constants & variables
       
    // MARK:- UIView Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInitialView()
    }
    
    //MARK:- Class Methods and Functions
    func setupInitialView(){
        
        btnGet.backgroundColor = kAppThemeGreenLightColor
        btnGet.layer.cornerRadius = 5.0
        btnGet.layer.masksToBounds = true
        
        self.setUpViewString()
    }

    func setUpViewString(){
        txtEnterPhone.placeholder = StringFile.strEnterMobileNum[kAppDelegate.userCurrentLanguage]
        lblTitle.text = "\(StringFile.strEnterMobileNum[kAppDelegate.userCurrentLanguage])!"
        btnGet.setTitle(StringFile.strSendOtp[kAppDelegate.userCurrentLanguage], for: .normal)
    }
    
    //MARK:- IBActions
    @IBAction func clickOnGet(_ sender: Any) {
        
       self.view.endEditing(true)
        if (txtEnterPhone.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            self.ShowAlertDisplay(titleObj: StringFile.strAppName[kAppDelegate.userCurrentLanguage], messageObj: StringFile.msgEnterPhoneNumber[kAppDelegate.userCurrentLanguage], viewcontrolelr: self)
        }else if txtEnterPhone.text!.count < PHONE_MAX_CHAR_SIZE{
            self.ShowAlertDisplay(titleObj: StringFile.strAppName[kAppDelegate.userCurrentLanguage], messageObj: StringFile.msgEnterPhoneNumberValid[kAppDelegate.userCurrentLanguage], viewcontrolelr: self)
        }
        else{
            self.sendOtpApi()
        }
    }
    
    
    func sendOtpApi(){

        GlobalMethods.presentLoaderViewCtr(withViewController: self)
        

        let reqParam = [ParameterKeys.Mobile : txtEnterPhone.text!] as [String : Any]

        WebServiceCall.callMethodWithURL(route: APIRouter.SendOtp(reqParam)) { (_ responseCode: Int?, _ responseValue : [String : AnyObject]?, _ error : Error?) in

            GlobalMethods.dismissLoaderView()
            
            if (responseValue != nil) {
                
                let responseModel = ResponseModelNew(dictionary: responseValue! as NSDictionary)
                
                if responseModel.status == 200 && responseModel.dataDict != nil {
                    
                    kAppDelegate.userLoginData = LoginUserModel(dictionary: responseModel.dataDict! as NSDictionary)
                    
                    self.performSegue(withIdentifier: kPushToVarifyOTPSegueID, sender: self)
               
                }
                else {
                    GlobalMethods.showAlert(alertTitle: StringFile.strError[kAppDelegate.userCurrentLanguage], alertMessage: StringFile.msgNeedProperDetails[kAppDelegate.userCurrentLanguage], viewCtr: self, completion: nil)
                }
            }
            else {
                GlobalMethods.showErrorFromAPI(responseCode, responseValue, error, viewCtr: self)
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    
    //MARK:- UITextField Delegate Method
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let  char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        
        if (isBackSpace == -92) {
            return true
        }
        
        if (textField.text?.count)! > 0 && textField != txtEnterPhone{
            return false
        }
        return string.isNumber
       
    }
}
