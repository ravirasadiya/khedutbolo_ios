//
//  WalletViewCtr.swift
//  KhedutBolo
//
//  Created by Trivedi Sagar on 3/6/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit
import EmptyDataSet_Swift

class WalletViewCtr: UIViewController {

    // MARK: - define IBOutlet
    @IBOutlet weak var lblTopTitle: UILabel!
    @IBOutlet weak var tblWallet: UITableView!
    @IBOutlet weak var lblYourWalletPoint: UILabel!
    @IBOutlet weak var viewBottom: UIView!
    @IBOutlet weak var lblTotalPoint: UILabel!

    
    // MARK: - define IBOutlet

    var arryWallet = [WalletModelDataModel]()
    var objWalletModel = WalletModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.setupInitialView()
    }
    
    //MARK:- Class Methods and Functions
    func setupInitialView(){
        lblTopTitle.text = StringFile.strWallet[kAppDelegate.userCurrentLanguage]
        lblYourWalletPoint.text = StringFile.strYourWalletPoint[kAppDelegate.userCurrentLanguage]
        tblWallet.registerCell(cellIDs: [walletListCell], isDynamicHeight: true, estimatedHeight: 70)
        viewBottom.isHidden = true
        
        self.getWalletAPI(withCustId: kAppDelegate.userLoginData.id)
    }
    
    //MARK:- IBActions
    @IBAction func clickOnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }*/
    

    //MARK:-  Web Services
    func getWalletAPI(withCustId cId: String){

        GlobalMethods.presentLoaderViewCtr(withViewController: self)
        
        WebServiceCall.callMethodWithURL(route: APIRouter.GetWallet(withCustId: cId)) { (_ responseCode: Int?, _ responseValue : [String : AnyObject]?, _ error : Error?) in
            
            GlobalMethods.dismissLoaderView()
            
            if (responseValue != nil) {
                
                let responseModel = WalletModel(dictionary: responseValue! as NSDictionary)
                
                if responseModel.status == 200{
                    self.objWalletModel = responseModel
                    self.arryWallet = responseModel.data
                    self.afterGetWallet()
                }
                else {
                    self.afterGetWallet()
                }
            }
            else {
                GlobalMethods.showErrorFromAPI(responseCode, responseValue, error, viewCtr: self)
            }
        }
    }
    
    func afterGetWallet(){
        if arryWallet.count != 0{
            self.tblWallet.reloadData()
            self.setViewHideShowWithAnimarion(view: viewBottom, hidden: false)
            self.lblTotalPoint.text = "\(self.objWalletModel.totalpoint)"
        }else{
            tblWallet.emptyDataSetSource = self
            tblWallet.emptyDataSetDelegate = self
            self.tblWallet.reloadData()
        }
    }
}

extension WalletViewCtr : UITableViewDelegate, UITableViewDataSource {

    //MARK:- UITableView Methods
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arryWallet.count
    }

    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
    
        let cell = tableView.dequeueReusableCell(withIdentifier: walletListCell, for: indexPath) as! WalletListCell
        let objModel = self.arryWallet[indexPath.row]
        cell.lblDate.text = DateGeneric.convertTime(withOldFormat: "yyyy-MM-dd HH:mm:ss", withNewFormat: "dd/MM/yyyy", strTime: objModel.created_at)
        cell.lblTitle.text = objModel.title
        cell.lblTotal.text = objModel.amount
       // cell.lblTotal.textColor = (objModel.credit_or_debit == "credit") ? kAppThemeGreenDarkColor : UIColor.red
        
        if objModel.credit_or_debit == "credit"{
            cell.lblTitle.textColor = kAppThemeGreenDarkColor
            
        }else {
            cell.lblTitle.textColor = UIColor.red
        }
        
        return cell
    }

    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {}
}

// MARK: - Empty Data Set Methods
extension WalletViewCtr : EmptyDataSetSource, EmptyDataSetDelegate{
    
    func emptyDataSetShouldDisplay(_ scrollView: UIScrollView) -> Bool {
        return true
    }
    
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        let str = StringFile.strNoDataFound[kAppDelegate.userCurrentLanguage]
        let font = kCustom_Font(fontName: kPoppin_SemiBold, fontSize: 18.0)
        let attributes = [NSAttributedString.Key.font: font]
        let attributedStr = NSAttributedString(string: str, attributes: attributes)
        return attributedStr
    }
    
    func image(forEmptyDataSet scrollView: UIScrollView) -> UIImage? {
        return UIImage.init(named: "nodata")
    }
    
    func spaceHeight(forEmptyDataSet scrollView: UIScrollView) -> CGFloat {
        return 20.0
    }
}

