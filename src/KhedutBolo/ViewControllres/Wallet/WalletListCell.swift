//
//  WalletListCell.swift
//  KhedutBolo
//
//  Created by Trivedi Sagar on 3/6/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit

class WalletListCell: UITableViewCell {
    
    // MARK: - define IBOutlet
    @IBOutlet weak var imgWallet: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTotal: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.contentView.backgroundColor = .clear
        self.backgroundColor = .clear
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
