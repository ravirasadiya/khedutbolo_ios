//
//  OrderListCell.swift
//  KhedutBolo
//
//  Created by Trivedi Sagar on 3/9/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit

class OrderListCell: UITableViewCell {

    // MARK: - define IBOutlet start
    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var viewBottom: UIView!
    @IBOutlet weak var lblTopTitel: UILabel!
    @IBOutlet weak var lblOrderDateTitle: UILabel!
    @IBOutlet weak var lblOrderDate: UILabel!
    @IBOutlet weak var lblPriceTitle: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblView: UILabel!
    @IBOutlet weak var btnView: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.contentView.backgroundColor = .clear
        self.backgroundColor = .clear
        self.selectionStyle = .none
        
        self.lblView.text = StringFile.strVIEW[kAppDelegate.userCurrentLanguage]
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
