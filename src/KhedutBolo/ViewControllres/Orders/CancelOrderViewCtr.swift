//
//  CancelOrderViewCtr.swift
//  KhedutBolo
//
//  Created by Trivedi Sagar on 11/1/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit
import EmptyDataSet_Swift

class CancelOrderViewCtr: UIViewController {
    
    // MARK: - define IBOutlet
    @IBOutlet weak var lblTopTitle: UILabel!
    @IBOutlet weak var tblOrder: UITableView!

    var arryOrder = [commonOrderDataModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.setupInitialView()
        

    }
    
    
    //MARK:- Class Methods and Functions
       func setupInitialView(){
        
            lblTopTitle.text = StringFile.strCancelOrder[kAppDelegate.userCurrentLanguage]
            tblOrder.registerCell(cellIDs: [orderListCell], isDynamicHeight: true, estimatedHeight: 124)
            self.getCancelOrderListAPI(withCustId: kAppDelegate.userLoginData.id)

       }
       
       //MARK:- IBActions
       @IBAction func clickOnBack(_ sender: Any) {
           self.navigationController?.popViewController(animated: true)
       }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    //MARK:-  Web Services
    func getCancelOrderListAPI(withCustId cId: String){
            
        GlobalMethods.presentLoaderViewCtr(withViewController: self)
    
        WebServiceCall.callMethodWithURL(route: APIRouter.GetOrder(withCustId: kAppDelegate.userLoginData.id, withFlag: "2")) { (_ responseCode: Int?, _ responseValue : [String : AnyObject]?, _ error : Error?) in
                
            GlobalMethods.dismissLoaderView()
            
            if (responseValue != nil) {
                
                let responseModel = commonOrderModel(dictionary: responseValue! as NSDictionary)
                
                if responseModel.status == 200{
                    
                    self.arryOrder = responseModel.data
                    self.afterGetOrder()
                }
                else {
                    self.afterGetOrder()
                }
            }
            else {
                GlobalMethods.showErrorFromAPI(responseCode, responseValue, error, viewCtr: self)
            }
        }
    }
    
    func afterGetOrder(){
        if arryOrder.count != 0{
            self.tblOrder.reloadData()
        }else{
            tblOrder.emptyDataSetSource = self
            tblOrder.emptyDataSetDelegate = self
            self.tblOrder.reloadData()
        }
    }
    
    @objc func btnViewDetails(_ sender: UIButton) {
        let obj_AllOrderViewCtr = self.storyboard?.instantiateViewController(withIdentifier: "AllOrderViewCtr") as! AllOrderViewCtr
        obj_AllOrderViewCtr.passedOrderType = OderType.CancelOrder.rawValue
        obj_AllOrderViewCtr.passedOrderId = "\(self.arryOrder[sender.tag].id)"
        self.navigationController?.pushViewController(obj_AllOrderViewCtr, animated: true)

    }

}

extension CancelOrderViewCtr : UITableViewDelegate, UITableViewDataSource {

    //MARK:- UITableView Methods
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arryOrder.count
    }

    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
    
        let cell = tableView.dequeueReusableCell(withIdentifier: orderListCell, for: indexPath) as! OrderListCell
        let objOrder = arryOrder[indexPath.row]
        cell.viewTop.backgroundColor = UIColor.red
        cell.viewBottom.backgroundColor = UIColor.red
        cell.lblTopTitel.textColor = UIColor.white
        cell.lblView.textColor = UIColor.white
        cell.lblTopTitel.text = "\(StringFile.strOrderid[kAppDelegate.userCurrentLanguage]) : \(objOrder.id)"
        cell.lblOrderDateTitle.text = "\(StringFile.strOrderDate[kAppDelegate.userCurrentLanguage]) :"
        cell.lblOrderDate.text = DateGeneric.convertTime(withOldFormat: "yyyy-MM-dd HH:mm:ss", withNewFormat: "dd-MM-yyyy HH:mm", strTime: objOrder.created_at)
        cell.lblPriceTitle.text = "\(StringFile.strTotalPrice[kAppDelegate.userCurrentLanguage]) :"
        cell.lblPrice.text = "₹\(objOrder.total)"
        cell.btnView.tag = indexPath.row
        cell.btnView.addTarget(self, action: #selector(btnViewDetails(_:)), for: .touchUpInside)
        return cell
    }

    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {}
}


// MARK: - Empty Data Set Methods
extension CancelOrderViewCtr : EmptyDataSetSource, EmptyDataSetDelegate{
    
    func emptyDataSetShouldDisplay(_ scrollView: UIScrollView) -> Bool {
        return true
    }
    
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        let str = "\(StringFile.strNoDataFound[kAppDelegate.userCurrentLanguage])"
        let font = kCustom_Font(fontName: kPoppin_SemiBold, fontSize: 18.0)
        let attributes = [NSAttributedString.Key.font: font]
        let attributedStr = NSAttributedString(string: str, attributes: attributes)
        return attributedStr
    }
    
    func image(forEmptyDataSet scrollView: UIScrollView) -> UIImage? {
        return UIImage.init(named: "nodata")
    }
    
    func spaceHeight(forEmptyDataSet scrollView: UIScrollView) -> CGFloat {
        return 20.0
    }
}
