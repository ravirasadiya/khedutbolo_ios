//
//  OrderViewCtr.swift
//  KhedutBolo
//
//  Created by Trivedi Sagar on 3/11/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit

class OrderViewCtr: UIViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
   
    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var lblDealerName: UILabel!
    @IBOutlet weak var lblSellerName: UILabel!
    
    @IBOutlet weak var viewProductDetail: UIView!
    
    @IBOutlet weak var lblProductSizeTitle: UILabel!
    @IBOutlet weak var lblProductSize: UILabel!
    
    @IBOutlet weak var lblProductQtyTitle: UILabel!
    @IBOutlet weak var lblProductQty: UILabel!
   
    @IBOutlet weak var lblProductSellingTitle: UILabel!
    @IBOutlet weak var lblProductSelling: UILabel!
    
    @IBOutlet weak var lblProductTotalTitle: UILabel!
    @IBOutlet weak var lblProductTotal: UILabel!
    
    @IBOutlet weak var lblProductShippingTitle: UILabel!
    @IBOutlet weak var lblProductShipping: UILabel!
    
    var objOrder = OrderDataModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.setupInitialView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
    }
    
   
    
    //MARK:- Class Methods and Functions
    func setupInitialView(){
        lblTitle.text = StringFile.strProductDeatils[kAppDelegate.userCurrentLanguage]
        imgProduct.sd_setImage(with: objOrder.coverImgUrl, placeholderImage: kProductImagePlaceHolder)
        lblProductName.text = objOrder.product_name
        lblDealerName.text = "\(StringFile.strBy[lan]) : \(objOrder.Dealer)"
        lblSellerName.text = "\(StringFile.strSellBy[kAppDelegate.userCurrentLanguage]) : \(StringFile.strAppName[kAppDelegate.userCurrentLanguage])"
        lblProductSize.text = "\(objOrder.value) \(objOrder.key)"
        lblProductSizeTitle.text = StringFile.strProductSize[kAppDelegate.userCurrentLanguage]
        lblProductQty.text = "\(objOrder.quantity)"
        lblProductQtyTitle.text = StringFile.strQuantity[kAppDelegate.userCurrentLanguage]
        lblProductSelling.text = "₹\(objOrder.sale_price)"
        lblProductSellingTitle.text = StringFile.strSellingPrice[kAppDelegate.userCurrentLanguage]
        lblProductShipping.text = "₹\(objOrder.shipping_price)"
        lblProductShippingTitle.text = StringFile.strShippingPrice[kAppDelegate.userCurrentLanguage]
        lblProductTotal.text = "₹\(objOrder.total)"
        lblProductTotalTitle.text = StringFile.strTotalPrice[kAppDelegate.userCurrentLanguage]
       
        viewProductDetail.dropShadow(color: UIColor.darkGray, opacity: 0.5, shadowRadius: 10.0, cornerRadius: 10.0)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    //MARK:- IBActions
    
    @IBAction func btnBackClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
