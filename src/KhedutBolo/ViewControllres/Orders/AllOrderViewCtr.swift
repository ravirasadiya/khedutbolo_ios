//
//  AllOrderViewCtr.swift
//  KhedutBolo
//
//  Created by Trivedi Sagar on 3/9/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit
import EmptyDataSet_Swift

class AllOrderViewCtr: UIViewController {
    
    // MARK: - define IBOutlet
    @IBOutlet weak var tblOrder: UITableView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnCancelOrder: UIButton!
    @IBOutlet weak var bottomTableConstraint: NSLayoutConstraint!

    var arryOrder = [OrderDataModel]()
    var passedOrderId = ""
    var passedOrderType = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.setupInitialView()
    }
    
    
    //MARK:- Class Methods and Functions
    func setupInitialView(){
        
        btnCancelOrder.layer.cornerRadius = 5
        btnCancelOrder.layer.masksToBounds = true
        
        tblOrder.registerCell(cellIDs: [orderCell], isDynamicHeight: true, estimatedHeight: 60)
        
        btnCancelOrder.isHidden = true
        self.bottomTableConstraint.constant = 0
        
        if passedOrderType == OderType.CurrentOrder.rawValue
        {
            lblTitle.text = StringFile.strOrderDetail[kAppDelegate.userCurrentLanguage]
            self.getCurrentOrderListAPI()
            self.setViewHideShowWithAnimarion(view: btnCancelOrder, hidden: false)
            self.bottomTableConstraint.constant = 58
        } else if passedOrderType == OderType.CompleteOrder.rawValue{
            lblTitle.text = StringFile.strOrderDetail[kAppDelegate.userCurrentLanguage]
            self.getCompleteOrderListAPI()
        } else if passedOrderType == OderType.CancelOrder.rawValue{
            lblTitle.text = StringFile.strOrderDetail[kAppDelegate.userCurrentLanguage]
            self.getCancelOrderListAPI()
        }
        
        btnCancelOrder.setTitle(StringFile.strCancelOrderCap[kAppDelegate.userCurrentLanguage], for: .normal)
    }
       
    //MARK:- IBActions
    @IBAction func clickOnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnCancelOrderClick(_ sender: Any) {
        GlobalMethods.showAlert(alertTitle: StringFile.strAppName[kAppDelegate.userCurrentLanguage], alertMessage: StringFile.msgCancelOrderInfo[kAppDelegate.userCurrentLanguage], okActionTitle: StringFile.strYesSmall[kAppDelegate.userCurrentLanguage], cancelActionTitle: StringFile.strNoSmall[kAppDelegate.userCurrentLanguage], viewCtr: self, successCompletion: {
                self.cancelOrderApi()
            }) {
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    //MARK:-  Web Services
    func getCurrentOrderListAPI(){
            
        GlobalMethods.presentLoaderViewCtr(withViewController: self)
        
        let reqParam = [ParameterKeys.User_Id : kAppDelegate.userLoginData.id, ParameterKeys.Order_id : passedOrderId] as [String : Any]

        WebServiceCall.callMethodWithURL(route: APIRouter.CurrentOrder(reqParam)) { (_ responseCode: Int?, _ responseValue : [String : AnyObject]?, _ error : Error?) in
                
            GlobalMethods.dismissLoaderView()
            
            if (responseValue != nil) {
                
                let responseModel = OrderModel(dictionary: responseValue! as NSDictionary)
                
                if responseModel.status == 200{
                    
                    self.arryOrder = responseModel.data
                    self.afterGetOrder()
                }
                else {
                    self.afterGetOrder()
                }
            }
            else {
                GlobalMethods.showErrorFromAPI(responseCode, responseValue, error, viewCtr: self)
            }
        }
    }
    
    func getCompleteOrderListAPI(){
            
        GlobalMethods.presentLoaderViewCtr(withViewController: self)
    
        let reqParam = [ParameterKeys.User_Id : kAppDelegate.userLoginData.id, ParameterKeys.Order_id : passedOrderId] as [String : Any]

        WebServiceCall.callMethodWithURL(route: APIRouter.CompleteOrder(reqParam)) { (_ responseCode: Int?, _ responseValue : [String : AnyObject]?, _ error : Error?) in

            GlobalMethods.dismissLoaderView()
            
            if (responseValue != nil) {
                
                let responseModel = OrderModel(dictionary: responseValue! as NSDictionary)
                
                if responseModel.status == 200{
                    
                    self.arryOrder = responseModel.data
                    self.afterGetOrder()
                }
                else {
                    self.afterGetOrder()
                }
            }
            else {
                GlobalMethods.showErrorFromAPI(responseCode, responseValue, error, viewCtr: self)
            }
        }
    }
    
    func getCancelOrderListAPI(){
            
        GlobalMethods.presentLoaderViewCtr(withViewController: self)
        
        let reqParam = [ParameterKeys.User_Id : kAppDelegate.userLoginData.id, ParameterKeys.Order_id : passedOrderId] as [String : Any]

        WebServiceCall.callMethodWithURL(route: APIRouter.ShowCancelOrder(reqParam)) { (_ responseCode: Int?, _ responseValue : [String : AnyObject]?, _ error : Error?) in

            GlobalMethods.dismissLoaderView()
            
            if (responseValue != nil) {
                
                let responseModel = OrderModel(dictionary: responseValue! as NSDictionary)
                
                if responseModel.status == 200{
                    
                    self.arryOrder = responseModel.data
                    self.afterGetOrder()
                }
                else {
                    self.afterGetOrder()
                }
            }
            else {
                GlobalMethods.showErrorFromAPI(responseCode, responseValue, error, viewCtr: self)
            }
        }
    }
    
    func afterGetOrder(){
        if arryOrder.count != 0{
            self.tblOrder.reloadData()
        }else{
            tblOrder.emptyDataSetSource = self
            tblOrder.emptyDataSetDelegate = self
            self.tblOrder.reloadData()
        }
    }
    
    func moveToOrderDetail(withOrderModel orderModel: OrderDataModel){
        let obj_Order = self.storyboard?.instantiateViewController(withIdentifier: "OrderViewCtr") as! OrderViewCtr
        obj_Order.objOrder = orderModel
        self.navigationController?.pushViewController(obj_Order, animated: true)
    }
    
    func cancelOrderApi(){

        GlobalMethods.presentLoaderViewCtr(withViewController: self)
        
        WebServiceCall.callMethodWithURL(route: APIRouter.GetCancelOrder(withOrderId: passedOrderId, userId: kAppDelegate.userLoginData.id)) { (_ responseCode: Int?, _ responseValue : [String : AnyObject]?, _ error : Error?) in

            GlobalMethods.dismissLoaderView()
            
            if (responseValue != nil) {
                
                let responseModel = OrderCancelModel(dictionary: responseValue! as NSDictionary)
                
                if responseModel.status == 200{
                    self.navigationController?.popViewController(animated: true)
                }
                else {
                    GlobalMethods.showAlert(alertTitle: StringFile.strError[kAppDelegate.userCurrentLanguage], alertMessage: StringFile.msgNeedProperDetails[kAppDelegate.userCurrentLanguage], viewCtr: self, completion: nil)
                }
            }
            else {
                GlobalMethods.showErrorFromAPI(responseCode, responseValue, error, viewCtr: self)
            }
        }
    }

}

extension AllOrderViewCtr : UITableViewDelegate, UITableViewDataSource {

    //MARK:- UITableView Methods
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arryOrder.count
    }

    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
    
        let cell = tableView.dequeueReusableCell(withIdentifier: orderCell, for: indexPath) as! OrderCell
        cell.viewBack.backgroundColor = kCurrentOrderLightGray
        let objOrder = arryOrder[indexPath.row]
        cell.lblProductName.text = objOrder.product_name
        cell.lblPrise.text = "₹\(objOrder.sale_price)"
        cell.lblOldPrise.text = objOrder.price
        cell.lblPerset.text = StringFile.strPerSet[kAppDelegate.userCurrentLanguage]
        cell.lblQty.text = "\(StringFile.strQty[kAppDelegate.userCurrentLanguage]) : \(objOrder.quantity)"
        cell.imgProduct.sd_setImage(with: objOrder.coverImgUrl, placeholderImage: kProductImagePlaceHolder)
        cell.lblDate.text = DateGeneric.convertTime(withOldFormat: "yyyy-MM-dd HH:mm:ss", withNewFormat: "dd/MM/yyyy", strTime: objOrder.created_at)
        return cell
    }
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        self.moveToOrderDetail(withOrderModel: self.arryOrder[indexPath.row])
    }
}

// MARK: - Empty Data Set Methods
extension AllOrderViewCtr : EmptyDataSetSource, EmptyDataSetDelegate{
    
    func emptyDataSetShouldDisplay(_ scrollView: UIScrollView) -> Bool {
        return true
    }
    
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        let str = StringFile.strNoDataFound[kAppDelegate.userCurrentLanguage]
        let font = kCustom_Font(fontName: kPoppin_SemiBold, fontSize: 18.0)
        let attributes = [NSAttributedString.Key.font: font]
        let attributedStr = NSAttributedString(string: str, attributes: attributes)
        return attributedStr
    }
    
    func image(forEmptyDataSet scrollView: UIScrollView) -> UIImage? {
        return UIImage.init(named: "nodata")
    }
    
    func spaceHeight(forEmptyDataSet scrollView: UIScrollView) -> CGFloat {
        return 20.0
    }
}

