//
//  ChangeLanguageViewCtr.swift
//  KhedutBolo
//
//  Created by Trivedi Sagar on 11/1/19.
//  Copyright © 2019 mac. All rights reserved.
//
import LGSideMenuController
import UIKit

class ChangeLanguageViewCtr: UIViewController {
    
    // MARK: - define IBOutlet
    @IBOutlet weak var lblTopTitle: UILabel!
    @IBOutlet weak var viewEnglish: UIView!
    @IBOutlet weak var lblEnglish: UILabel!
    @IBOutlet weak var imgEnglish: UIImageView!
    
    @IBOutlet weak var viewHindi: UIView!
    @IBOutlet weak var lblHindi: UILabel!
    @IBOutlet weak var imgHindi: UIImageView!
    
    @IBOutlet weak var viewGujarati: UIView!
    @IBOutlet weak var lblGujarati: UILabel!
    @IBOutlet weak var imgGujarati: UIImageView!
    
    @IBOutlet weak var btnSubmit: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.setupInitialView()
    }
    override func viewDidDisappear(_ animated: Bool) {
        GlobalMethods.updateLanguageForCurrentUser()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.setLanguageView(withTypeId: lan)
    }
    //MARK:- Class Methods and Functions
    func setupInitialView(){
        lblEnglish.text = StringFile.strEnglishTitle
        lblHindi.text = StringFile.strHindiTitle
        lblGujarati.text = StringFile.strGujaratiTitle
        viewEnglish.dropShadow(color: UIColor.darkGray, opacity: 0.4, shadowRadius: 5.0, cornerRadius: 10.0)
        viewHindi.dropShadow(color: UIColor.darkGray, opacity: 0.4, shadowRadius: 5.0, cornerRadius: 10.0)
        viewGujarati.dropShadow(color: UIColor.darkGray, opacity: 0.4, shadowRadius: 5.0, cornerRadius: 10.0)
        
        btnSubmit.dropShadow(color: UIColor.darkGray, opacity: 0.0, shadowRadius: 5.0, cornerRadius: 10.0)
        btnSubmit.backgroundColor = kAppThemeGreenDarkColor
    
        self.setLanguageView(withTypeId: kAppDelegate.userCurrentLanguage)
    }
    
    func setStringSetup(){
        lblTopTitle.text = StringFile.strChangeLanguage[kAppDelegate.userCurrentLanguage]
        btnSubmit.setTitle(StringFile.strSetAsDefault[kAppDelegate.userCurrentLanguage], for: .normal)
       // GlobalMethods.updateLanguageForCurrentUser()
        GlobalMethods.dismissLoaderView()
    }
    
    func setLanguageView(withTypeId sender:Int){
         
        imgEnglish.image = UIImage.init(named: "uncheck")
        imgHindi.image = UIImage.init(named: "uncheck")
        imgGujarati.image = UIImage.init(named: "uncheck")
        
        viewEnglish.backgroundColor = UIColor.white
        viewHindi.backgroundColor = UIColor.white
        viewGujarati.backgroundColor = UIColor.white
        
        lblEnglish.textColor = UIColor.black
        lblHindi.textColor = UIColor.black
        lblGujarati.textColor = UIColor.black
        
        if sender == Language.kEnglish.rawValue{
            imgEnglish.image = UIImage.init(named: "check")
            viewEnglish.backgroundColor = kAppThemeGreenDarkColor
            lblEnglish.textColor = UIColor.white
        }else if sender == Language.kHindi.rawValue{
            imgHindi.image = UIImage.init(named: "check")
            viewHindi.backgroundColor = kAppThemeGreenDarkColor
            lblHindi.textColor = UIColor.white
        }else{
            imgGujarati.image = UIImage.init(named: "check")
            viewGujarati.backgroundColor = kAppThemeGreenDarkColor
            lblGujarati.textColor = UIColor.white
        }
        kAppDelegate.userCurrentLanguage = sender
        self.setStringSetup()
     }
    
    //MARK:- IBActions
    @IBAction func clickOnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickOnEnglish(_ sender: Any) {
        lan = Language.kEnglish.rawValue
        print("lan 2 english:\(lan)")
        kAppDelegate.userCurrentLanguage = Language.kEnglish.rawValue
        self.setLanguageView(withTypeId: Language.kEnglish.rawValue)
    }
    
    @IBAction func clickOnHindi(_ sender: Any) {
        lan = Language.kHindi.rawValue
        print("lan 2:\(lan)")
        kAppDelegate.userCurrentLanguage = Language.kHindi.rawValue
        self.setLanguageView(withTypeId: Language.kHindi.rawValue)
    }
    
    @IBAction func clickOnGujarati(_ sender: Any) {
        lan = Language.kGujarati.rawValue
        print("lan 2:\(lan)")
        kAppDelegate.userCurrentLanguage = Language.kGujarati.rawValue
        self.setLanguageView(withTypeId: Language.kGujarati.rawValue)
    }
    
    @IBAction func clickOnSubmit(_ sender: Any) {
        GlobalMethods.presentLoaderViewCtr(withViewController: self)
        print("lan submit:\(lan)")
        self.updateProfileLanguage()
        let obj_HomeViewCtr = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewCtr") as! HomeViewCtr

        let nav_HomeViewCtr = UINavigationController(rootViewController: obj_HomeViewCtr)
        nav_HomeViewCtr.isNavigationBarHidden = true

        let obj_LeftMenuViewCtr = self.storyboard?.instantiateViewController(withIdentifier: "LeftMenuViewCtr") as! LeftMenuViewCtr

        let sideMenuViewCtr = LGSideMenuController()
        sideMenuViewCtr.isLeftViewSwipeGestureEnabled = false
        sideMenuViewCtr.rootViewController = nav_HomeViewCtr
        sideMenuViewCtr.leftViewController = obj_LeftMenuViewCtr

        sideMenuViewCtr.leftViewPresentationStyle = .slideAbove
        sideMenuViewCtr.leftViewWidth = SystemSize.width - (SystemSize.width/4)
        GlobalMethods.dismissLoaderView()
        self.navigationController?.pushViewController(sideMenuViewCtr, animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    func updateProfileLanguage(){
        GlobalMethods.presentLoaderViewCtr(withViewController: self)
        let selectedlang = GlobalMethods.getCurrentLanguageCode()
        let lan1 = GlobalMethods.getCurrentLanguageCodeLan()
        print("lan updatedprofile:\(lan1)")
        let reqParam = [ParameterKeys.Id: kAppDelegate.userLoginData.id, ParameterKeys.Mobile: kAppDelegate.userLoginData.mobile, ParameterKeys.Name: kAppDelegate.userLoginData.name, ParameterKeys.Address: "", ParameterKeys.Language: selectedlang, ParameterKeys.Sid : kAppDelegate.userLoginData.state_id, ParameterKeys.Cid : kAppDelegate.userLoginData.city_id, ParameterKeys.Tid : kAppDelegate.userLoginData.tehsil_id, ParameterKeys.Vid : kAppDelegate.userLoginData.village_id, ParameterKeys.User_Id : kAppDelegate.userLoginData.id] as [String : Any]
        
        
         WebServiceCall.uploadImageWithURL(route: APIRouter.UpdateProfile(reqParam), imageArray : nil) { (_ responseCode: Int?, _ responseValue : [String : AnyObject]?, _ error : Error?) in
                
          //  GlobalMethods.dismissLoaderView()
            
            if (responseValue != nil) {
                
                let responseModel = ResponseModelNew(dictionary: responseValue! as NSDictionary)
                
                if responseModel.status == 200 && responseModel.dataDict != nil {
                    kAppDelegate.userLoginData.delete()
                    kAppDelegate.userLoginData = LoginUserModel(dictionary: responseModel.dataDict! as NSDictionary)
                    kAppDelegate.userLoginData.save()
                    print("userLogin1:\(kAppDelegate.userLoginData.language)")
                    let alertViewController = UIAlertController(title: StringFile.strThankYou[kAppDelegate.userCurrentLanguage], message: StringFile.msgSuccessChangeLanuage[kAppDelegate.userCurrentLanguage], preferredStyle: .alert)
                    
                    let okAction = UIAlertAction(title: StringFile.strOK[kAppDelegate.userCurrentLanguage], style: .cancel) { (action) -> Void in
                        
                        NotificationCenter.default.post(name: Notification.Name(kHomeViewUpdateNeed), object: nil)
                      //  GlobalMethods.dismissLoaderView()
                        //self.navigationController?.popToRootViewController(animated: true)
                        let obj_HomeViewCtr = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewCtr") as! HomeViewCtr

                        let nav_HomeViewCtr = UINavigationController(rootViewController: obj_HomeViewCtr)
                        nav_HomeViewCtr.isNavigationBarHidden = true

                        let obj_LeftMenuViewCtr = self.storyboard?.instantiateViewController(withIdentifier: "LeftMenuViewCtr") as! LeftMenuViewCtr

                        let sideMenuViewCtr = LGSideMenuController()
                        sideMenuViewCtr.isLeftViewSwipeGestureEnabled = false
                        sideMenuViewCtr.rootViewController = nav_HomeViewCtr
                        sideMenuViewCtr.leftViewController = obj_LeftMenuViewCtr

                        sideMenuViewCtr.leftViewPresentationStyle = .slideAbove
                        sideMenuViewCtr.leftViewWidth = SystemSize.width - (SystemSize.width/4)
                        GlobalMethods.dismissLoaderView()
                        self.navigationController?.pushViewController(sideMenuViewCtr, animated: true)
                        
                    }
                    
                    alertViewController.addAction(okAction)
                    GlobalMethods.dismissLoaderView()
                    self.present(alertViewController, animated: true, completion: nil)
                    
//                    GlobalMethods.showAlert(alertTitle: StringFile.strThankYou[kAppDelegate.userCurrentLanguage], alertMessage: StringFile.msgSuccessChangeLanuage[kAppDelegate.userCurrentLanguage], viewCtr: self) {
//
//                        self.navigationController?.popToRootViewController(animated: true)
//                        NotificationCenter.default.post(name: Notification.Name(kHomeViewUpdateNeed), object: nil)
////                    }
                }
                else {
                    GlobalMethods.dismissLoaderView()
                    GlobalMethods.showAlert(alertTitle: StringFile.strError[kAppDelegate.userCurrentLanguage], alertMessage: StringFile.msgNeedProperDetails[kAppDelegate.userCurrentLanguage], viewCtr: self, completion: nil)
                }
            }
            else {
                GlobalMethods.dismissLoaderView()
                GlobalMethods.showErrorFromAPI(responseCode, responseValue, error, viewCtr: self)
            }
        }
    }
}
