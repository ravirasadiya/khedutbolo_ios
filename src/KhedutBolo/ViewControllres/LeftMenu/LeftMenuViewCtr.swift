//
//  LeftMenuViewCtr.swift


import UIKit
import LGSideMenuController


class LeftMenuViewCtr: UIViewController {
    
    //MARK:- Outlets

    @IBOutlet weak var tblMenu: UITableView!

    //MARK:- Variables
    var arrayMenu = [LeftMenuModel]()
    var isUserProfileUpdated : Bool = false
    
    var userDetailDict : [String : Any]!
    
    //MARK:- UIView Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.setupInitialView()
        /*~~~~~ notification  ~~~~~*/        
        NotificationCenter.default.addObserver(self, selector: #selector(moveToLoginPage), name: NSNotification.Name(rawValue: kLogoutMoveLoginPage), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateHomeView), name: NSNotification.Name(rawValue: kHomeViewUpdateNeed), object: nil)

    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
    }
    @objc func updateHomeView(){
        print(kAppDelegate.userCurrentLanguage)
        setupInitialView()
    }
    //MARK:- Class Methods and Functions
    func setupInitialView(){
        
        tblMenu.registerCell(cellIDs: [leftMenuCell], isDynamicHeight: true, estimatedHeight: 60)
        
        let leftMenuArry = [["Title" : StringFile.strMyProfile[lan], "LeftMenuType" : LeftMenuType.MyProfile, "imgName": "avatar"],
                            ["Title" : StringFile.strCurrentOrder[lan], "LeftMenuType" : LeftMenuType.CurrentOrder, "imgName": "order"],
                            ["Title" : StringFile.strCompleteOrder[lan], "LeftMenuType" : LeftMenuType.CompleteOrder, "imgName": "shopping-bag"],
                            ["Title" : StringFile.strCancelOrder[lan], "LeftMenuType" : LeftMenuType.CancelOrder, "imgName": "menu-shopping-cart"], ["Title" : StringFile.strWallet[lan], "LeftMenuType" : LeftMenuType.Wallet, "imgName": "Icon awesome-wallet"],
                            ["Title" : StringFile.strChangeLanguage[lan], "LeftMenuType" : LeftMenuType.ChangeLanguage, "imgName": "translation"],
                            ["Title" : StringFile.strShareApp[lan], "LeftMenuType" : LeftMenuType.ShareApp, "imgName": "share"],
                            ["Title" : StringFile.strLogout[lan], "LeftMenuType" : LeftMenuType.Logout, "imgName": "logout"]]
        
        arrayMenu = [LeftMenuModel](dictionaryArray: leftMenuArry as [NSDictionary])
    }

    @objc func moveToLoginPage(){
        GlobalMethods.logoutMoveToLoginScreen(withNavigation: self.navigationController)
    }
}

extension LeftMenuViewCtr : UITableViewDelegate, UITableViewDataSource {
    
    //MARK:- UITableView Methods
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrayMenu.count;
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: leftMenuCell, for: indexPath) as! LeftMenuCell
        
        let menuModel = arrayMenu[indexPath.row]
        cell.lblMenuTitle.text = menuModel.Title
        cell.imgTitle.image = UIImage.init(named: menuModel.ImgName)
        
        return cell
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let menuModel = arrayMenu[indexPath.row]
        
        
        if menuModel.LeftMenuType == LeftMenuType.CurrentOrder {
            
            let obj_CurrentOrderViewCtr = self.storyboard?.instantiateViewController(withIdentifier: "CurrentOrderViewCtr") as! CurrentOrderViewCtr
            
            if let obj_NavCtr = self.sideMenuController?.rootViewController as? UINavigationController {
                obj_NavCtr.pushViewController(obj_CurrentOrderViewCtr, animated: true)
            }
            
            sideMenuController?.hideLeftView()
        }
        else if menuModel.LeftMenuType == LeftMenuType.CompleteOrder {

            let obj_CompleteOrderViewCtr = self.storyboard?.instantiateViewController(withIdentifier: "CompleteOrderViewCtr") as! CompleteOrderViewCtr
            
            if let obj_NavCtr = self.sideMenuController?.rootViewController as? UINavigationController {
                obj_NavCtr.pushViewController(obj_CompleteOrderViewCtr, animated: true)
            }
            
            sideMenuController?.hideLeftView()
        }
        else if menuModel.LeftMenuType == LeftMenuType.CancelOrder {

            let obj_CancelOrderViewCtr = self.storyboard?.instantiateViewController(withIdentifier: "CancelOrderViewCtr") as! CancelOrderViewCtr
            
            if let obj_NavCtr = self.sideMenuController?.rootViewController as? UINavigationController {
                obj_NavCtr.pushViewController(obj_CancelOrderViewCtr, animated: true)
            }
            
            sideMenuController?.hideLeftView()
        }
        else if menuModel.LeftMenuType == LeftMenuType.Wallet {

            let obj_WalletViewCtr = self.storyboard?.instantiateViewController(withIdentifier: "WalletViewCtr") as! WalletViewCtr
            
            if let obj_NavCtr = self.sideMenuController?.rootViewController as? UINavigationController {
                obj_NavCtr.pushViewController(obj_WalletViewCtr, animated: true)
            }
                
            sideMenuController?.hideLeftView()
        }
        else if menuModel.LeftMenuType == LeftMenuType.ChangeLanguage {

            let obj_ChangeLanViewCtr = self.storyboard?.instantiateViewController(withIdentifier: "ChangeLanguageViewCtr") as! ChangeLanguageViewCtr
            
            if let obj_NavCtr = self.sideMenuController?.rootViewController as? UINavigationController {
                obj_NavCtr.pushViewController(obj_ChangeLanViewCtr, animated: true)
            }
            
            sideMenuController?.hideLeftView()
        }
        else if menuModel.LeftMenuType == LeftMenuType.MyProfile {

            let obj_ProfileViewCtr = self.storyboard?.instantiateViewController(withIdentifier: "MyProfileViewCtr") as! MyProfileViewCtr
            
            if let obj_NavCtr = self.sideMenuController?.rootViewController as? UINavigationController {
                obj_NavCtr.pushViewController(obj_ProfileViewCtr, animated: true)
            }
            
            sideMenuController?.hideLeftView()
        }
        else if menuModel.LeftMenuType == LeftMenuType.Logout {
            GlobalMethods.showAlert(alertTitle: StringFile.strAppName[kAppDelegate.userCurrentLanguage], alertMessage: StringFile.msgLogoutConfirm[kAppDelegate.userCurrentLanguage], okActionTitle: StringFile.strLogout[kAppDelegate.userCurrentLanguage], cancelActionTitle: StringFile.strCancel[kAppDelegate.userCurrentLanguage], viewCtr: self, successCompletion: {
                self.logoutApiCall()
            }) {
                //
            }
        }
        else if menuModel.LeftMenuType == LeftMenuType.ShareApp {
            GlobalMethods.shareActivity(withText: "", withShareUrl: "www.google.com", withViewController: self)
        }
    /*    else if menuModel.LeftMenuType == LeftMenuType.RideHistory {
            
            if FirebaseAPI.instance.isAnonymousUser {
                GlobalMethods.presentToSignUpAlertViewCtr(withViewController: self)
            }
            else {
                let obj_MyRidesViewCtr = self.storyboard?.instantiateViewController(withIdentifier: "MyRidesViewCtr") as! MyRidesViewCtr
                
                if let obj_NavCtr = self.sideMenuController?.rootViewController as? UINavigationController {
                    obj_NavCtr.pushViewController(obj_MyRidesViewCtr, animated: true)
                }
                
                sideMenuController?.hideLeftView()
            }
        }
        else if menuModel.LeftMenuType == LeftMenuType.Leaderboard {
            
            if FirebaseAPI.instance.isAnonymousUser {
                GlobalMethods.presentToSignUpAlertViewCtr(withViewController: self)
            }
            else {
                let obj_LeaderBoradViewCtr = self.storyboard?.instantiateViewController(withIdentifier: "LeaderBoradViewCtr") as! LeaderBoradViewCtr
                
                if let obj_NavCtr = self.sideMenuController?.rootViewController as? UINavigationController {
                    obj_NavCtr.pushViewController(obj_LeaderBoradViewCtr, animated: true)
                }
                
                sideMenuController?.hideLeftView()
            }
        }
        else if menuModel.LeftMenuType == LeftMenuType.ReferEarn {
            
            self.pushToReferEarnViewCtr()
        }
        else if menuModel.LeftMenuType == LeftMenuType.PrivacyPolicy {
            GlobalMethods.openSafariController(withURL: kPrivacyURL, withViewController: self)
            sideMenuController?.hideLeftView()
        }
        else if menuModel.LeftMenuType == LeftMenuType.TermsofUse {
            GlobalMethods.openSafariController(withURL: kTermsOfUseURL, withViewController: self)
            sideMenuController?.hideLeftView()
        }
        else if menuModel.LeftMenuType == LeftMenuType.Help {
            GlobalMethods.openSafariController(withURL: kHelpURL, withViewController: self)
            sideMenuController?.hideLeftView()
        }
        else if menuModel.LeftMenuType == LeftMenuType.Support {
            GlobalMethods.openSafariController(withURL: kSupportURL, withViewController: self)
            sideMenuController?.hideLeftView()
        }
        else {
            sideMenuController?.hideLeftView()
        }*/
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    func logoutApiCall(){
        GlobalMethods.presentLoaderViewCtr(withViewController: self)
    
        GlobalMethods.userLogoutApiCall(withToken: "") { (isSuccess) in
            
            GlobalMethods.dismissLoaderView()
        
            if isSuccess{
                GlobalMethods.logoutMoveToLoginScreen(withNavigation: self.navigationController)
            }
        }
    }
}
