//
//  CompanyListViewCtr.swift
//  KhedutBolo
//
//  Created by Trivedi Sagar on 10/31/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit

class CompanyListViewCtr: UIViewController {
    
    // MARK: - define IBOutlet
    @IBOutlet weak var lblTopTitle: UILabel!
    @IBOutlet weak var cvList: UICollectionView!
    
    var arrCompanyList = [CompanyModel]()
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
         cvList.register(UINib(nibName: companyListCCell, bundle: nil), forCellWithReuseIdentifier: companyListCCell)
        self.setupInitialView()
    }
    
    //MARK:- Class Methods and Functions
    func setupInitialView(){
        lblTopTitle.text = StringFile.strCompanies[kAppDelegate.userCurrentLanguage]
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    
    //MARK:- IBActions
    @IBAction func clickOnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
// MARK: - UICollectionView Delegate, DataSource Methods
extension CompanyListViewCtr : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrCompanyList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: companyListCCell, for: indexPath) as! CompanyListCCell
        
        let objModel = self.arrCompanyList[indexPath.row]
        cell.imgTitle.sd_setImage(with: objModel.coverImgUrl, placeholderImage: kCompanyImagePlaceHolder)
        cell.lblTitle.text = objModel.name
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let cellWidth = (SystemSize.width - 65)/2 //50
        let cellSize  = CGSize(width: cellWidth, height: (cellWidth/2) + 65) //50
        return cellSize
       
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let objModel = self.arrCompanyList[indexPath.row]

        let obj_CatView = self.storyboard?.instantiateViewController(withIdentifier: "CompanyCategoryListViewCtr") as! CompanyCategoryListViewCtr
        obj_CatView.companyId = objModel.id
        obj_CatView.companyName = objModel.name
        self.navigationController?.pushViewController(obj_CatView, animated: true)
    }

}

