//
//  CompanyCategoryListViewCtr.swift
//  KhedutBolo
//
//  Created by Trivedi Sagar on 10/31/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit
import EmptyDataSet_Swift


class CompanyCategoryListViewCtr: UIViewController {

    // MARK: - define IBOutlet
    @IBOutlet weak var cvCategory: UICollectionView!
    @IBOutlet weak var lblTopTitle: UILabel!

    var companyId : UInt = 0
    var companyName = ""
    var allProductArray = [CompanyModel]()
    var storedOffsets = [Int: CGFloat]()
    var IsCompanyCatCalled : Bool = false
    var viewCtr = UIViewController()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.setupInitialView()
    }
    
    //MARK:- Class Methods and Functions
    func setupInitialView(){
        lblTopTitle.text = StringFile.strCompanyName[lan]//companyName
        cvCategory.register(UINib(nibName: prodCatCCell, bundle: nil), forCellWithReuseIdentifier: prodCatCCell)
        self.getCompanyProductAPI(withCompanyId: companyId)

        viewCtr = self
    }
    
    //MARK:- IBActions
    @IBAction func clickOnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - Web Service Function Methods
    func getCompanyProductAPI(withCompanyId cId: UInt){

        GlobalMethods.presentLoaderViewCtr(withViewController: self)
        
        let reqParam = [ParameterKeys.Company_id : cId, ParameterKeys.User_Id : kAppDelegate.userLoginData.id] as [String : Any]

        WebServiceCall.callMethodWithURL(route: APIRouter.CompanyProducts(reqParam)) { (_ responseCode: Int?, _ responseValue : [String : AnyObject]?, _ error : Error?) in

            GlobalMethods.dismissLoaderView()
            
            if (responseValue != nil) {
                
                let responseModel = CompanyModelResponse(dictionary: responseValue! as NSDictionary)
                
                if responseModel.status == 200{
                    self.allProductArray = responseModel.data
                    self.setResponseCompanyProduct()
                }
                else {
                    self.setCollectionViewData()
                  //  GlobalMethods.showAlert(alertTitle: StringFile.strError[kAppDelegate.userCurrentLanguage], alertMessage: responseModel.message, viewCtr: self, completion: nil)
                }
            }
            else {
                GlobalMethods.showErrorFromAPI(responseCode, responseValue, error, viewCtr: self)
            }
        }
    }
    
    
    
    func setResponseCompanyProduct(){
                
        self.setCollectionViewData()

    }
    
    func setCollectionViewData(){
        if allProductArray.count != 0{
            self.cvCategory.reloadData()
        }else{
            cvCategory.emptyDataSetSource = self
            cvCategory.emptyDataSetDelegate = self
            self.cvCategory.reloadData()
        }
    }

}

extension CompanyCategoryListViewCtr : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.allProductArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
       
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: prodCatCCell, for: indexPath) as! ProdCatCCell
        
            let objProduct = allProductArray[indexPath.row]

            cell.lblName.text = objProduct.name
            cell.lblPrice.text = objProduct.price
            cell.lblSalePrice.text = objProduct.sale_price
            cell.lblPerSet.text = StringFile.strPerSet[kAppDelegate.userCurrentLanguage]
            cell.lblDealerName.text = "\(StringFile.strBy[lan]) : \(objProduct.company_name)"
            cell.imgProduct.sd_setImage(with: objProduct.coverImgUrl, placeholderImage: kProductImagePlaceHolder)

        
        return cell
       
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let cellWidth = (SystemSize.width-65)/2 //50
        let cellSize  = CGSize(width: cellWidth, height: cellWidth + 84) //72
        return cellSize
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        let prodId = "\(allProductArray[indexPath.row].id)"
        
        GlobalMethods.pushToProductCtr(withViewController: viewCtr, animated: true, productId: prodId)

    }
}


// MARK: - Empty Data Set Methods
extension CompanyCategoryListViewCtr : EmptyDataSetSource, EmptyDataSetDelegate{
    
    func emptyDataSetShouldDisplay(_ scrollView: UIScrollView) -> Bool {
        return true
    }
    
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        let str = StringFile.strNoDataFound[lan]
        let font = kCustom_Font(fontName: kPoppin_SemiBold, fontSize: 18.0)
        let attributes = [NSAttributedString.Key.font: font]
        let attributedStr = NSAttributedString(string: str, attributes: attributes)
        return attributedStr
    }
    
    func image(forEmptyDataSet scrollView: UIScrollView) -> UIImage? {
        return UIImage.init(named: "nodata")
    }
    
    func spaceHeight(forEmptyDataSet scrollView: UIScrollView) -> CGFloat {
        return 20.0
    }
}
