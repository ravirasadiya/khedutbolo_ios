//
//  AllProductsViewCtr.swift
//  KhedutBolo
//
//  Created by Trivedi Sagar on 10/25/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit
import EmptyDataSet_Swift

class AllProductsViewCtr: UIViewController {

    // MARK: - define IBOutlet
    @IBOutlet weak var lblCartCount: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var cvProducts: UICollectionView!
    @IBOutlet weak var btnRighMenu: UIButton!
    @IBOutlet weak var btnCart: UIButton!
    @IBOutlet weak var btnSearch: UIButton!
    
    @IBOutlet weak var viewFilter: UIView!
    @IBOutlet weak var tblFilter: UITableView!
    
    @IBOutlet weak var viewBottom: UIView!
    @IBOutlet weak var btnClear: UIButton!
    @IBOutlet weak var btnApplyFilter: UIButton!
    
    var arrAllProduct = [ProductDetailModel]()


    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        cvProducts.register(UINib(nibName: prodCatCCell, bundle: nil), forCellWithReuseIdentifier: prodCatCCell)
       
        tblFilter.registerCell(cellIDs: [filterCell], isDynamicHeight: true, estimatedHeight: 40)
        
        tblFilter.tableFooterView = viewBottom
        
        self.setupInitialView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if kAppDelegate.cartCount > 0{
            lblCartCount.isHidden = false
            lblCartCount.text = "\(kAppDelegate.cartCount)"
        }else{
            lblCartCount.isHidden = true
        }
    }
    
    //MARK:- Class Methods and Functions
    func setupInitialView(){
        
        btnClear.layer.cornerRadius = 10
        btnClear.layer.masksToBounds = true
        
        btnApplyFilter.layer.cornerRadius = 10
        btnApplyFilter.layer.masksToBounds = true
        
        self.viewFilter.isHidden = true
        
        lblCartCount.layer.cornerRadius = lblCartCount.bounds.width/2
        lblCartCount.layer.masksToBounds = true
        lblCartCount.isHidden = true
        self.setViewStringSetup()
    }
    
    func setViewStringSetup(){
        lblTitle.text = StringFile.strTrendingProducts[lan]
    }
    
    //MARK:- IBActions
    @IBAction func clickOnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickOnSearch(_ sender: Any) {
        
    }
    
    @IBAction func clickOnCart(_ sender: Any) {
        let obj_CartView = self.storyboard?.instantiateViewController(withIdentifier: "CartViewCtr") as! CartViewCtr
        self.navigationController?.pushViewController(obj_CartView, animated: true)
    }
    
    @IBAction func clickOnRightMenu(_ sender: Any) {
        if viewFilter.isHidden{
            btnRighMenu.setImage(UIImage.init(named: "filter_after"), for: .normal)
            self.setViewHideShowWithAnimarion(view: viewFilter, hidden: false)
        }else{
            self.setViewHideShowWithAnimarion(view: viewFilter, hidden: true)
            btnRighMenu.setImage(UIImage.init(named: "filter_before"), for: .normal)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    

}

// MARK: - UICollectionView Delegate, DataSource Methods
extension AllProductsViewCtr : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrAllProduct.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: prodCatCCell, for: indexPath) as! ProdCatCCell
        let objModel = self.arrAllProduct[indexPath.row]
        cell.lblName.text = objModel.name
        cell.lblPrice.text = "₹\(objModel.price)"
        cell.lblSalePrice.text = "₹\(objModel.sale_price)"
        cell.lblPerSet.text = StringFile.strPerSet[kAppDelegate.userCurrentLanguage]
        cell.lblDealerName.text = "\(StringFile.strBy[lan]) : \(objModel.company_name)"
        cell.imgProduct.sd_setImage(with: objModel.coverImgUrl, placeholderImage: kProductImagePlaceHolder)
        return cell
       
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let cellWidth = (SystemSize.width-50)/2
        let cellSize  = CGSize(width: cellWidth, height: cellWidth + 72)
        return cellSize
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
    
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let obj_ProdView = self.storyboard?.instantiateViewController(withIdentifier: "ProductViewCtr") as! ProductViewCtr
        obj_ProdView.productId = "\(arrAllProduct[indexPath.row].id)"
        self.navigationController?.pushViewController(obj_ProdView, animated: true)
    }
}


extension AllProductsViewCtr : UITableViewDelegate, UITableViewDataSource {

    //MARK:- UITableView Methods
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView.init()
        headerView.backgroundColor = UIColor.clear
        
        let lblSecTitle = UILabel.init(frame: CGRect(x: 8, y: 8, width: 123, height: 25))
        lblSecTitle.textColor = UIColor.white
        
        var strTitle = StringFile.strTechnicalName[kAppDelegate.userCurrentLanguage]
        if section == 1{
            strTitle = StringFile.strCompanyName[kAppDelegate.userCurrentLanguage]
        }
        
        let lblUnderline = UILabel.init(frame: CGRect(x: 8, y: 30, width: 123, height: 1))
        lblUnderline.backgroundColor = UIColor.white
        
        lblSecTitle.text = strTitle

        headerView.addSubview(lblSecTitle)
        headerView.addSubview(lblUnderline)
        
        return headerView
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 4
    }

    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
    
        let cell = tableView.dequeueReusableCell(withIdentifier: filterCell, for: indexPath) as! FilterCell
        
        var strTitle = StringFile.strItemName[kAppDelegate.userCurrentLanguage]
        
        if indexPath.section == 1{
            strTitle = StringFile.strCompanyName[kAppDelegate.userCurrentLanguage]
        }
        
        cell.imgCheck.image = UIImage.init(named: "uncheckBox")
        
        if indexPath.row == 0{
            cell.imgCheck.image = UIImage.init(named: "checkBox")
        }
        
        cell.lblTitle.text = "\(strTitle) \(indexPath.row)"
        
        return cell
    }

    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {}
    
    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
}

