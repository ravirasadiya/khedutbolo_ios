//
//  ProdCatCCell.swift
//  KhedutBolo
//
//  Created by Trivedi Sagar on 10/25/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit

class ProdCatCCell: UICollectionViewCell {

    // MARK: - define IBOutlet start
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var viewImgProductBack: UIView!

    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblSalePrice: UILabel!
    @IBOutlet weak var lblPerSet: UILabel!
    @IBOutlet weak var lblMarker: UILabel!
    @IBOutlet weak var lblDealerName: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.viewImgProductBack.dropShadow(color: UIColor.lightGray, opacity: 0.4, shadowRadius: 3.0, cornerRadius: 5.0)

        
        self.imgProduct.layer.cornerRadius = 5
        self.imgProduct.layer.masksToBounds = true
    }

}
