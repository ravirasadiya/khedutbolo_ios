//
//  FilterCell.swift
//  KhedutBolo
//
//  Created by Trivedi Sagar on 11/6/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit

class FilterCell: UITableViewCell {

    // MARK: - define IBOutlet start
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgCheck: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
