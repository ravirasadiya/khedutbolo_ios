//
//  NotificationViewCtr.swift
//  KhedutBolo
//
//  Created by Trivedi Sagar on 11/2/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit
import EmptyDataSet_Swift

class NotificationViewCtr: UIViewController {

    // MARK: - define IBOutlet
    @IBOutlet weak var lblTopTitle: UILabel!
    @IBOutlet weak var tblNotification: UITableView!
    
    var arryNotification = [NotificationDataModel]()
    var yourDict = NSDictionary()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.setupInitialView()
    }
    
    //MARK:- Class Methods and Functions
    func setupInitialView(){
        lblTopTitle.text = StringFile.strNotification[kAppDelegate.userCurrentLanguage]
        tblNotification.registerCell(cellIDs: [notificationCell], isDynamicHeight: true, estimatedHeight: 74)
        
        self.getNotificationListAPI(withCustId: kAppDelegate.userLoginData.id)

    }
    
    //MARK:- IBActions
    @IBAction func clickOnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }*/
    

    //MARK:-  Web Services
    func getNotificationListAPI(withCustId cId: String){
      
        GlobalMethods.presentLoaderViewCtr(withViewController: self)

        let reqParam = [ParameterKeys.User_Id : cId] as [String : Any]

        WebServiceCall.callMethodWithURL(route: APIRouter.GetNotification(reqParam)) { (_ responseCode: Int?, _ responseValue : [String : AnyObject]?, _ error : Error?) in
                
            GlobalMethods.dismissLoaderView()
            
            if (responseValue != nil) {
                
                let responseModel = NotificationModel(dictionary: responseValue! as NSDictionary)
                
                if responseModel.status == 200{
                    
                    self.arryNotification = responseModel.data
                    self.afterGetNoitification()
                }
                else {
                    self.afterGetNoitification()
                }
            }
            else {
                self.afterGetNoitification()
                //GlobalMethods.showErrorFromAPI(responseCode, responseValue, error, viewCtr: self)
            }
        }
    }
    
    func afterGetNoitification(){
        if arryNotification.count != 0{
            self.tblNotification.reloadData()
        }else{
            tblNotification.emptyDataSetSource = self
            tblNotification.emptyDataSetDelegate = self
            self.tblNotification.reloadData()
        }
    }
}

extension NotificationViewCtr : UITableViewDelegate, UITableViewDataSource {

    //MARK:- UITableView Methods
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arryNotification.count
    }

    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
    
        let cell = tableView.dequeueReusableCell(withIdentifier: notificationCell, for: indexPath) as! NotificationCell
        let objModel = self.arryNotification[indexPath.row]
        cell.lblNotification.text = objModel.notify_detail
        cell.lblDate.text = DateGeneric.convertTime(withOldFormat: "yyyy-MM-dd HH:mm:ss", withNewFormat: "dd/MM/yyyy", strTime: objModel.date)
        cell.lblTime.text = DateGeneric.convertTime(withOldFormat: "HH:mm:ss", withNewFormat: "h:mm a", strTime: objModel.time)
        return cell
    }

    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {}
}

// MARK: - Empty Data Set Methods
extension NotificationViewCtr : EmptyDataSetSource, EmptyDataSetDelegate{
    
    func emptyDataSetShouldDisplay(_ scrollView: UIScrollView) -> Bool {
        return true
    }
    
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        let str = StringFile.strNoDataFound[kAppDelegate.userCurrentLanguage]
        let font = kCustom_Font(fontName: kPoppin_SemiBold, fontSize: 18.0)
        let attributes = [NSAttributedString.Key.font: font]
        let attributedStr = NSAttributedString(string: str, attributes: attributes)
        return attributedStr
    }
    
    func image(forEmptyDataSet scrollView: UIScrollView) -> UIImage? {
        return UIImage.init(named: "nodata")
    }
    
    func spaceHeight(forEmptyDataSet scrollView: UIScrollView) -> CGFloat {
        return 20.0
    }
}

