//
//  NotificationCell.swift
//  KhedutBolo
//
//  Created by Trivedi Sagar on 11/2/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit

class NotificationCell: UITableViewCell {
    
    // MARK: - define IBOutlet
    @IBOutlet weak var imgNotification: UIImageView!
    @IBOutlet weak var lblNotification: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblSeprator: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.contentView.backgroundColor = .clear
        self.backgroundColor = .clear
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
