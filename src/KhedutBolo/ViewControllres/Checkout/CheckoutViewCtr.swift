//
//  CheckoutViewCtr.swift
//  KhedutBolo
//
//  Created by Trivedi Sagar on 11/5/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import DropDown

class CheckoutViewCtr: UIViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var hightMainViewConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var viewClientAddress: UIView!
    @IBOutlet weak var tfName: SkyFloatingLabelTextField!
    @IBOutlet weak var tfNumber: SkyFloatingLabelTextField!
    @IBOutlet weak var tfEmail: SkyFloatingLabelTextField!
    @IBOutlet weak var tfPincode: SkyFloatingLabelTextField!
    @IBOutlet weak var tfFullAddress: SkyFloatingLabelTextField!
    @IBOutlet weak var tfTotalPayment: SkyFloatingLabelTextField!
    @IBOutlet weak var btnPayment: UIButton!
    @IBOutlet weak var lblShippingAddress: UILabel!
    @IBOutlet weak var lblSelectedPaymentMethod: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblWalletAmount: UILabel!
    @IBOutlet weak var lblTotalPrice: UILabel!

    @IBOutlet weak var lblCondition: UILabel!
    
    @IBOutlet weak var lblCODTitle: UILabel!
    @IBOutlet weak var lblNetbankingTitle: UILabel!
    @IBOutlet weak var lblShippingAddTitle: UILabel!
    @IBOutlet weak var lblSelectedPaymentMethodTitle: UILabel!
    @IBOutlet weak var lblPriceTitle: UILabel!
    @IBOutlet weak var lblWalletAmountTitle: UILabel!
    @IBOutlet weak var lblTotalPriceTitle: UILabel!


    @IBOutlet weak var tfState: SkyFloatingLabelTextField!{
        didSet{
            tfState.titleFormatter = { $0 }
            tfState.titleColor = kAppThemeGreenDarkColor
            tfState.titleFont = kCustom_Font(fontName: kPoppin_SemiBold, fontSize: 12.0)
            tfState.placeholderFont = kCustom_Font(fontName: kPoppin_Regular, fontSize: 16.0)
            tfState.placeholder = StringFile.strState[kAppDelegate.userCurrentLanguage]
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
            imageView.contentMode = .scaleAspectFit
            imageView.image = UIImage.init(named: "dropDownBlack")
            
            self.tfState.rightViewMode = UITextField.ViewMode.always
            self.tfState.rightView = imageView
            
        }
    }

    @IBOutlet weak var tfCity: SkyFloatingLabelTextField!{
        didSet{
            tfCity.titleFormatter = { $0 }
            tfCity.titleColor = kAppThemeGreenDarkColor
            tfCity.titleFont = kCustom_Font(fontName: kPoppin_SemiBold, fontSize: 12.0)
            tfCity.placeholderFont = kCustom_Font(fontName: kPoppin_Regular, fontSize: 16.0)
            tfCity.placeholder = StringFile.strCity[kAppDelegate.userCurrentLanguage]
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
            imageView.contentMode = .scaleAspectFit
            imageView.image = UIImage.init(named: "dropDownBlack")
            
            self.tfCity.rightViewMode = UITextField.ViewMode.always
            self.tfCity.rightView = imageView
            
        }
    }

    @IBOutlet weak var tfTehsil: SkyFloatingLabelTextField!{
        didSet{
            tfTehsil.titleFormatter = { $0 }
            tfTehsil.titleColor = kAppThemeGreenDarkColor
            tfTehsil.titleFont = kCustom_Font(fontName: kPoppin_SemiBold, fontSize: 12.0)
            tfTehsil.placeholderFont = kCustom_Font(fontName: kPoppin_Regular, fontSize: 16.0)
            tfTehsil.placeholder = StringFile.strTehsil[kAppDelegate.userCurrentLanguage]
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
            imageView.contentMode = .scaleAspectFit
            imageView.image = UIImage.init(named: "dropDownBlack")
            
            self.tfTehsil.rightViewMode = UITextField.ViewMode.always
            self.tfTehsil.rightView = imageView
            
        }
    }

    @IBOutlet weak var tfVillage: SkyFloatingLabelTextField!{
        didSet{
            tfVillage.titleFormatter = { $0 }
            tfVillage.titleColor = kAppThemeGreenDarkColor
            tfVillage.titleFont = kCustom_Font(fontName: kPoppin_SemiBold, fontSize: 12.0)
            tfVillage.placeholderFont = kCustom_Font(fontName: kPoppin_Regular, fontSize: 16.0)
            tfVillage.placeholder = StringFile.strVillage[kAppDelegate.userCurrentLanguage]
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
            imageView.contentMode = .scaleAspectFit
            imageView.image = UIImage.init(named: "dropDownBlack")
            
            self.tfVillage.rightViewMode = UITextField.ViewMode.always
            self.tfVillage.rightView = imageView
        }
    }

    @IBOutlet weak var lblPaymentMethodTitle: UILabel!
    
    @IBOutlet weak var imgCOD: UIImageView!
    @IBOutlet weak var imgNetBanking: UIImageView!
    
    @IBOutlet weak var viewBankDetails: UIView!
    
    @IBOutlet weak var lblOrderSummaryTitle: UILabel!
    
    @IBOutlet weak var topOrderSummaryConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var viewOrderSummary: UIView!
    
    @IBOutlet weak var viewItemDetail: UIView!
    
    @IBOutlet weak var btnComplete: UIButton!
    
    @IBOutlet weak var btnNetBanking: UIButton!
    // MARK: - constants & variables
    
    var payment = "cod"
    
    var arryStateData = [StateDataModel]()
    var arryCityData = [CityDataModel]()
    var arryTehsilData = [TehsilDataModel]()
    var arryVillageData = [VillageDataModel]()
    
    var myPickerView : UIPickerView!
    var toolBar : UIToolbar!
    
    var selectedStateIndex = 0
    var currentStateIndex = 0
    
    var selectedCityIndex = 0
    var currentCityIndex = 0
    
    var selectedTehsilIndex = 0
    var currentTehsilIndex = 0
    
    var selectedVillageIndex = 0
    var currentVillageIndex = 0
    
    var strStateId = ""
    var strCityId = ""
    var strTehsilId = ""
    var strVillageId = ""
    
    var passedPrice = ""
    var passedWalletAmount = ""
    var passedWalletPoint = ""
    var passedGSTAmount = ""
    var passedTotalAmount = ""
    var isFromCart = false
    
    //MARK:- UIView Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setupInitialView()
    }
    
    
    //MARK:- Class Methods and Functions
    func setupInitialView(){
        
        lblTitle.text = StringFile.strOrder[kAppDelegate.userCurrentLanguage]
        tfPincode.placeholder = StringFile.strPinCode[kAppDelegate.userCurrentLanguage]
        tfFullAddress.placeholder = StringFile.strFullAddress[kAppDelegate.userCurrentLanguage]
        
        lblPaymentMethodTitle.text = StringFile.strPaymentMethod[kAppDelegate.userCurrentLanguage]
        lblCODTitle.text = StringFile.strCOD[kAppDelegate.userCurrentLanguage]
        lblNetbankingTitle.text = StringFile.strNetBanking[kAppDelegate.userCurrentLanguage]
        
        tfTotalPayment.placeholder = StringFile.strTotalPayment[kAppDelegate.userCurrentLanguage]
        
        btnPayment.setTitle(StringFile.strPaymentCap[kAppDelegate.userCurrentLanguage], for: .normal)
        
        lblOrderSummaryTitle.text = StringFile.strOrderSummary[kAppDelegate.userCurrentLanguage]
        
        lblShippingAddTitle.text = StringFile.strShippingAddress[kAppDelegate.userCurrentLanguage]
        
        lblSelectedPaymentMethodTitle.text = StringFile.strSelectedPaymentMethod[kAppDelegate.userCurrentLanguage]
        
        lblPriceTitle.text = StringFile.strPrice[kAppDelegate.userCurrentLanguage]
        lblWalletAmountTitle.text = StringFile.strWalletAmount[kAppDelegate.userCurrentLanguage]
        lblTotalPriceTitle.text = StringFile.strTotalPrice[kAppDelegate.userCurrentLanguage]
        btnComplete.setTitle(StringFile.strComplete[kAppDelegate.userCurrentLanguage], for: .normal)
        
        viewClientAddress.dropShadow(color: UIColor.darkGray, opacity: 0.5, shadowRadius: 10.0, cornerRadius: 10.0)
                
        viewOrderSummary.dropShadow(color: UIColor.darkGray, opacity: 0.5, shadowRadius: 10.0, cornerRadius: 10.0)
        
        viewItemDetail.dropShadow(color: UIColor.darkGray, opacity: 0.5, shadowRadius: 10.0, cornerRadius: 10.0)
        
        btnComplete.layer.cornerRadius = 10
        btnComplete.layer.masksToBounds = true
        
        btnPayment.layer.cornerRadius = 10
        btnPayment.layer.masksToBounds = true
        
        tfName.titleFormatter = { $0 }
        tfName.titleColor = kAppThemeGreenDarkColor
        tfName.titleFont = kCustom_Font(fontName: kPoppin_SemiBold, fontSize: 12.0)
        tfName.placeholderFont = kCustom_Font(fontName: kPoppin_Regular, fontSize: 16.0)
        tfName.placeholder = StringFile.strName[kAppDelegate.userCurrentLanguage]
    
        
        tfNumber.titleFormatter = { $0 }
        tfNumber.titleColor = kAppThemeGreenDarkColor
        tfNumber.titleFont = kCustom_Font(fontName: kPoppin_SemiBold, fontSize: 12.0)
        tfNumber.placeholderFont = kCustom_Font(fontName: kPoppin_Regular, fontSize: 16.0)
        tfNumber.placeholder = StringFile.strMobileNumber[kAppDelegate.userCurrentLanguage]

        
        tfEmail.titleFormatter = { $0 }
        tfEmail.titleColor = kAppThemeGreenDarkColor
        tfEmail.titleFont = kCustom_Font(fontName: kPoppin_SemiBold, fontSize: 12.0)
        tfEmail.placeholderFont = kCustom_Font(fontName: kPoppin_Regular, fontSize: 16.0)
        tfEmail.placeholder = StringFile.strEmail[kAppDelegate.userCurrentLanguage]

        tfPincode.titleFormatter = { $0 }
        tfPincode.titleColor = kAppThemeGreenDarkColor
        tfPincode.titleFont = kCustom_Font(fontName: kPoppin_SemiBold, fontSize: 12.0)
        tfPincode.placeholderFont = kCustom_Font(fontName: kPoppin_Regular, fontSize: 16.0)
        
        tfFullAddress.titleFormatter = { $0 }
        tfFullAddress.titleColor = kAppThemeGreenDarkColor
        tfFullAddress.titleFont = kCustom_Font(fontName: kPoppin_SemiBold, fontSize: 12.0)
        tfFullAddress.placeholderFont = kCustom_Font(fontName: kPoppin_Regular, fontSize: 16.0)
        
        tfTotalPayment.titleFormatter = { $0 }
        tfTotalPayment.titleColor = kAppThemeGreenDarkColor
        tfTotalPayment.titleFont = kCustom_Font(fontName: kPoppin_SemiBold, fontSize: 12.0)
        tfTotalPayment.placeholderFont = kCustom_Font(fontName: kPoppin_Regular, fontSize: 16.0)
        tfTotalPayment.isUserInteractionEnabled = false
        
        tfName.text = kAppDelegate.userLoginData.name
        tfNumber.text = kAppDelegate.userLoginData.mobile
        tfState.text = kAppDelegate.userLoginData.state
        tfCity.text = kAppDelegate.userLoginData.city
        tfTehsil.text = kAppDelegate.userLoginData.tehsil
        tfVillage.text = kAppDelegate.userLoginData.village
        
        strStateId = kAppDelegate.userLoginData.state_id
        strCityId = kAppDelegate.userLoginData.city_id
        strTehsilId = kAppDelegate.userLoginData.tehsil_id
        strVillageId = kAppDelegate.userLoginData.village_id
        
        let doubleGstAmount = Double(passedGSTAmount) ?? 0
        let doubleTotalAmount = Double(passedTotalAmount)!
        
        let amount = (doubleTotalAmount) + (doubleGstAmount)
        
        let myWalletAmount = Double(passedWalletAmount)
        
        let TotalAmount = (amount) - (myWalletAmount!)

        let strAmount = String(format: "%.2f", amount)
        let strTotalAmount = String(format: "%.2f", TotalAmount)

        lblShippingAddress.text = ""
        tfTotalPayment.text = "₹\(strTotalAmount)"
        lblPrice.text = "₹\(strAmount)"
        lblWalletAmount.text = "₹\(passedWalletAmount)"
        lblTotalPrice.text = "₹\(strTotalAmount)"
        
        lblCondition.text = StringFile.msgGSTIncluded[kAppDelegate.userCurrentLanguage]

        self.btnCodClick(self)
        imgNetBanking.isHidden = false
        btnNetBanking.isHidden = false
        lblNetbankingTitle.isHidden = false
        self.btnNetBankingClick(self)
        tfName.becomeFirstResponder()
        
        self.getStateApi()

    }
    

    func showDropDown(withType type: Int){
        //type:
        // 1 = State, 2 = City, 3 = Tehsil, 4 - Village
        
        for view in self.view.subviews{
            if view == self.myPickerView{
                toolBar.removeFromSuperview()
                myPickerView.removeFromSuperview()
            }
        }
        
        self.myPickerView = UIPickerView(frame:CGRect(x: 0, y: UIScreen.main.bounds.size.height - 216, width: self.view.frame.size.width, height: 216))
        self.myPickerView.delegate = self
        self.myPickerView.dataSource = self
        self.myPickerView.backgroundColor = UIColor.white
        self.myPickerView.tag = type
        self.view.addSubview(self.myPickerView)
        toolBar = UIToolbar(frame:CGRect(x: 0, y: UIScreen.main.bounds.size.height - 216, width: self.view.frame.size.width, height: 50))
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = kAppThemeGreenDarkColor
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: StringFile.strDone[kAppDelegate.userCurrentLanguage], style: .plain, target: self, action: #selector(self.doneClick(sender:)))
        doneButton.tag = type
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: StringFile.strCancel[kAppDelegate.userCurrentLanguage], style: .plain, target: self, action: #selector(self.cancelClick(sender:)))
        cancelButton.tag = type
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        self.view.addSubview(toolBar)
        
        if type == 1{
            self.myPickerView.selectRow(selectedStateIndex, inComponent: 0, animated: true)
        }else if type == 2{
            self.myPickerView.selectRow(selectedCityIndex, inComponent: 0, animated: true)
        }else if type == 3{
            self.myPickerView.selectRow(selectedTehsilIndex, inComponent: 0, animated: true)
        }else if type == 4{
            self.myPickerView.selectRow(selectedVillageIndex, inComponent: 0, animated: true)
        }
        

    }
    
    @objc func doneClick(sender: UIButton!) {
        
        self.view.endEditing(true)
        if sender.tag == 1{
            selectedStateIndex = currentStateIndex
            tfState.text = self.arryStateData[selectedStateIndex].DTName
            self.strStateId = self.arryStateData[selectedStateIndex].STCode
            self.tfCity.text = ""
            self.tfTehsil.text = ""
            self.tfVillage.text = ""
            self.arryCityData.removeAll()
            self.arryTehsilData.removeAll()
            self.arryVillageData.removeAll()
            self.getCityApi(withState: self.arryStateData[selectedStateIndex].STCode)
        }else if sender.tag == 2{
            selectedCityIndex = currentCityIndex
            tfCity.text = self.arryCityData[selectedCityIndex].DTName
            self.strCityId = self.arryCityData[selectedCityIndex].DTCode
            self.tfTehsil.text = ""
            self.tfVillage.text = ""
            self.arryTehsilData.removeAll()
            self.arryVillageData.removeAll()
            self.getTehsilApi(withCity: self.arryCityData[selectedCityIndex].DTCode)

        }else if sender.tag == 3{
            selectedTehsilIndex = currentTehsilIndex
            tfTehsil.text = self.arryTehsilData[selectedTehsilIndex].SDTName
            self.strTehsilId = self.arryTehsilData[selectedTehsilIndex].SDTCode
            self.tfVillage.text = ""
            self.arryVillageData.removeAll()
            self.getVillageApi(withTehsil: self.arryTehsilData[selectedTehsilIndex].SDTCode, isFirstTime: false)
        }else if sender.tag == 4{
            selectedVillageIndex = currentVillageIndex
            tfVillage.text = self.arryVillageData[selectedVillageIndex].Name
            self.strVillageId = self.arryVillageData[selectedVillageIndex].TVCode
        }
        
        toolBar.removeFromSuperview()
        myPickerView.removeFromSuperview()
    }
    
    @objc func cancelClick(sender: UIButton!) {
        
        if sender.tag == 1{
            if selectedStateIndex != currentStateIndex{
                currentStateIndex = selectedStateIndex
                tfState.text = self.arryStateData[currentStateIndex].DTName
                strStateId = self.arryStateData[currentStateIndex].STCode
            }
        }else if sender.tag == 2{
            if selectedCityIndex != currentCityIndex{
                currentCityIndex = selectedCityIndex
                tfCity.text = self.arryCityData[currentCityIndex].DTName
                strCityId = self.arryCityData[currentCityIndex].DTCode
            }
        }else if sender.tag == 3{
            if selectedTehsilIndex != currentTehsilIndex{
                currentTehsilIndex = selectedTehsilIndex
                tfTehsil.text = self.arryTehsilData[currentTehsilIndex].SDTName
                strTehsilId = self.arryTehsilData[currentTehsilIndex].SDTCode
            }
        }else if sender.tag == 4{
            if selectedVillageIndex != currentVillageIndex{
                currentVillageIndex = selectedVillageIndex
                tfVillage.text = self.arryVillageData[currentVillageIndex].Name
                strVillageId = self.arryVillageData[currentVillageIndex].TVCode
            }
        }
        
        toolBar.removeFromSuperview()
        myPickerView.removeFromSuperview()
    }
    
    func moveToRootViewCtr(){
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: - IBActions
    @IBAction func btnBackClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnStateChangeClick(_ sender: Any) {
        if arryStateData.count > 0{
            self.showDropDown(withType: 1)
        }
    }

    @IBAction func btnCityChangeClick(_ sender: Any) {
        if arryCityData.count > 0{
            self.showDropDown(withType: 2)
        }
    }
    
    @IBAction func btnTehsilChangeClick(_ sender: Any) {
        if arryTehsilData.count > 0{
            self.showDropDown(withType: 3)
        }
    }
    
    @IBAction func btnVillageChangeClick(_ sender: Any) {
        if arryVillageData.count > 0{
            self.showDropDown(withType: 4)
        }
    }
    
    @IBAction func btnCodClick(_ sender: Any) {
        payment = "cod"
        imgCOD.image = UIImage.init(named: "check")
        imgNetBanking.image = UIImage.init(named: "uncheck")
        
        lblSelectedPaymentMethod.text = StringFile.strCOD[kAppDelegate.userCurrentLanguage]
        
        self.setViewHideShowWithAnimarion(view: viewBankDetails, hidden: true)
        self.setViewHideShowWithAnimarion(view: btnComplete, hidden: false)

        topOrderSummaryConstraint.constant = 24
        hightMainViewConstraint.constant = 1100
    }
    
    @IBAction func btnNetBankingClick(_ sender: Any) {
        payment = "netbanking"
        imgNetBanking.image = UIImage.init(named: "check")
        imgCOD.image = UIImage.init(named: "uncheck")
        
        lblSelectedPaymentMethod.text = StringFile.strNetBanking[kAppDelegate.userCurrentLanguage]
        
        self.setViewHideShowWithAnimarion(view: viewBankDetails, hidden: false)
        self.setViewHideShowWithAnimarion(view: btnComplete, hidden: true)

        topOrderSummaryConstraint.constant = 152
        hightMainViewConstraint.constant = 1100
        
    }
    
    @IBAction func btnCompleteClick(_ sender: Any) {
        self.checkValidationCallApi(withType: false)
    }
    
    @IBAction func btnPaymentClick(_ sender: Any) {
        self.checkValidationCallApi(withType: true)
    }
    
    func checkValidationCallApi(withType isFromPayment:Bool){

        self.view.endEditing(true)
         if (tfName.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            self.ShowAlertDisplay(titleObj: StringFile.strAppName[kAppDelegate.userCurrentLanguage], messageObj: StringFile.msgPleaseEnterName[kAppDelegate.userCurrentLanguage], viewcontrolelr: self)
         }else if (tfNumber.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            self.ShowAlertDisplay(titleObj: StringFile.strAppName[kAppDelegate.userCurrentLanguage], messageObj: StringFile.msgPleaseEnterPhone[kAppDelegate.userCurrentLanguage], viewcontrolelr: self)
         }else if tfNumber.text!.count < PHONE_MAX_CHAR_SIZE{
             self.ShowAlertDisplay(titleObj: StringFile.strAppName[kAppDelegate.userCurrentLanguage], messageObj: StringFile.msgEnterPhoneNumberValid[kAppDelegate.userCurrentLanguage], viewcontrolelr: self)
         }else if (tfState.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            self.ShowAlertDisplay(titleObj: StringFile.strAppName[kAppDelegate.userCurrentLanguage], messageObj: StringFile.msgPleaseSelectState[kAppDelegate.userCurrentLanguage], viewcontrolelr: self)
         }else if (tfCity.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            self.ShowAlertDisplay(titleObj: StringFile.strAppName[kAppDelegate.userCurrentLanguage], messageObj: StringFile.msgPleaseSelectCity[kAppDelegate.userCurrentLanguage], viewcontrolelr: self)
         }else if (tfTehsil.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            self.ShowAlertDisplay(titleObj: StringFile.strAppName[kAppDelegate.userCurrentLanguage], messageObj: StringFile.msgPleaseSelectTehsil[kAppDelegate.userCurrentLanguage], viewcontrolelr: self)
         }else if (tfVillage.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            self.ShowAlertDisplay(titleObj: StringFile.strAppName[kAppDelegate.userCurrentLanguage], messageObj: StringFile.msgPleaseSelectVillage[kAppDelegate.userCurrentLanguage], viewcontrolelr: self)
         }else if (tfPincode.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            self.ShowAlertDisplay(titleObj: StringFile.strAppName[kAppDelegate.userCurrentLanguage], messageObj: StringFile.msgPleaseEnterPinCode[kAppDelegate.userCurrentLanguage], viewcontrolelr: self)
         }else if (tfFullAddress.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            self.ShowAlertDisplay(titleObj: StringFile.strAppName[kAppDelegate.userCurrentLanguage], messageObj: StringFile.msgPleaseEnterFullAddress[kAppDelegate.userCurrentLanguage], viewcontrolelr: self)
         }
         else
         {
            if isFromPayment{
                // PayU Money integrate
            }else{
                self.addOrderApiCall()
            }
         }
    }
    
    //MARK:- Web Services Call
    
    func addOrderApiCall(){
        
       
            GlobalMethods.presentLoaderViewCtr(withViewController: self)
        
        let reqParam = [ParameterKeys.Alias : tfName.text!, ParameterKeys.Mobile: tfNumber.text!, ParameterKeys.Zip : tfPincode.text!, ParameterKeys.City : strCityId, ParameterKeys.Street : tfFullAddress.text!, ParameterKeys.State: strStateId, ParameterKeys.Tid: strTehsilId, ParameterKeys.Vid: strVillageId, ParameterKeys.FullAddress: tfFullAddress.text!, ParameterKeys.Payment: payment, ParameterKeys.Customer_id: kAppDelegate.userLoginData.id, ParameterKeys.Mail: tfEmail.text!, ParameterKeys.Wallet_point: passedWalletPoint, ParameterKeys.Total_gst: passedGSTAmount, ParameterKeys.User_Id : kAppDelegate.userLoginData.id] as [String : Any]

        WebServiceCall.callMethodWithURL(route: APIRouter.AddOrder(reqParam)) { (_ responseCode: Int?, _ responseValue : [String : AnyObject]?, _ error : Error?) in
            
                GlobalMethods.dismissLoaderView()
            
            
            if (responseValue != nil) {
                
                let responseModel = CheckOutModel(dictionary: responseValue! as NSDictionary)
                
                if responseModel.status == 200{
                        kAppDelegate.cartCount = 0
//                        GlobalMethods.showAlert(alertTitle: StringFile.strAppName[kAppDelegate.userCurrentLanguage], alertMessage: StringFile.msgOrderSuccessfully[kAppDelegate.userCurrentLanguage], viewCtr: self) {
//                            self.navigationController?.popToRootViewController(animated: true)
//                        }
                        
//                        GlobalMethods.showAlert(alertTitle: StringFile.strAppName[kAppDelegate.userCurrentLanguage], alertMessage: StringFile.msgOrderSuccessfully[kAppDelegate.userCurrentLanguage], okActionTitle: StringFile.strOK[kAppDelegate.userCurrentLanguage], viewCtr: self, completion: {
//
//                            self.moveToRootViewCtr()
//                        })

                    
                    let alertWindow = UIWindow(frame: UIScreen.main.bounds)
                    alertWindow.rootViewController = UIViewController()
                    alertWindow.windowLevel = UIWindow.Level.alert + 1

                    let alert = UIAlertController(title: StringFile.strAppName[kAppDelegate.userCurrentLanguage], message: StringFile.msgOrderSuccessfully[kAppDelegate.userCurrentLanguage], preferredStyle: .alert)

                    let okAction = UIAlertAction(title: StringFile.strOK[kAppDelegate.userCurrentLanguage], style: .cancel) { (action) -> Void in
                        self.navigationController?.popToRootViewController(animated: true)
                    }
                    alert.addAction(okAction)

                    
                    alertWindow.makeKeyAndVisible()
                    alertWindow.rootViewController?.present(alert, animated: true, completion: nil)
                    
                }
                else {
                    GlobalMethods.showAlert(alertTitle: StringFile.strError[kAppDelegate.userCurrentLanguage], alertMessage: StringFile.msgNeedProperDetails[kAppDelegate.userCurrentLanguage], viewCtr: self, completion: nil)
                }
            }
            else {
                GlobalMethods.showErrorFromAPI(responseCode, responseValue, error, viewCtr: self)
            }
        }
    }
    
    func getStateApi(){
        
        GlobalMethods.presentLoaderViewCtr(withViewController: self)
        
        WebServiceCall.cancelAllAPIRequest()

        WebServiceCall.callMethodWithURL(route: APIRouter.GetState(custId:kAppDelegate.userLoginData.id,Luang:kAppDelegate.userCurrentLanguage)) { (_ responseCode: Int?, _ responseValue : [String : AnyObject]?, _ error : Error?) in
            
            GlobalMethods.dismissLoaderView()
            
             if (responseValue != nil) {
                
                let responseModel = StateModel(dictionary: responseValue! as NSDictionary)

                if responseModel.status == 200{
                    self.arryStateData = responseModel.data
                    if self.strStateId.count > 0 && self.arryCityData.count == 0{
                        self.setUpCurrentStateIndex()
                        self.getCityApi(withState: self.strStateId,isNeedToLoader: false)
                    }
                }
                else {
                   GlobalMethods.showAlert(alertTitle: StringFile.strError[kAppDelegate.userCurrentLanguage], alertMessage: StringFile.msgNeedProperDetails[kAppDelegate.userCurrentLanguage], viewCtr: self, completion: nil)
               }
           }
           else {
               GlobalMethods.showErrorFromAPI(responseCode, responseValue, error, viewCtr: self)
           }
        }
    }
    
    func setUpCurrentStateIndex(){
        for (index, objState) in arryStateData.enumerated() {
            if objState.STCode == self.strStateId{
                selectedStateIndex = index
                currentStateIndex = index
            }
        }
    }
        
    func getCityApi(withState stateId: String, isNeedToLoader: Bool = true){
        
        if isNeedToLoader{
            GlobalMethods.presentLoaderViewCtr(withViewController: self)
        }
        
        WebServiceCall.cancelAPIRequest(arryLastPathComponents: [APIRouter.GetCities([:]).path])
        let reqParam = [ParameterKeys.State_id : stateId,ParameterKeys.Customer_id: kAppDelegate.userLoginData.id,ParameterKeys.Language:kAppDelegate.userCurrentLanguage] as [String : Any]

        WebServiceCall.callMethodWithURL(route: APIRouter.GetCities(reqParam)) { (_ responseCode: Int?, _ responseValue : [String : AnyObject]?, _ error : Error?) in
            if isNeedToLoader{
                GlobalMethods.dismissLoaderView()
            }
            
             if (responseValue != nil) {
                
                let responseModel = CityModel(dictionary: responseValue! as NSDictionary)

                if responseModel.status == 200{
                    self.arryCityData = responseModel.data
                    if self.strCityId.count > 0 && self.arryTehsilData.count == 0{
                        self.getTehsilApi(withCity: self.strCityId, isNeedToLoader: false)
                        self.setUpCurrentCityIndex()
                    }
                }
                else {
                   GlobalMethods.showAlert(alertTitle: StringFile.strError[kAppDelegate.userCurrentLanguage], alertMessage: StringFile.msgNeedProperDetails[kAppDelegate.userCurrentLanguage], viewCtr: self, completion: nil)
               }
           }
           else {
               GlobalMethods.showErrorFromAPI(responseCode, responseValue, error, viewCtr: self)
           }
        }
    }
    
    func setUpCurrentCityIndex(){
        for (index, objCity) in arryCityData.enumerated() {
            if objCity.DTCode == self.strCityId{
                selectedCityIndex = index
                currentCityIndex = index
            }
        }
    }
        
    func getTehsilApi(withCity cityId: String, isNeedToLoader: Bool = true){
        
        if isNeedToLoader{
            GlobalMethods.presentLoaderViewCtr(withViewController: self)
        }
        
        WebServiceCall.callMethodWithURL(route: APIRouter.GetTehsil(withCityId: cityId)) { (_ responseCode: Int?, _ responseValue : [String : AnyObject]?, _ error : Error?) in
            if isNeedToLoader{
                GlobalMethods.dismissLoaderView()
            }
            
             if (responseValue != nil) {
                
                let responseModel = TehsilModel(dictionary: responseValue! as NSDictionary)

                if responseModel.status == 200{
                    self.arryTehsilData = responseModel.data
                    if self.strTehsilId.count > 0 && self.arryVillageData.count == 0{
                        self.getVillageApi(withTehsil: self.strTehsilId, isFirstTime: true, isNeedToLoader: false)
                        self.setUpCurrentTehsilIndex()
                    }
                }
                    
                else {
                   GlobalMethods.showAlert(alertTitle: StringFile.strError[kAppDelegate.userCurrentLanguage], alertMessage: StringFile.msgNeedProperDetails[kAppDelegate.userCurrentLanguage], viewCtr: self, completion: nil)
               }
           }
           else {
               GlobalMethods.showErrorFromAPI(responseCode, responseValue, error, viewCtr: self)
           }
        }
    }
    
    func setUpCurrentTehsilIndex(){
        
        for (index, objTehsil) in arryTehsilData.enumerated() {
            if objTehsil.SDTName == kAppDelegate.userLoginData.tehsil{
                selectedTehsilIndex = index
                currentTehsilIndex = index
            }
        }
    }
    
    func getVillageApi(withTehsil tehsilId: String, isFirstTime: Bool, isNeedToLoader: Bool = true){
        
        if isNeedToLoader{
            GlobalMethods.presentLoaderViewCtr(withViewController: self)
        }
        
        WebServiceCall.callMethodWithURL(route: APIRouter.GetVillage(withTehsilId: tehsilId)) { (_ responseCode: Int?, _ responseValue : [String : AnyObject]?, _ error : Error?) in
            
            
            GlobalMethods.dismissLoaderView()
            
            
            
             if (responseValue != nil) {
                
                let responseModel = VillageModel(dictionary: responseValue! as NSDictionary)

                if responseModel.status == 200{
                    self.arryVillageData = responseModel.data
                    if isFirstTime{
                        self.setUpCurrentVillageIndex()
                    }
                }
                else {
                   GlobalMethods.showAlert(alertTitle: StringFile.strError[kAppDelegate.userCurrentLanguage], alertMessage: StringFile.msgNeedProperDetails[kAppDelegate.userCurrentLanguage], viewCtr: self, completion: nil)
               }
           }
           else {
               GlobalMethods.showErrorFromAPI(responseCode, responseValue, error, viewCtr: self)
           }
        }
    }
    
    func setUpCurrentVillageIndex(){
        for (index, objVillage) in arryVillageData.enumerated() {
            if objVillage.TVCode == self.strVillageId{
                selectedVillageIndex = index
                currentVillageIndex = index
            }
        }
    }

}

extension CheckoutViewCtr:UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        if textField == tfFullAddress{
            self.view.endEditing(true)
        }
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == self.tfState {
            return false
        }else if textField == self.tfCity{
            return false
        } else if textField == self.tfTehsil{
            return false
        } else if textField == self.tfVillage{
            return false
        }
        return true
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if textField == tfFullAddress{
            self.lblShippingAddress.text = tfFullAddress.text
        }
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool
    {
        
        let  char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        
        if (isBackSpace == -92) {
            return true
        }
        
        if textField == tfPincode{
            if textField.text!.count == 6{
                return false
            }
        }
        
        if textField == tfNumber || textField == tfPincode{
            return string.isNumber
        }
        
        
        return true
    }
}

extension CheckoutViewCtr : UIPickerViewDelegate, UIPickerViewDataSource{
    //MARK:- PickerView Delegate & DataSource
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        var returnCount = 0
        if pickerView.tag == 1{
            returnCount = self.arryStateData.count
        } else if pickerView.tag == 2{
            returnCount = self.arryCityData.count
        } else if pickerView.tag == 3{
            returnCount = self.arryTehsilData.count
        } else if pickerView.tag == 4{
            returnCount = self.arryVillageData.count
        }
        return returnCount
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        var passString = ""
        if pickerView.tag == 1{
            passString = self.arryStateData[row].DTName
        } else if pickerView.tag == 2{
            passString = self.arryCityData[row].DTName
        } else if pickerView.tag == 3{
            passString = self.arryTehsilData[row].SDTName
        } else if pickerView.tag == 4{
            passString = self.arryVillageData[row].Name
        }
        return passString
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView.tag == 1{
            let passString = self.arryStateData[row].DTName
            tfState.text = passString
            currentStateIndex = row
            strStateId = self.arryStateData[currentStateIndex].STCode
        } else if pickerView.tag == 2{
            let passString = self.arryCityData[row].DTName
            tfCity.text = passString
            currentCityIndex = row
            strCityId = self.arryCityData[currentCityIndex].DTCode
        } else if pickerView.tag == 3{
            let passString = self.arryTehsilData[row].SDTName
            tfTehsil.text = passString
            currentTehsilIndex = row
            strTehsilId = self.arryTehsilData[currentTehsilIndex].SDTCode
        } else if pickerView.tag == 4{
            let passString = self.arryVillageData[row].Name
            tfVillage.text = passString
            currentVillageIndex = row
            strVillageId = self.arryVillageData[currentVillageIndex].TVCode
        }
    }

}
