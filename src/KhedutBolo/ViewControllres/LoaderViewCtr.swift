//
//  LoaderViewCtr.swift
//  KhedutBolo
//
//  Created by Trivedi Sagar on 3/27/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit
import SwiftGifOrigin


class LoaderViewCtr: UIViewController {
    
    var objTimer: Timer?

    
    // MARK:- Outlets
    
    @IBOutlet weak var imgGif: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        /*~~~~~ notification  ~~~~~*/
        NotificationCenter.default.addObserver(self, selector: #selector(self.dismissLoaderView), name: NSNotification.Name(rawValue: kDismissLoaderView), object: nil)
        
        imgGif.loadGif(name: "load")
        self.setTimer()
    }
    
    
    override func viewDidDisappear(_ animated: Bool) {
        objTimer?.invalidate()
    }
    
    func setTimer(){
//        objTimer?.invalidate()
        guard objTimer == nil else { return }
        objTimer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(dismissTimerLoadView), userInfo: nil, repeats: true)
    }
    
    @objc func dismissTimerLoadView(){
        DispatchQueue.main.async {
            print("dismissTimerLoadView")
            self.objTimer?.invalidate()
            self.objTimer = nil
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @objc func dismissLoaderView(){
        print("dismissLoaderView")
        objTimer?.invalidate()
        objTimer = nil
        self.dismiss(animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
