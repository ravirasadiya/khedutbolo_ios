//
//  OffersViewCtr.swift
//  KhedutBolo
//
//  Created by Trivedi Sagar on 10/25/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit
import EmptyDataSet_Swift

class OffersViewCtr: UIViewController {

    // MARK: - define IBOutlet
    @IBOutlet weak var lblTopTitle: UILabel!
    @IBOutlet weak var cvOfferProduct: UICollectionView!

    var passIdOfferId = 0
    
    var arrProductOffer = [ProductDetailModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.setupInitialView()
    }
    
    //MARK:- Class Methods and Functions
    func setupInitialView(){
        lblTopTitle.text = StringFile.strOffers[kAppDelegate.userCurrentLanguage]
        cvOfferProduct.register(UINib(nibName: prodCatCCell, bundle: nil), forCellWithReuseIdentifier: prodCatCCell)
        self.getOfferDetailApi()

    }
    
    //MARK:- IBActions
    @IBAction func clickOnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    
    //MARK:- Web Services Call
    
    func getOfferDetailApi(){
      
        let reqParam = [ParameterKeys.Offer_id : passIdOfferId, ParameterKeys.User_Id : kAppDelegate.userLoginData.id] as [String : Any]

        GlobalMethods.presentLoaderViewCtr(withViewController: self)
        
        WebServiceCall.callMethodWithURL(route: APIRouter.OfferDetails(reqParam)) { (_ responseCode: Int?, _ responseValue : [String : AnyObject]?, _ error : Error?) in

            GlobalMethods.dismissLoaderView()
            
            if (responseValue != nil) {
                
                let responseModel = SimilarProductResponseModel(dictionary: responseValue! as NSDictionary)
                
                if responseModel.status == 200{
                    
                    self.arrProductOffer.removeAll()
                    self.arrProductOffer = responseModel.data
                    self.apiResponseSet()

                }
                else {
                    GlobalMethods.showAlert(alertTitle: StringFile.strError[kAppDelegate.userCurrentLanguage], alertMessage: StringFile.msgNeedProperDetails[kAppDelegate.userCurrentLanguage], viewCtr: self, completion: nil)
                }
            }
            else {
                GlobalMethods.showErrorFromAPI(responseCode, responseValue, error, viewCtr: self)
            }
        }
    }
    
    func apiResponseSet(){
        if arrProductOffer.count != 0{
            self.cvOfferProduct.reloadData()
        }else{
            cvOfferProduct.emptyDataSetSource = self
            cvOfferProduct.emptyDataSetDelegate = self
            self.cvOfferProduct.reloadData()
        }
    }
}

// MARK: - UICollectionView Delegate, DataSource Methods
extension OffersViewCtr : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    
        return arrProductOffer.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
      
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: prodCatCCell, for: indexPath) as! ProdCatCCell
        let objModel = self.arrProductOffer[indexPath.row]
        cell.lblName.text = objModel.name
        cell.lblPrice.text = "₹\(objModel.price)"
        cell.lblSalePrice.text = "₹\(objModel.sale_price)"
        cell.lblPerSet.text = StringFile.strPerSet[kAppDelegate.userCurrentLanguage]
        cell.lblDealerName.text = ""
        cell.imgProduct.sd_setImage(with: objModel.coverImgUrl, placeholderImage: kProductImagePlaceHolder)
        return cell
       
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let cellWidth = (SystemSize.width-65)/2 //50
        let cellSize  = CGSize(width: cellWidth, height: cellWidth + 65) //52
        return cellSize
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       
        let objModel = self.arrProductOffer[indexPath.row]
            
        GlobalMethods.pushToProductCtr(withViewController: self, animated: true, productId: "\(objModel.id)")
            
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
    }
}

// MARK: - Empty Data Set Methods
extension OffersViewCtr : EmptyDataSetSource, EmptyDataSetDelegate{
    
    func emptyDataSetShouldDisplay(_ scrollView: UIScrollView) -> Bool {
        return true
    }
    
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        let str = StringFile.strNoDataFound[kAppDelegate.userCurrentLanguage]
        let font = kCustom_Font(fontName: kPoppin_SemiBold, fontSize: 18.0)
        let attributes = [NSAttributedString.Key.font: font]
        let attributedStr = NSAttributedString(string: str, attributes: attributes)
        return attributedStr
    }
    
    func image(forEmptyDataSet scrollView: UIScrollView) -> UIImage? {
        return UIImage.init(named: "nodata")
    }
    
    func spaceHeight(forEmptyDataSet scrollView: UIScrollView) -> CGFloat {
        return 20.0
    }
}
