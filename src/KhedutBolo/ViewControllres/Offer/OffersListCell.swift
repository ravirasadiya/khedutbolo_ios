//
//  OffersListCell.swift
//  KhedutBolo
//
//  Created by Trivedi Sagar on 10/25/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit

class OffersListCell: UITableViewCell {

    // MARK: - define IBOutlet start
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgOffers: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.imgOffers.layer.cornerRadius = 5
        self.imgOffers.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
