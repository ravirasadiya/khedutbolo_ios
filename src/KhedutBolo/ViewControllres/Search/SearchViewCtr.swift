//
//  SearchViewCtr.swift
//  KhedutBolo
//
//  Created by Trivedi Sagar on 3/5/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit
import EmptyDataSet_Swift

class SearchViewCtr: UIViewController{

    // MARK: - define IBOutlet
    @IBOutlet weak var cvProduct: UICollectionView!
    @IBOutlet weak var objSearchBar: UISearchBar!
    
    @IBOutlet weak var btnClose: UIButton!
    
    var arrProductOffer = [ProductDetailModel]()
    var isSearch : Bool = false
    var  textFieldInsideUISearchBar:UITextField?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.setupInitialView()
    }
    
    //MARK:- Class Methods and Functions
    func setupInitialView(){
        textFieldInsideUISearchBar = objSearchBar.value(forKey: "searchField") as? UITextField
        textFieldInsideUISearchBar?.textColor = UIColor.white
        textFieldInsideUISearchBar!.leftViewMode = UITextField.ViewMode.never
        
        textFieldInsideUISearchBar!.backgroundColor = kAppThemeGreenDarkColor
        textFieldInsideUISearchBar!.attributedPlaceholder = NSAttributedString(string: textFieldInsideUISearchBar?.placeholder ?? StringFile.strSearch[kAppDelegate.userCurrentLanguage], attributes: [NSAttributedString.Key.foregroundColor : UIColor.white])
        textFieldInsideUISearchBar?.clearButtonMode = .never
        textFieldInsideUISearchBar?.font = kCustom_Font(fontName: kPoppin_Regular, fontSize: 16.0)
        
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).tintColor = .white

        
        objSearchBar.backgroundImage = UIImage()

        cvProduct.register(UINib(nibName: prodCatCCell, bundle: nil), forCellWithReuseIdentifier: prodCatCCell)
        objSearchBar.becomeFirstResponder()
    }
    
    //MARK:- IBActions
    @IBAction func clickOnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnCloseOnClick(_ sender: UIButton) {
        if textFieldInsideUISearchBar?.text?.isEmpty == true{
            self.navigationController?.popViewController(animated: true)
        }else{
            textFieldInsideUISearchBar?.text = ""
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
  
    //MARK:- Web Services Call
    
    func getSearchProduct(withSearchText strSearch:String) {
        
        WebServiceCall.cancelAllAPIRequest()

        WebServiceCall.callMethodWithURL(route: APIRouter.GetSearch(withValue: strSearch, custId: kAppDelegate.userLoginData.id)) { (_ responseCode: Int?, _ responseValue : [String : AnyObject]?, _ error : Error?) in
            
            self.arrProductOffer.removeAll()
            if (responseValue != nil) {
                
                let responseModel = SimilarProductResponseModel(dictionary: responseValue! as NSDictionary)
                if responseModel.status == 200{
                    self.arrProductOffer = responseModel.data
                }
                self.afterGetSearchDataSet()
            }
            else {
                self.afterGetSearchDataSet()
                GlobalMethods.showErrorFromAPI(responseCode, responseValue, error, viewCtr: self)
            }
        }
    }
    
    func afterGetSearchDataSet(){
        if arrProductOffer.count != 0{
            self.cvProduct.reloadData()
        }else{
            cvProduct.emptyDataSetSource = self
            cvProduct.emptyDataSetDelegate = self
            self.cvProduct.reloadData()
        }
    }

}

// MARK: - UICollectionView Delegate, DataSource Methods
extension SearchViewCtr : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    
        return arrProductOffer.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
      
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: prodCatCCell, for: indexPath) as! ProdCatCCell
        let objModel = self.arrProductOffer[indexPath.row]
        cell.lblName.text = objModel.name
        cell.lblPrice.text = "₹\(objModel.price)"
        cell.lblSalePrice.text = "₹\(objModel.sale_price)"
        cell.lblPerSet.text = StringFile.strPerSet[kAppDelegate.userCurrentLanguage]
        cell.lblDealerName.text = "\(StringFile.strBy[kAppDelegate.userCurrentLanguage]) : \(StringFile.strAppName[kAppDelegate.userCurrentLanguage])"
        cell.imgProduct.sd_setImage(with: objModel.coverImgUrl, placeholderImage: kProductImagePlaceHolder)
        return cell
       
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let cellWidth = (SystemSize.width-50)/2
        let cellSize  = CGSize(width: cellWidth, height: cellWidth + 72)
        return cellSize
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       
        let objModel = self.arrProductOffer[indexPath.row]
            
        GlobalMethods.pushToProductCtr(withViewController: self, animated: true, productId: "\(objModel.id)")
            
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
    }
}


extension SearchViewCtr : UISearchBarDelegate{
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        isSearch = true
    }

    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        isSearch = false
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        self.navigationController?.popViewController(animated: false)
        isSearch = false
    }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        isSearch = false
    }

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
       // if searchText.count > 0 {
            self.getSearchProduct(withSearchText: searchText)
        //}
    }
}
extension UIColor {
    convenience init(r: Int, g: Int, b: Int, a: Int = 255) {
        self.init(red: CGFloat(r) / 255.0, green: CGFloat(g) / 255.0, blue: CGFloat(b) / 255.0, alpha: CGFloat(a) / 255.0)
    }

    convenience init(netHex:Int) {
        self.init(r:(netHex >> 16) & 0xff, g:(netHex >> 8) & 0xff, b:netHex & 0xff)
    }
}

// MARK: - Empty Data Set Methods
extension SearchViewCtr : EmptyDataSetSource, EmptyDataSetDelegate{
    
    func emptyDataSetShouldDisplay(_ scrollView: UIScrollView) -> Bool {
        return true
    }
    
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        let str = StringFile.strNoDataFound[kAppDelegate.userCurrentLanguage]
        let font = kCustom_Font(fontName: kPoppin_SemiBold, fontSize: 18.0)
        let attributes = [NSAttributedString.Key.font: font]
        let attributedStr = NSAttributedString(string: str, attributes: attributes)
        return attributedStr
    }
    
    func image(forEmptyDataSet scrollView: UIScrollView) -> UIImage? {
        return UIImage.init(named: "nodata")
    }
    
    func spaceHeight(forEmptyDataSet scrollView: UIScrollView) -> CGFloat {
        return 20.0
    }
}
