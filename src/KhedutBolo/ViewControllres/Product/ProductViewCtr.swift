//
//  ProductViewCtr.swift
//  KhedutBolo
//
//  Created by Trivedi Sagar on 11/4/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit
class ProductViewCtr: UIViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnCart: UIButton!
    @IBOutlet weak var lblCartCount: UILabel!

    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var lblDealerName: UILabel!
    @IBOutlet weak var lblSellerName: UILabel!
    
    @IBOutlet weak var viewProductDetail: UIView!
    
    @IBOutlet weak var lblProductSizeTitle: UILabel!
    @IBOutlet weak var lblProductSize: UILabel!
    @IBOutlet weak var viewProdSizeDropDown: UIView!
    @IBOutlet weak var imgSizeDropDown: UIImageView!

    
    @IBOutlet weak var lblProductQtyTitle: UILabel!
    @IBOutlet weak var lblProductQty: UILabel!
    @IBOutlet weak var btnQtyMinus: UIButton!
    @IBOutlet weak var btnQtyPlus: UIButton!
    
    @IBOutlet weak var lblProductSellingTitle: UILabel!
    @IBOutlet weak var lblProductSelling: UILabel!
    
    @IBOutlet weak var lblProductTotalTitle: UILabel!
    @IBOutlet weak var lblProductTotal: UILabel!
    
    @IBOutlet weak var lblProductShippingTitle: UILabel!
    @IBOutlet weak var lblProductShipping: UILabel!
    
    @IBOutlet weak var viewBuyCart: UIView!
    
    @IBOutlet weak var btnBuy: UIButton!
    @IBOutlet weak var btnAddCart: UIButton!
    
    @IBOutlet weak var btnCallOrder: UIButton!
    
    @IBOutlet weak var viewSeparator: UIView!
    
    @IBOutlet weak var topLabelAboutProductConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblAboutProductTitle: UILabel!
    
    @IBOutlet weak var viewAboutProduct: UIView!
    @IBOutlet weak var lblTechNameTitle: UILabel!
    @IBOutlet weak var lblValueByAdminTitle: UILabel!
    
    @IBOutlet weak var tblProdTech: UITableView!
    @IBOutlet weak var hightViewProdTechConstraint: NSLayoutConstraint!

    @IBOutlet weak var hightSimilarProCVConstraint: NSLayoutConstraint!
    @IBOutlet weak var hightMainviewConstraint: NSLayoutConstraint!

    @IBOutlet weak var lblSimilarProdTitle: UILabel!
    @IBOutlet weak var cvProduct: UICollectionView!
    
    @IBOutlet weak var viewOffer: UIView!
    @IBOutlet weak var lblOfferInstruction: UILabel!
    @IBOutlet weak var lblOfferFreeCount: UILabel!
    @IBOutlet weak var lblOfferProdName: UILabel!
    @IBOutlet weak var lblOfferProdQty: UILabel!
    @IBOutlet weak var imgOfferProd: UIImageView!
    @IBOutlet weak var imgOfferFreeProduct: UIImageView!

    var productId = ""
    var passedAttrbId = ""
    var objProduct = ProductDetailModel()
    var arrySimilarProduct = [ProductDetailModel]()
    
    var myPickerView : UIPickerView!
    var toolBar : UIToolbar!
    var selectedProductUnitIndex = 0
    var currentProductUnitIndex = 0
    var currentProductUnit = productDetailUnitDataModel()
        
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        tblProdTech.registerCell(cellIDs: [productTechCell], isDynamicHeight: true, estimatedHeight: 28)
        cvProduct.register(UINib(nibName: similarProdCCell, bundle: nil), forCellWithReuseIdentifier: similarProdCCell)
        
        tblProdTech.rowHeight = UITableView.automaticDimension
        
        self.setupInitialView()
        
        lblCartCount.isHidden = true
        
        /*~~~~~ notification  ~~~~~*/
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateCartCount), name: NSNotification.Name(rawValue: kUpdateProductViewCartCountNC), object: nil)

    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.updateCartCount()
    }
    
    @objc func updateCartCount(){
        if kAppDelegate.cartCount > 0{
            lblCartCount.isHidden = false
            lblCartCount.text = "\(kAppDelegate.cartCount)"
        }else{
            lblCartCount.isHidden = true
        }
    }
    override func viewDidLayoutSubviews() {
        if objProduct.additional_info.count > 1{
            
            self.viewAboutProduct.isHidden = false //true
            hightViewProdTechConstraint.constant = tblProdTech.contentSize.height * 1.1 //* CGFloat((objProduct.additional_info.count) - 2) //0
            
            lblAboutProductTitle.isHidden = false
            
            
        }else{
                    let font = UIFont.systemFont(ofSize: 12)
            //        var width:CGFloat = 0
                    var numberOfLinesKey:CGFloat = 0
                    for infoValue in self.objProduct.additional_info {
                        let width = infoValue.value.sizeOfString(OfFont: font).width // size: {w: 98.912 h: 14.32}
                        if width < 125 {
                            numberOfLinesKey = numberOfLinesKey + 1
                        }
                        else{
                            let i = Int(width.rounded(.up) / 125) // 3
                            numberOfLinesKey = CGFloat(i) + numberOfLinesKey + 1
                        }
                    }
                    
                    var numberOfLinesValue:CGFloat = 0
                    for infoValue in self.objProduct.additional_info {
                        let width = infoValue.key.sizeOfString(OfFont: font).width // size: {w: 98.912 h: 14.32}
                        if width < 125 {
                            numberOfLinesValue = numberOfLinesValue + 1
                        }
                        else{
                            let i = Int(width.rounded(.up) / 125) // 3
                            numberOfLinesValue = CGFloat(i) + numberOfLinesValue
                        }
                    }


                    
                    hightViewProdTechConstraint.constant = CGFloat(((numberOfLinesKey > numberOfLinesValue ? numberOfLinesKey : numberOfLinesValue) * 28) + 16)

        }
        tblProdTech.reloadData()
    }
    //MARK:- Class Methods and Functions
    func setupInitialView(){
           
        viewProductDetail.dropShadow(color: UIColor.darkGray, opacity: 0.5, shadowRadius: 10.0, cornerRadius: 10.0)
        viewOffer.dropShadow(color: UIColor.darkGray, opacity: 0.5, shadowRadius: 10.0, cornerRadius: 10.0)

        viewBuyCart.layer.cornerRadius = 10
        viewBuyCart.layer.masksToBounds = true
        
        btnCallOrder.layer.cornerRadius = 10
        btnCallOrder.layer.masksToBounds = true
        
        btnBuy.backgroundColor = UIColor.black
        btnAddCart.backgroundColor = kAppThemeGreenDarkColor
        
        viewAboutProduct.layer.cornerRadius = 10.0
        viewAboutProduct.layer.borderColor = kAppThemeGreenDarkColor.cgColor
        viewAboutProduct.layer.borderWidth = 1.0
        viewAboutProduct.layer.masksToBounds = true
        
        lblCartCount.layer.cornerRadius = lblCartCount.bounds.width/2
        lblCartCount.layer.masksToBounds = true
        
        topLabelAboutProductConstraint.constant = 16
        viewOffer.isHidden = true

        imgSizeDropDown.isHidden = true
        
        lblSimilarProdTitle.isHidden = true
        
        lblProductSizeTitle.font = kCustom_Font(fontName: kPoppin_SemiBold, fontSize: 12.0)
        lblProductQtyTitle.font = kCustom_Font(fontName: kPoppin_SemiBold, fontSize: 12.0)
        lblProductSellingTitle.font = kCustom_Font(fontName: kPoppin_SemiBold, fontSize: 12.0)
        lblProductShippingTitle.font = kCustom_Font(fontName: kPoppin_SemiBold, fontSize: 12.0)
        lblProductTotalTitle.font = kCustom_Font(fontName: kPoppin_SemiBold, fontSize: 12.0)
        
        lblProductSizeTitle.text = StringFile.strProductSize[lan]
        lblProductQtyTitle.text = StringFile.strQuantity[lan]
        lblProductSellingTitle.text = StringFile.strSellingPrice[lan]
        lblProductShippingTitle.text = StringFile.strShippingPrice[lan]
        lblProductTotalTitle.text = StringFile.strTotalPrice[lan]
        
        btnBuy.setTitle(StringFile.strBUYNOW[lan], for: .normal)
        btnAddCart.setTitle(StringFile.strADDTOCART[lan], for: .normal)
        btnCallOrder.setTitle(StringFile.strCALLTOGIVEORDER[lan], for: .normal)
        lblAboutProductTitle.text = StringFile.strAboutProduct[lan]
        
        lblTechNameTitle.text = StringFile.strTechnicalName[lan]
        lblValueByAdminTitle.text = StringFile.strValuebyAdmin[lan]
        
        lblSimilarProdTitle.text = StringFile.strSimilarProduct[lan]
        
        self.getProductDetailsAPI(withProductId: productId)

    }
    
    func moveToCheckOutPage(withDetail totalAmount: String, gstAmount: String, walletAmount: String, wallet_point: String){
        let obj_CheckoutView = self.storyboard?.instantiateViewController(withIdentifier: "CheckoutViewCtr") as! CheckoutViewCtr
        obj_CheckoutView.passedTotalAmount = totalAmount
        obj_CheckoutView.passedPrice = currentProductUnit.sale_price
        obj_CheckoutView.passedGSTAmount = gstAmount
        obj_CheckoutView.passedWalletPoint = wallet_point
        obj_CheckoutView.passedWalletAmount = walletAmount
        self.navigationController?.pushViewController(obj_CheckoutView, animated: true)
    }

    func showUnitList(){
        self.myPickerView = UIPickerView(frame:CGRect(x: 0, y: UIScreen.main.bounds.size.height - 216, width: self.view.frame.size.width, height: 216))
        self.myPickerView.delegate = self
        self.myPickerView.dataSource = self
        self.myPickerView.backgroundColor = UIColor.white
        self.myPickerView.tag = 1
        self.view.addSubview(self.myPickerView)
        toolBar = UIToolbar(frame:CGRect(x: 0, y: UIScreen.main.bounds.size.height - 216, width: self.view.frame.size.width, height: 50))
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = kAppThemeGreenDarkColor
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: StringFile.strDone[lan], style: .plain, target: self, action: #selector(self.doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: StringFile.strCancel[lan], style: .plain, target: self, action: #selector(self.cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        self.view.addSubview(toolBar)
        
        self.myPickerView.selectRow(selectedProductUnitIndex, inComponent: 0, animated: true)

    }
    
    @objc func doneClick() {
        selectedProductUnitIndex = currentProductUnitIndex
        toolBar.removeFromSuperview()
        myPickerView.removeFromSuperview()
        
        self.getProductAttributeAPI(withProductId: productId, withAttributeId: "\(objProduct.unit[self.selectedProductUnitIndex].id)")
        
//        self.currentProductUnit = objProduct.unit[self.selectedProductUnitIndex]
//        self.updateProductUnitDetail(withDetails: self.currentProductUnit)
    }
    
    @objc func cancelClick() {
        if selectedProductUnitIndex != currentProductUnitIndex{
            currentProductUnitIndex = selectedProductUnitIndex
            
            self.getProductAttributeAPI(withProductId: productId, withAttributeId: "\(objProduct.unit[self.selectedProductUnitIndex].id)")

            
//            self.currentProductUnit = objProduct.unit[self.selectedProductUnitIndex]
//            self.updateProductUnitDetail(withDetails: self.currentProductUnit)
        }
        toolBar.removeFromSuperview()
        myPickerView.removeFromSuperview()
    }
    
    func updateProductUnitDetail(withDetails objUnitModel: productDetailUnitDataModel){
       lblProductSize.text = "\(objUnitModel.value) \(objUnitModel.key)"
        let avblQnt : Int = Int(self.currentProductUnit.available_quantity)!
        if avblQnt == 0 {
            objUnitModel.userSelectedQuantity = avblQnt
        }
        lblProductQty.text = "\(objUnitModel.userSelectedQuantity)"
        lblProductSelling.text = "₹ \(objUnitModel.sale_price)"
       lblProductShipping.text = "₹ \(objProduct.shipping_price)"
        
        
        let doubleSellingPrice : Double = Double(objUnitModel.sale_price)!
       
        let doubleShipingPrice : Double = Double(objProduct.shipping_price)!
        
//        let sellingPrice = objUnitModel.sale_price.split(separator: ".")
//        let selling_price = sellingPrice.count > 1 ? sellingPrice[0] : "0"
//        let intPrice : Int = Int(selling_price)!
//
//        let shippingPrice = objProduct.shipping_price.split(separator: ".")
//        let shp_price = shippingPrice.count > 1 ? shippingPrice[0] : "0"
//        let intShippingPrice : Int = Int(shp_price)!
        
        let dAmount = doubleSellingPrice * Double(objUnitModel.userSelectedQuantity)
       
       // let amount : Int = (intPrice * Int(objUnitModel.userSelectedQuantity))
        
        let totalAmount = dAmount + doubleShipingPrice
        
        if objUnitModel.userSelectedQuantity != 0{
            lblProductTotal.text = "₹ \(String(format: "%.2f", totalAmount))"
            objUnitModel.totalAmount = totalAmount
        }else{
            lblProductTotal.text = "₹ 0.00"
            objUnitModel.totalAmount = 0.00
        }

    }

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    //MARK: - Web Services
    
    func getProductDetailsAPI(withProductId prodId: String){
        GlobalMethods.presentLoaderViewCtr(withViewController: self)

        let reqParam = [ParameterKeys.Id : prodId, ParameterKeys.Customer_id : kAppDelegate.userLoginData.id, ParameterKeys.User_Id : kAppDelegate.userLoginData.id] as [String : Any]

        WebServiceCall.callMethodWithURL(route: APIRouter.ProductDetail(reqParam)) { (_ responseCode: Int?, _ responseValue : [String : AnyObject]?, _ error : Error?) in
                                    
            if (responseValue != nil) {
                
                let responseModel = ResponseModelNew(dictionary: responseValue! as NSDictionary)
                
                if responseModel.status == 200 && responseModel.dataDict != nil {
                    kOrderPhoneNumber = responseModel.mobile
                    self.objProduct = ProductDetailModel(dictionary: responseModel.dataDict! as NSDictionary)
                    self.setProductDetailRespone()
                
                }
                else {
                    kOrderPhoneNumber = responseModel.mobile
                    self.setProductDetailRespone()
                    GlobalMethods.showAlert(alertTitle: StringFile.strError[lan], alertMessage: StringFile.msgNeedProperDetails[lan], viewCtr: self, completion: nil)
                }
            }
            else {
                GlobalMethods.showErrorFromAPI(responseCode, responseValue, error, viewCtr: self)
            }
        }
    }
    
    
    func setProductDetailRespone(){
        lblTitle.text = StringFile.strProductDeatils[lan]//objProduct.name
        imgProduct.sd_setImage(with: objProduct.coverImgUrl, placeholderImage: kProductImagePlaceHolder)
        lblProductName.text = objProduct.name
        lblDealerName.text = "\(StringFile.strBy[lan]) : \(objProduct.company_name)"
        lblSellerName.text = "\(StringFile.strSellBy[lan]) : \(StringFile.strAppName[lan])"
        
        if passedAttrbId.count > 0{
            self.getProductAttributeAPI(withProductId: self.productId, withAttributeId: self.passedAttrbId, isNeedToLoader: false)
        }else{
            if objProduct.unit.count > 0{
                self.getProductAttributeAPI(withProductId: self.productId, withAttributeId: "\(objProduct.unit[0].id)", isNeedToLoader: false)
            }
        }
        

        if self.objProduct.unit.count > 1{
            self.imgSizeDropDown.isHidden = false
        }
        
        if objProduct.additional_info.count > 1{
            
            self.viewAboutProduct.isHidden = false //true
            hightViewProdTechConstraint.constant = tblProdTech.contentSize.height * 1.1 //* CGFloat((objProduct.additional_info.count) - 2) //0
            
            lblAboutProductTitle.isHidden = false
            
            
        }else{
                    let font = UIFont.systemFont(ofSize: 12)
            //        var width:CGFloat = 0
                    var numberOfLinesKey:CGFloat = 0
                    for infoValue in self.objProduct.additional_info {
                        let width = infoValue.value.sizeOfString(OfFont: font).width // size: {w: 98.912 h: 14.32}
                        if width < 125 {
                            numberOfLinesKey = numberOfLinesKey + 1
                        }
                        else{
                            let i = Int(width.rounded(.up) / 125) // 3
                            numberOfLinesKey = CGFloat(i) + numberOfLinesKey + 1
                        }
                    }
                    
                    var numberOfLinesValue:CGFloat = 0
                    for infoValue in self.objProduct.additional_info {
                        let width = infoValue.key.sizeOfString(OfFont: font).width // size: {w: 98.912 h: 14.32}
                        if width < 125 {
                            numberOfLinesValue = numberOfLinesValue + 1
                        }
                        else{
                            let i = Int(width.rounded(.up) / 125) // 3
                            numberOfLinesValue = CGFloat(i) + numberOfLinesValue
                        }
                    }


                    
                    hightViewProdTechConstraint.constant = CGFloat(((numberOfLinesKey > numberOfLinesValue ? numberOfLinesKey : numberOfLinesValue) * 28) + 16)

        }
        
        if objProduct.offer.offer_title.count > 0{
            topLabelAboutProductConstraint.constant = 178
            self.setViewHideShowWithAnimarion(view: viewOffer, hidden: false)
            lblOfferInstruction.text = StringFile.msgOfferInformation[lan]
            lblOfferProdName.text = objProduct.offer.offer_product
            imgOfferProd.sd_setImage(with: objProduct.offer.offerProductImgUrl, placeholderImage: kProductImagePlaceHolder)
            lblOfferProdQty.text = "\(StringFile.strQuantity[lan]) : \(objProduct.offer.offer_quantity)"
            lblOfferFreeCount.text = objProduct.offer.offer_title

        }else{
            topLabelAboutProductConstraint.constant = 16
            viewOffer.isHidden = true
        }
        
        tblProdTech.reloadData()
    }
    
    func getProductAttributeAPI(withProductId prodId: String, withAttributeId: String, isNeedToLoader: Bool = true){
      
        if isNeedToLoader{
            GlobalMethods.presentLoaderViewCtr(withViewController: self)
        }

        let reqParam = [ParameterKeys.Product_id : prodId, ParameterKeys.Customer_id : kAppDelegate.userLoginData.id, ParameterKeys.Attribute_id : withAttributeId, ParameterKeys.User_Id : kAppDelegate.userLoginData.id] as [String : Any]

        WebServiceCall.callMethodWithURL(route: APIRouter.ProductAttributeDetail(reqParam)) { (_ responseCode: Int?, _ responseValue : [String : AnyObject]?, _ error : Error?) in
            
            if isNeedToLoader{
                GlobalMethods.dismissLoaderView()
            }else{
                self.getSimilarProductAPI(withProductId: prodId)
            }
            
            
            if (responseValue != nil) {
                
                let responseModel = ResponseModelNew(dictionary: responseValue! as NSDictionary)
                
                if responseModel.status == 200 && responseModel.dataDict != nil {
                    
                    self.currentProductUnit = productDetailUnitDataModel(dictionary: responseModel.dataDict! as NSDictionary)
                    self.updateProductUnitDetail(withDetails: self.currentProductUnit)

                }
                else {
                    GlobalMethods.showAlert(alertTitle: StringFile.strError[lan], alertMessage: StringFile.msgNeedProperDetails[lan], viewCtr: self, completion: nil)
                }
            }
            else {
                GlobalMethods.showErrorFromAPI(responseCode, responseValue, error, viewCtr: self)
            }
        }
    }
    
    func getSimilarProductAPI(withProductId prodId: String){
      
        let reqParam = [ParameterKeys.Id : prodId, ParameterKeys.User_Id : kAppDelegate.userLoginData.id] as [String : Any]

        WebServiceCall.callMethodWithURL(route: APIRouter.SimilarProduct(reqParam)) { (_ responseCode: Int?, _ responseValue : [String : AnyObject]?, _ error : Error?) in
            
            GlobalMethods.dismissLoaderView()
            
            if (responseValue != nil) {
                
                let responseModel = SimilarProductResponseModel(dictionary: responseValue! as NSDictionary)
                
                if responseModel.status == 200{
                    
                    self.arrySimilarProduct.removeAll()
                    self.arrySimilarProduct = responseModel.data
                    self.similarProductApiResponseSet()

                }
                else {
                    GlobalMethods.showAlert(alertTitle: StringFile.strError[lan], alertMessage: StringFile.msgNeedProperDetails[lan], viewCtr: self, completion: nil)
                }
            }
            else {
                GlobalMethods.showErrorFromAPI(responseCode, responseValue, error, viewCtr: self)
            }
        }
    }
    
    func similarProductApiResponseSet(){
        
        if arrySimilarProduct.count > 0{
            hightSimilarProCVConstraint.constant = 220
            lblSimilarProdTitle.isHidden = false
        }else{
            hightSimilarProCVConstraint.constant = 0
        }
        
        hightMainviewConstraint.constant = hightSimilarProCVConstraint.constant + hightViewProdTechConstraint.constant + 800
        
        if objProduct.offer.offer_title.count > 0{
            hightMainviewConstraint.constant = hightMainviewConstraint.constant + 150
        }
        
        self.cvProduct.reloadData()

    }
    
    func addToCartAPI(){
        
        GlobalMethods.presentLoaderViewCtr(withViewController: self)

        let prodSize = "\(currentProductUnit.value)\(currentProductUnit.key)"

        let reqParam = [ParameterKeys.Customer_id : kAppDelegate.userLoginData.id, ParameterKeys.Product_id: objProduct.id, ParameterKeys.Quantity : self.lblProductQty.text!, ParameterKeys.Product_size: prodSize, ParameterKeys.Total_price : currentProductUnit.totalAmount, ParameterKeys.Attribute_id : currentProductUnit.id, ParameterKeys.User_Id : kAppDelegate.userLoginData.id] as [String : Any]

        WebServiceCall.callMethodWithURL(route: APIRouter.AddToCart(reqParam)) { (_ responseCode: Int?, _ responseValue : [String : AnyObject]?, _ error : Error?) in
            GlobalMethods.dismissLoaderView()
            
            if (responseValue != nil) {
                
                let responseModel = AddToCartModel(dictionary: responseValue! as NSDictionary)
                
                if responseModel.status == 200{
                    
                   GlobalMethods.showAlert(alertTitle: StringFile.strSuccess[lan], alertMessage: StringFile.msgAddCartSuccess[lan], viewCtr: self, completion: nil)
                    
                    var avblQnt : Int = Int(self.currentProductUnit.available_quantity)!
                    
                    avblQnt = avblQnt - Int(self.currentProductUnit.userSelectedQuantity)
                    
                   // self.currentProductUnit.userSelectedQuantity = 0
                    self.currentProductUnit.available_quantity = "\(avblQnt)"
                    self.updateProductUnitDetail(withDetails: self.currentProductUnit)
                
                    GlobalMethods.getCartCount()

                }
                else {
                    GlobalMethods.showAlert(alertTitle: StringFile.strError[lan], alertMessage: StringFile.msgNeedProperDetails[lan], viewCtr: self, completion: nil)
                }
            }
            else {
                GlobalMethods.showErrorFromAPI(responseCode, responseValue, error, viewCtr: self)
            }
        }
    }
    //buy-now?
    func getProductBuyDetails(){
      
       // startAnimating(kLoaderSize, message: "", type: NVActivityIndicatorType.ballSpinFadeLoader, fadeInAnimation: nil)
        GlobalMethods.presentLoaderViewCtr(withViewController: self)
        

        WebServiceCall.callMethodWithURL(route: APIRouter.GetBuyNowProduct(withCustId: kAppDelegate.userLoginData.id, withProdId: "\(objProduct.id)", withQuantity: self.lblProductQty.text!, withAttributeId: "\(currentProductUnit.id)", withTotalPrice: "\(currentProductUnit.totalAmount)")) { (_ responseCode: Int?, _ responseValue : [String : AnyObject]?, _ error : Error?) in
                        
           // self.stopAnimating(nil)
            GlobalMethods.dismissLoaderView()
            
            if (responseValue != nil) {
                
                let responseModel = ProductBuyNowModel(dictionary: responseValue! as NSDictionary)
                
                if responseModel.status == 200 {
                    self.moveToCheckOutPage(withDetail: responseModel.data.total_order_price, gstAmount: responseModel.data.gst_price, walletAmount: responseModel.data.total_wallet, wallet_point: responseModel.data.total_point)
                
                }
                else {
                    GlobalMethods.showAlert(alertTitle: StringFile.strError[lan], alertMessage: StringFile.msgNeedProperDetails[lan], viewCtr: self, completion: nil)
                }
            }
            else {
                GlobalMethods.showErrorFromAPI(responseCode, responseValue, error, viewCtr: self)
            }
        }
    }

    //MARK:- IBActions
    
    @IBAction func btnCartClick(_ sender: Any) {
        let obj_CartView = self.storyboard?.instantiateViewController(withIdentifier: "CartViewCtr") as! CartViewCtr
        self.navigationController?.pushViewController(obj_CartView, animated: true)
    }
    
    @IBAction func btnBackClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnDropDownClick(_ sender: Any) {
        if self.objProduct.unit.count > 1{
            self.showUnitList()
        }
    }
    
    @IBAction func btnQtyMinusClick(_ sender: Any) {
        
        if currentProductUnit.userSelectedQuantity > 1{
            currentProductUnit.userSelectedQuantity = currentProductUnit.userSelectedQuantity - 1
            self.updateProductUnitDetail(withDetails: currentProductUnit)
        }
    }
    
    @IBAction func btnQtyPlusClick(_ sender: Any) {
        
        let avlQnt : Int = Int(currentProductUnit.available_quantity)!
        if avlQnt > currentProductUnit.userSelectedQuantity{
            currentProductUnit.userSelectedQuantity = currentProductUnit.userSelectedQuantity + 1
            self.updateProductUnitDetail(withDetails: currentProductUnit)
        }else{
            
            if avlQnt > 0{
                GlobalMethods.showAlert(alertTitle: StringFile.strSorry[lan], alertMessage: StringFile.msgProductOutofStock[lan], viewCtr: self, completion: nil)

            }else{
                GlobalMethods.showAlert(alertTitle: StringFile.strSorry[lan], alertMessage: StringFile.msgProductOutofStock[lan], viewCtr: self, completion: nil)
            }
            
        }
     
    }
    
    @IBAction func btnBuyNowClick(_ sender: Any) {
        if currentProductUnit.userSelectedQuantity > 0{
            self.getProductBuyDetails()
        }else{
            GlobalMethods.showAlert(alertTitle: StringFile.strAlert[lan], alertMessage: StringFile.msgPleaseSelectQuantity[lan], viewCtr: self, completion: nil)
        }
    }
    @IBAction func btnAddCartClick(_ sender: Any) {
        
        if currentProductUnit.userSelectedQuantity > 0{
            self.addToCartAPI()
        }else{
            GlobalMethods.showAlert(alertTitle: StringFile.strAlert[lan], alertMessage: StringFile.msgPleaseSelectQuantity[lan], viewCtr: self, completion: nil)
        }

    }
    
    @IBAction func btnCallToOrderClick(_ sender: Any) {
        
        if let url = URL(string: "tel://\(kOrderPhoneNumber)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            }
            else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    
}


extension ProductViewCtr : UITableViewDelegate, UITableViewDataSource {

    //MARK:- UITableView Methods
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.objProduct.additional_info.count
    }

    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
    
        let cell = tableView.dequeueReusableCell(withIdentifier: productTechCell, for: indexPath) as! ProductTechCell
        let objInfoModel = self.objProduct.additional_info[indexPath.row]
        
        cell.lblKey.text = objInfoModel.value
        cell.lblKey.numberOfLines = 0;
        cell.lblKey.lineBreakMode = .byWordWrapping;

        cell.lblValue.text = objInfoModel.key
        cell.lblValue.numberOfLines = 0;
        cell.lblValue.lineBreakMode = .byWordWrapping;

        return cell
    }

    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {}
}

// MARK: - UICollectionView Delegate, DataSource Methods
extension ProductViewCtr : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrySimilarProduct.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
       
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: similarProdCCell, for: indexPath) as! SimilarProdCCell
        
        let objModel = self.arrySimilarProduct[indexPath.row]
        cell.lblName.text = objModel.name
        cell.lblPrice.text = "₹\(objModel.price)"
        cell.lblSalePrice.text = "₹\(objModel.sale_price)"
        cell.lblPerSet.text = ""
        cell.lblPerSet.textColor = UIColor.black
        cell.lblMarker.textColor = UIColor.black
        cell.lblDealerName.text = "\(StringFile.strBy[lan]) : \(objModel.product_dealer)"
        cell.imgProduct.sd_setImage(with: objModel.coverImgUrl, placeholderImage: kProductImagePlaceHolder)
        
        return cell
       
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let cellWidth = 180
        let cellSize  = CGSize(width: cellWidth, height: 220)
        return cellSize

    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {

    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let objModel = self.arrySimilarProduct[indexPath.row]
        GlobalMethods.pushToProductCtr(withViewController: self, animated: true, productId: "\(objModel.id)")
    }
}

extension ProductViewCtr : UIPickerViewDelegate, UIPickerViewDataSource{
    //MARK:- PickerView Delegate & DataSource
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.objProduct.unit.count

    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let passString = "\(self.objProduct.unit[row].value) \(self.objProduct.unit[row].key)"
        return passString
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let passString = "\(self.objProduct.unit[row].value) \(self.objProduct.unit[row].key)"
        lblProductSize.text = passString
        currentProductUnitIndex = row
    }

}
