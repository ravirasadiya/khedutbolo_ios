//
//  ProductTechCell.swift
//  KhedutBolo
//
//  Created by Trivedi Sagar on 11/4/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit

class ProductTechCell: UITableViewCell {

    // MARK: - define IBOutlet start
    @IBOutlet weak var lblKey: UILabel!
    @IBOutlet weak var lblValue: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
