//
//  ProductOfferModel.swift
//  KhedutBolo
//
//  Created by Trivedi Sagar on 1/8/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit
import EVReflection


class OfferService: EVObject{
    var status : Int = 0
    var message = ""
    var data = [ProductOfferModel]()
    var mobile = ""
}

class ProductOfferModel: EVObject {
    var id : Int = 0
    var offer_type = ""
    var cover = ""
    var description1 = ""
    var coverImgUrl : URL?
    
    override public func initValidation(_ dict: NSDictionary) {
        if let sValue = dict["description"] as? String {
            self.description1 = sValue
        }

        if let sValue = dict["cover"] as? String {
            let fullUrl = "\(sValue)"
            self.coverImgUrl = URL(string: fullUrl.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)!)
            
        }
    
    }
}
