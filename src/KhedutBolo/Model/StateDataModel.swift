//
//  StateDataModel.swift
//  KhedutBolo
//
//  Created by Trivedi Sagar on 2/7/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit
import EVReflection

class StateModel: EVObject {
    var status : Int = 0
    var message = ""
    var data = [StateDataModel]()
}

class StateDataModel: EVObject {
    var STCode = ""
    var DTName = ""
}

class CityModel: EVObject{
    var status : Int = 0
    var message = ""
    var data = [CityDataModel]()
}

class CityDataModel: EVObject{
    var DTCode = ""
    var DTName = ""
}

class TehsilModel: EVObject{
    var status : Int = 0
    var message = ""
    var data = [TehsilDataModel]()
}

class TehsilDataModel: EVObject{
    var SDTCode = ""
    var SDTName = ""
}

class VillageModel: EVObject{
    var status : Int = 0
    var message = ""
    var data = [VillageDataModel]()
}

class VillageDataModel: EVObject{
    var TVCode = ""
    var Name = ""
}


