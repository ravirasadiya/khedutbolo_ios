//
//  LeftMenuModel.swift

import UIKit
import EVReflection

class LeftMenuModel: EVObject {

    var Title               : String = ""
    var LeftMenuType        : LeftMenuType!
    var ImgName             : String = ""
    
    override public func initValidation(_ dict: NSDictionary) {
        
        if let sValue = dict["LeftMenuType"] as? LeftMenuType {
            self.LeftMenuType = sValue
        }
    }
}
