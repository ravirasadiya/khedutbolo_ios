//
//  CartModel.swift
//  KhedutBolo
//
//  Created by Trivedi Sagar on 1/23/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit
import EVReflection

class CartModel: EVObject {
    var status : Int = 0
    var message = ""
    var data = CartObjectModel()
}

class CartObjectModel: EVObject{
    var total : Int = 0
    var carts = [CartDataModel]()
    var gst_price = ""
    var total_order_price = ""
    var total_point : Int = 0
    var total_wallet : Int = 0
    
    //After delete count will be total cart counr
    var count : Int = 0
}

class CartDataModel: EVObject{
    var id : Int = 0
    var name = ""
    var price = ""
    var quantity = ""
    var product_size = ""
    var attribute_id = ""
    var sale_price = ""
    var total_price = ""
    var cover = ""
    var dealer_name = ""
    
    var coverImgUrl : URL?

    override public func initValidation(_ dict: NSDictionary) {
        if let sValue = dict["cover"] as? String {
            let fullUrl = "\(sValue)"
            self.coverImgUrl = URL(string: fullUrl.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)!)
            
        }
    }
}

class AddToCartModel: EVObject {
    var status : Int = 0
    var message = ""
    
}

class GetCartCountModel: EVObject {
    var status : Int = 0
    var message = ""
   // var data : Int = 0
    var intData : Int = 0
    override public func initValidation(_ dict: NSDictionary) {
        
        if let sValue = dict["data"] as? Int {
            self.intData = sValue
        }
    }
}

class CheckOutModel: EVObject {
    var status : Int = 0
    var message = ""
    var intData : Int = 0
    override public func initValidation(_ dict: NSDictionary) {
        
        if let sValue = dict["data"] as? Int {
            self.intData = sValue
        }
    }
}
