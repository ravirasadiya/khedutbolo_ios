//
//  WalletModel.swift
//  KhedutBolo
//
//  Created by Trivedi Sagar on 3/6/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit
import EVReflection


class WalletModel: EVObject {
    var status : Int = 0
    var message = ""
    var data = [WalletModelDataModel]()
    var totalpoint : Int = 0
    var totalamount = ""
}

class WalletModelDataModel: EVObject {
    
    var id : Int = 0
    var customer_id = ""
    var order_id = ""
    var amount = ""
    var points = ""
    var credit_or_debit = ""
    var description1 = ""
    var created_at = ""
    var title = ""
    
    override public func initValidation(_ dict: NSDictionary) {
        if let sValue = dict["description"] as? String {
            self.description1 = sValue
        }
    }
}
