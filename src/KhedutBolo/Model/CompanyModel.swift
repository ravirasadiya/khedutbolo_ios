//
//  CompanyModel.swift
//  KhedutBolo
//
//  Created by Trivedi Sagar on 1/11/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit
import EVReflection


class CompanyModelResponse: EVObject {
    var status : Int = 0
    var message = ""
    var data = [CompanyModel]()
}

class CompanyModel: EVObject {
    var id : UInt = 0
    var name = ""
    var cover  = ""
    var price  = ""
    var sale_price  = ""
    var company_name = ""
    var coverImgUrl : URL?
    var category_name = ""
    
    override public func initValidation(_ dict: NSDictionary) {
        if let sValue = dict["cover"] as? String {
            let fullUrl = sValue
            self.coverImgUrl = URL(string: fullUrl.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)!)
            
        }
    }
}


class CategoriesModel : EVObject{
    var categariesType = ""
    var cat_data = [CompanyModel]()
}


