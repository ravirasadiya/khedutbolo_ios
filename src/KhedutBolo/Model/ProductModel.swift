//
//  ProductModel.swift
//  KhedutBolo
//
//  Created by Trivedi Sagar on 1/11/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit
import EVReflection

class ProductResponseModel: EVObject {
    var status : Int = 0
    var message = ""
    var data = [ProductModel]()
 
}

class FilterModel: EVObject{
    var status : Int = 0
    var message = ""
    var data = [FilterDataModel]()
}

class FilterDataModel: EVObject{
    var type = ""
    var companyFilter = [FilterCompanyModel]()
    var technicalFilter = [FilterTechicalsModel]()
    
    override public func initValidation(_ dict: NSDictionary) {
        
        if let sValue = dict["type"] as? String {
            
            if sValue == ParameterKeys.Companies{
                if let searchArry = dict["data"]  as? [[String : AnyObject]] {
                    for objDict in searchArry  {
                        let objModel = FilterCompanyModel(dictionary: objDict as NSDictionary)
                        self.companyFilter.append(objModel)
                    }
                }
            }
            
            if sValue == ParameterKeys.Techicals{
                if let searchArry = dict["data"]  as? [[String : AnyObject]] {
                    for objDict in searchArry  {
                        let objModel = FilterTechicalsModel(dictionary: objDict as NSDictionary)
                        self.technicalFilter.append(objModel)
                    }
                }
            }
                        
        }
    }
}

class FilterCompanyModel: EVObject{
    var company_id = ""
    var name = ""
    
    var isSelected = false
}

class FilterTechicalsModel: EVObject{
    var technical_name = ""
    var id = ""
    var isSelected = false
}

class ProductModel: EVObject {
    var type = ""
    var priority : Int = 0
    var myProduct = [ProductDetailModel]()
    var myCompanyList = [CompanyModel]()
    var myTreadingProduct = [ProductDetailModel]()
    
    override public func initValidation(_ dict: NSDictionary) {
        
        if let sValue = dict["type"] as? String {
            
            if sValue == ParameterKeys.ProductCat_ProductManage{
                if let searchArry = dict["data"]  as? [[String : AnyObject]] {
                    for objDict in searchArry  {
                        let objModel = ProductDetailModel(dictionary: objDict as NSDictionary)
                        self.myProduct.append(objModel)
                    }
                }
            }
            
            if sValue == ParameterKeys.Company_List{
                if let searchArry = dict["data"]  as? [[String : AnyObject]] {
                    for objDict in searchArry  {
                        let objModel = CompanyModel(dictionary: objDict as NSDictionary)
                        self.myCompanyList.append(objModel)
                    }
                }
            }
            
            if sValue == ParameterKeys.Trending_Products{
                if let searchArry = dict["data"]  as? [[String : AnyObject]] {
                    for objDict in searchArry  {
                        let objModel = ProductDetailModel(dictionary: objDict as NSDictionary)
                        self.myTreadingProduct.append(objModel)
                    }
                }
            }
            
        }
    }

}

class ProductDetailModel: EVObject {
    var id : UInt = 0
    var name = ""
    var cover = ""
    var price = ""
    var sale_price = ""
    var company_name = ""
    var coverImgUrl : URL?
    
    //Product Detail
    var mobile = ""
    var status = ""
    var weight = ""
    var shipping_price = ""
    
    var additional_info = [productAdditionalDataModel]()
    var unit = [productDetailUnitDataModel]()
    var offer = productOfferDataModel()
    
    //Similar product
    var company_id = ""
    var product_dealer = ""
    
    //Sub Categories Product
    var technical_name = ""
    var dealer_name = ""
    var category_name = ""

    override public func initValidation(_ dict: NSDictionary) {
        if let sValue = dict["cover"] as? String {
            let fullUrl = "\(sValue)"
            self.coverImgUrl = URL(string: fullUrl.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)!)
            
        }
    }
}

class productAdditionalDataModel: EVObject {
    var key = ""
    var value = ""
}

class productDetailUnitDataModel: EVObject {
    var key = ""
    var value = ""
    var quantity = ""
    var price = ""
    var sale_price = ""
    var available_quantity = ""
    var id : UInt = 0
    var user_quantity  = ""
    
    var userSelectedQuantity : Int = 1
    var totalAmount : Double = 0.0
    
    override public func initValidation(_ dict: NSDictionary) {
        if let sValue = dict["user_quantity"] as? String {
            
            let intValue = Int(sValue)
            
            if intValue! > 0{
                self.userSelectedQuantity = intValue! 
            }
           
        }
    }
}

class productOfferDataModel: EVObject{
    var offer_title = ""
    var offer_product = ""
    var offer_quantity : Int = 0
    var offer_product_image = ""
    
    var offerProductImgUrl : URL?

    override public func initValidation(_ dict: NSDictionary) {
        if let sValue = dict["offer_product_image"] as? String {
            let fullUrl = "\(sValue)"
            self.offerProductImgUrl = URL(string: fullUrl.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)!)
            
        }
    }
}

class SimilarProductResponseModel: EVObject {
    var status : Int = 0
    var message = ""
    var data = [ProductDetailModel]()
 
}



class SubCategoriesResponseModel: EVObject {
    var status : Int = 0
    var message = ""
    var data = [ProductDetailModel]()
    var subcategory_status : Int = 0
}

class ProductBuyNowModel: EVObject{
    var status : Int = 0
    var message = ""
    var data = ProductBuyNowDataModel()
}
class ProductBuyNowDataModel : EVObject{
    var total_point = ""
    var total_wallet = ""
    var gst_price = ""
    var total_order_price = ""
}
