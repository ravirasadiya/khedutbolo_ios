//
//  NotificationModel.swift
//  KhedutBolo
//
//  Created by Trivedi Sagar on 2/10/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit
import EVReflection


class NotificationModel: EVObject {
    var status : Int = 0
    var message = ""
    var data = [NotificationDataModel]()
}

class NotificationDataModel: EVObject {
    var notify_detail = ""
    var date = ""
    var time = ""
}
