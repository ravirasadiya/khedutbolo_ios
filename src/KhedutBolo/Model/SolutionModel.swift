//
//  SolutionModel.swift

import UIKit
import EVReflection

class CropModel: EVObject{
    var status : Int = 0
    var message = ""
    var data = [CropDataModel]()
}

class CropDataModel: EVObject{
    var id  : Int = 0
    var name = ""
    var image = ""
    
    var imgUrl              : URL?
    
    override public func initValidation(_ dict: NSDictionary) {
        if let sValue = dict["image"] as? String {
            let fullUrl = "\(sValue)"
            self.imgUrl = URL(string: fullUrl.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)!)
            
        }
    }
}

class CropSolutionResponseModel: EVObject {
    var status : Int = 0
    var message = ""
    var data = [CropSolutionDataModel]()
 
}

class CropSolutionDataModel: EVObject {
    var id                  : String = ""
    var name                : String = ""
    var image               : String = ""
    
    var imgUrl              : URL?
    
    override public func initValidation(_ dict: NSDictionary) {
        if let sValue = dict["image"] as? String {
            let fullUrl = "\(sValue)"
            self.imgUrl = URL(string: fullUrl.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)!)
            
        }
    }
}


class SolutionModelResponse: EVObject {
    var status : Int = 0
    var message = ""
    var data = [SolutionModel]()
}

class SolutionModel: EVObject {
    var id : UInt = 0
    var name = ""
    var technical_name = ""
    var cover  = ""
    var price  = ""
    var sale_price  = ""
    var company_name = ""
    var coverImgUrl : URL?
    var category_name = ""
    var additional_info = [productAdditionalDataModel]()
    var dealer_name = ""
    var mobile = ""

    override public func initValidation(_ dict: NSDictionary) {
        if let sValue = dict["cover"] as? String {
            let fullUrl = "\(sValue)"
            self.coverImgUrl = URL(string: fullUrl.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)!)
            
        }
    }
}


class SolutionCategoriesModel : EVObject{
    var categariesType = ""
    var cat_data = [SolutionModel]()
}


