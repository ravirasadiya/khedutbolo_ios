//
//  ResponseModel.swift
//  ClipDart
//
//  Created by Chirag Patel on 21/11/19.
//  Copyright © 2019 ClipDart Inc. All rights reserved.
//

import UIKit
import EVReflection

/*class ResponseModel: EVObject {
    
    var status              : UInt = 0
    
    var data                : [String : AnyObject]?
    
    var message            : String = ""

    var email            : String = ""
    var code            : String = ""
    
    var intData : Int = 0
    override public func initValidation(_ dict: NSDictionary) {
        
        if let sValue = dict["data"] as? [String : AnyObject] {
            self.data = sValue
        }
        
        if let sValue = dict["data"] as? Int {
            self.intData = sValue
        }
        
    }
    
    
}*/

class ResponseModelNew: EVObject {
    
    var status              : UInt = 0
    
    var dataDict                : [String : AnyObject]?
    
    var message            : String = ""

    var email            : String = ""
    var code            : String = ""
    
    var intData : Int = 0
    override public func initValidation(_ dict: NSDictionary) {
        
        if let sValue = dict["data"] as? [String : AnyObject] {
            self.dataDict = sValue
        }
        
        if let sValue = dict["data"] as? Int {
            self.intData = sValue
        }
        
    }
    var mobile            : String = ""

    
}
