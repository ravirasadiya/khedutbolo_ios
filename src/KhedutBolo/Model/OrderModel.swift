//
//  OrderModel.swift
//  KhedutBolo
//
//  Created by Trivedi Sagar on 1/24/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit
import EVReflection


class OrderModel: EVObject {
    var status : Int = 0
    var message = ""
    var data = [OrderDataModel]()
}

class OrderDataModel: EVObject{
    
    var order_id : Int = 0
    var product_name = ""
    var price = ""
    var quantity = ""
    var sale_price = ""
    var created_at = ""
    var cover = ""
    var shipping_price = ""
    var value = ""
    var key = ""
    var Dealer = ""
    var total : Double = 0.0
   
    var coverImgUrl : URL?

    override public func initValidation(_ dict: NSDictionary) {
        if let sValue = dict["cover"] as? String {
            let fullUrl = "\(sValue)"
            self.coverImgUrl = URL(string: fullUrl.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)!)
            
        }
    }
}

class commonOrderModel: EVObject{
    var status : Int = 0
    var message = ""
    var data = [commonOrderDataModel]()
}
class commonOrderDataModel: EVObject{
    var id : Int = 0
    var created_at = ""
    var total = ""
}

class OrderCancelModel: EVObject{
    var status : Int = 0
    var message = ""
}
