//
//  LoginUserModel.swift
//  KhedutBolo
//
//  Created by Trivedi Sagar on 1/7/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit
import EVReflection
import Keychain

let kLoggedInUserModel = "LoggedInUserModel"

class LoginUserModel: EVObject {

    var is_register = ""
    var mobile = ""
    var otp = ""
    var id = ""
    var address = ""
    var language = ""
    var image = ""
    var name = ""
    var state = ""
    var state_id = ""
    var city = ""
    var city_id = ""
    var tehsil = ""
    var tehsil_id = ""
    var village = ""
    var village_id = ""
        
    /*----- save loggedin user details -----*/
    func save() {
        
        UserDefaults.standard.removeObject(forKey: kLoggedInUserModel)
        UserDefaults.standard.synchronize()
        
        /*---- encode object and save in userdefaults -----*/
        let user_EncodedObject = NSKeyedArchiver.archivedData(withRootObject: self)
        UserDefaults.standard.set(user_EncodedObject, forKey: kLoggedInUserModel)
        UserDefaults.standard.synchronize()
    }
    
    /*----- delete loggedin user details -----*/
    func delete() {
        
        kAppDelegate.userLoginData = nil
        
        UserDefaults.standard.removeObject(forKey: kLoggedInUserModel)
        UserDefaults.standard.synchronize()
        
       // _ = Keychain.delete(ParameterKeys.AuthToken)
    }
    
}
