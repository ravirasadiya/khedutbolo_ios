//
//  AppDelegate.swift
//  KhedutBolo
//
//  Created by Trivedi Sagar on 10/18/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit
import Firebase
import FirebaseMessaging
var lan = -1
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate, MessagingDelegate{
    
    var window: UIWindow?
    
    /*~~~~~ push notification ~~~~~*/
    var dicRemoteNotification : [AnyHashable : Any]?
    var iOSDeviceToken : String = ""

    /*----- 0-English, 1-Hindi 2-Gujarati -----*/
    var userCurrentLanguage = 0
    var userLoginData : LoginUserModel!
    var cartCount = 0
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        UIApplication.shared.statusBarStyle = .lightContent
//        if #available(iOS 13.0, *) {
//            window?.overrideUserInterfaceStyle = .light
//        }
        
        /*~~~~~ firebase configuration ~~~~~*/
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        
        /*~~~~~ ask for push notification ~~~~~*/
        registerPushNotification()
        
        /*----- set languege -----*/
        if UserDefaults.standard.object(forKey: kLanguage) != nil{
          //  lan =  GlobalMethods.GetupdateLanguageForCurrentUser(str: userLoginData.language)
          //  lan = 0
            self.userCurrentLanguage = UserDefaults.standard.object(forKey: kLanguage) as! Int
        }else{
            GlobalMethods.SetUserDefualtLanguage(withLanguageType: Language.kEnglish.rawValue)
        }
        
        /*~~~~~ check user is logged in or not ~~~~~*/
        if let user_DecodedObject = UserDefaults.standard.object(forKey: kLoggedInUserModel) {
            self.userLoginData = NSKeyedUnarchiver.unarchiveObject(with: user_DecodedObject as! Data) as! LoginUserModel?
            lan =  GlobalMethods.GetupdateLanguageForCurrentUser()
            print("lan appmain:\(lan)")
        }
        
        return true
    }

    // MARK: UISceneSession Lifecycle

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
    //MARK: - Register token for push notifications
    func registerPushNotification() {
            
            /*----- register push notification -----*/
            // iOS 10 support
            if #available(iOS 10, *) {
                UNUserNotificationCenter.current().requestAuthorization(options:[.badge, .alert, .sound]){ (granted, error) in
                    
                if granted {
                    DispatchQueue.main.async {
                        UIApplication.shared.registerForRemoteNotifications()
                    }
                } else {
                    // handle the error
                }
            }
        }
    }
        
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        print("didRegisterForRemoteNotificationsWithDeviceToken")
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        print(deviceTokenString)
        //WebServiceCall.sendTokenToServer(withDeviceToken: iOSDeviceToken)
    }

    // Push notification received
    func application(_ application: UIApplication, didReceiveRemoteNotification data: [AnyHashable : Any]) {

        /*----- check user is logged in or not -----*/
        if (UserDefaults.standard.object(forKey: kLoggedInUserModel) != nil) {
           // HandlePushNotification.handlePushNotificationWithBodyMessage(userInfo: data, appState: application.applicationState)
        }
    }
    
     func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        
        print("didReceiveRegistrationToken")
        let dataDict:[String: String] = ["token": fcmToken]
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
        
        iOSDeviceToken = fcmToken
        print(iOSDeviceToken)
     }
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                        didReceive response: UNNotificationResponse,
                                        withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo1 = response.notification.request.content.userInfo
        // Print message ID.
        // if let messageID = userInfo[gcmMessageIDKey] {
        // print("Message ID: \(messageID)")
        // }
        //
        print(userInfo1)
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let presentViewController = storyBoard.instantiateViewController(withIdentifier: "NotificationViewCtr") as! NotificationViewCtr
        
        presentViewController.yourDict = userInfo1 as NSDictionary //pass userInfo data to viewController
        self.window?.rootViewController = presentViewController
        presentViewController.present(presentViewController, animated: true, completion: nil)
        
        completionHandler()
    }
        }



